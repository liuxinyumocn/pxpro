// domoe/pay/view.js
import config from '../../app/config.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        console.log(options);
        //尝试申请微信支付
        wx.request({
            url: config.pay + "pay/Cashier/payMini",
            method:"POST",
            data:{
                "appid": "wxed84ffb2f008f653",
                "order":options.order,
                "openid": getApp().data.user.getOpenID()
            },
            success:(res)=>{
                console.log(res);
                if(res.data.code == 200){
                    wx.requestPayment({
                        timeStamp: res.data.timestamp+"",
                        nonceStr: res.data.nonceStr,
                        package: res.data.package,
                        signType: res.data.signType,
                        paySign: res.data.paySign,
                        success:(res)=>{
                            this.back("请稍后", "success");
                        },
                        fail:(res)=>{
                            this.back("未支付");
                        }
                    })
                }else{
                    this.back(res.data.errmsg);
                }
            },
            fail:(res)=>{
                this.back("网络异常");
            }
        })
    },
    back:function(msg,icon = "none"){
        wx.showToast({
            title: msg,
            icon: icon
        })
        setTimeout(function(){
            wx.navigateBack({});
        },1500);
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})