import config from '../../app/config.js';
class Browser {
    constructor() {
        this.callback = function(){}
    }
    open(url,callback=function(){}){
        this.callback = callback;
        wx.navigateTo({
            url: '/domoe/browser/view?url=' + encodeURIComponent(url),
        })
    }
    forpay(orderid, callback = function () {},paydomain='https://pay.domoe.cn/'){
        this.callback = callback;
        let url = paydomain+'pay/Cashier/deal/query/'+orderid;
        this.open(url,callback);
    }
}

export default new Browser();