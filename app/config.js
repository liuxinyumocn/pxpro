let local = false;
/*
    本地环境配置
*/
let config_local = {
    pxpro:'http://192.168.1.102:8080/',
    systemInfo: null,
    system: 'iOS',
    pay:'https://m.domoe.cn/',
    viptoken:'fNrXj8HQJv9AviffPt8h0',
    
    //COS
    bucket:'pxpro-1256097093',
    region: 'ap-shanghai',
    path: "pxpro/",
    autoSignDistance: 200000
}

/*
    线上环境配置
*/
let config_online = {
    pxpro: 'https://pxpro.domoe.cn/',
    systemInfo: null,
    system:'iOS',
    pay: 'https://m.domoe.cn/',
    viptoken: 'fNrXj8HQJv9AviffPt8h0',

    //COS
    bucket: 'pxpro-1256097093',
    region: 'ap-shanghai',
    path: "pxpro/",
    autoSignDistance: 200000
}
let si = wx.getSystemInfoSync();
config_local.systemInfo = si;
config_online.systemInfo = si;
if (si.system.indexOf('iOS') != 0) {
    config_local.system = 'Android';
    config_online.system = 'Android';
}

let out = local ? config_local : config_online;
export default out;