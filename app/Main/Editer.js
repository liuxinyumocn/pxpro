/*
    编辑器
*/
import format from '../lib/format.js';
import vip from '../user/vip/vip.js';

export default class Editer{

    constructor(){
        this._Data = [];    //点位序列 
        this._L = 0;        //左右上下 界
        this._R = 0;
        this._T = 0;
        this._B = 0;

        this._width = 0;    //画布宽高
        this._height = 0;
        this._px = 10;       //单个像素块的宽度
        this._x = 0;        //屏幕左上角距离参考原点左上角的像素坐标（非像素块坐标）
        this._y = 0;
        this._backgroundColor = null;   //背景颜色 默认透明 为 null

        //辅助线
        /*
            {
                p:'h/v',    //水平或垂直
                v:123,      //水平或垂直的偏移量
                id:1
            }
        */
        this._guideId = 4;      //自增的索引
        this._guide = false;    //是否开启辅助线模式
        this._gData = [];       //辅助线数据

        //当前画笔配置
        this.pencilColor = 'rgb(0,0,0)';   //画笔颜色
        this.pencilCover = false;        //覆盖模式 关闭时画笔不会污染到已经绘制的像素方格

        //撤销功能
        this._Steps = [];    //撤销记录   撤销记录仅记录20步骤 实际运行回退10步骤
        this._currentStep = []; //当前步骤
        this._currentRecord = false; //单步骤记录标识
        this._alerting = false;
        this._bztsvip = false;

        //参考图像模块
        this._photoWidth = 0;   //图像宽度
        this._photoHeight = 0;  //图像高度
        this._photoData = [];
        this._photoX = 0;       //偏移
        this._photoY = 0;
        this._photoShow = false;

        //回调事件
        this.OnChange = function(){}
    }

    /*
        获取当前视野信息
    */
    getViewData(opt) {
        opt = opt || {};
        format.callback(opt);
        //获取视窗大小
        let windx = this._x;
        let windy = this._y;
        let windw = windx + this._width;
        let windh = windy + this._height;
        //网格
        let brid = {
            x:this._x,
            y:this._y
        };
        //像素块
        let arr = [];
        for(let i in this._Data){
            let itm = this._Data[i];
            let itmx = itm.x * this._px;
            let itmy = itm.y * this._px;
            if (itmx + this._px >= windx && itmy + this._px >= windy &&
                itmx <= windw && itmy <= windh){
                arr.push(itm);
            }
        }
        let block = {
            data:arr,
            offsetx:this._x,
            offsety: this._y
        }
        //参考图像
        let arr2 = [];
        arr2 = this._photoData;
        let photo = {
            show: this._photoShow,
            data:arr2,
            w:this._photoWidth,
            h:this._photoHeight,
            px: this._px,
            offsetx: this._x,
            offsety: this._y,
            posX:this._photoX,
            posY:this._photoY
        }

        opt._success({
            px:this._px,
            brid:brid,
            block:block,
            backgroundColor: this._backgroundColor,
            guide: !this._guide ? null : {
                data:this._gData,
                x:this._x,
                y:this._y,
                px:this._px
            },
            photo:photo
        });
    }

    suitview(opt) { //设置为当前最适视野
        opt = opt || {};
        format.callback(opt);
        if(this._Data.length == 0){
            opt._success();
            return  ;
        }
        let w = parseInt(this._width / (this._R - this._L));
        let h = parseInt(this._height/(this._B - this._T));
        let px = 0;
        if(w > 10)
            w = 10;
        if(h > 10)
            h = 10;
        if(w > h)
            px = h;
        else 
            px = w;
        this._px = px;
        //计算偏移
        let offx = parseInt((this._R - this._L) / 2 + this._L);
        let offy = parseInt((this._B - this._T) / 2 + this._T);
        this._x = offx * px - this._width / 2;
        this._y = offy * px - this._height / 2;
        this.OnChange();
        setTimeout(function(){
            opt._success();
        }.bind(this),1000/50);
    }

    /*
        设置当前视野信息
        opt={
            offset:{x,y},
            scale:{
                    x:_op.x,
                    y:_op.y,
                    scale:scale
                }
        }
    */
    setViewData(opt){
        if(opt.offset != null){
            this._x -= opt.offset.x;
            this._y -= opt.offset.y;
        }
        if (opt.scale != null) {
            //以中心点放大
            let npx = this._px * opt.scale.scale;

            if(vip.getVIPData().vip){
                if (npx < 2) {
                    npx = 2;
                    this._alert('已达视野上限');
                } else if (npx < 4 && this._px > npx) {
                    this._alert('视野过大会影响处理性能');
                }
            }else{
                if(npx < 8)
                {
                    npx = 9;
                }else if(npx > 12){
                    npx = 12;
                }
                this._alert('请开通会员解锁更大视野', !this._bztsvip);
            }

            //计算图像中心像素点
            this._px = npx;
            let w = this._width / 2, h = this._height / 2;    
            let ox =  this._x + w, oy = this._y + h;         
            let nx = ox * opt.scale.scale , ny = oy * opt.scale.scale; 
            let offx = nx - w, offy = ny - h;   
            this._x = offx;
            this._y = offy;
        }

        this.OnChange();
    }

    _alert(str,model = false){
        if(!model){
            if (!this._alerting) {
                wx.showToast({
                    title: str,
                    icon: 'none'
                })
                this._alerting = true;
                setTimeout(function () {
                    this._alerting = false;
                }.bind(this), 2000);
            }
        }else{
            if(!this._alerting){
                this._alerting = true;
                wx.showModal({
                    title: '提示',
                    content: '激活会员可以解锁更大视野，免费的！是否立即前往？',
                    success:(res)=>{
                        this._alerting = false;
                        if(res.confirm){
                            vip.open();
                        }else{
                            this._bztsvip = true;
                        }
                    }
                })
            }
        }
    }

    /*
        由当前屏幕坐标计算当前像素块坐标
        return {
            x:
            y:
        }
    */
    getPosByTouchPos(tx,ty){
        let px = tx + this._x;
        let py = ty + this._y;
        let ox, oy;
        ox = px / this._px;
        oy = py / this._px;
        return {
            x: parseInt(ox) + (ox < 0 ? -1 : 0),
            y: parseInt(oy) + (oy < 0 ? -1 : 0)
        }
    }

    /*
        结算此前的步骤
    */
    stepSettle(){
        if(this._currentStep.length > 1)
            this._Steps.push(this._currentStep);
        this._currentStep = [];
        this._currentStep.push({
            type: 'ltrb',
            l:this._L,
            t:this._T,
            r:this._R,
            b:this._B
        });
        //当当前记录的步骤超过30时，取最后面的10步骤
        if(this._Steps.length >= 30){
            let arr = [];
            for(let i = this._Steps.length - 11;i < this._Steps.length;i++){
                arr.push(this._Steps[i]);
            }
            this._Steps = arr;
        }
    }

    /*
        记录新步骤
    */
    _record(attr){
        this._currentStep.push(attr);
    }

    /*
        撤销一步骤
        撤销前将会进行一次自动结算
    画笔工具    
        {
            type:'pcl',
            x:x,
            y:y,
            bf:itm.c,
            aft:c
        }
    橡皮工具
        {
            type:'eraser',
            x:x,
            y:y,
            bf:item.c
        }
    边缘
        {
            type: 'ltrb',
            l:this._L,
            t:this._T,
            r:this._R,
            b:this._B
        }
    背景颜色
        {
            type: 'bgc',
            bc: this._backgroundColor,
            ac: this.pencilColor
        }

    */
    
    back(opt) {
        opt = opt || {};
        format.callback(opt);
        this.stepSettle();
        if (this._Steps.length == 0){
            opt._fail({
                code:400,
                errmsg:'不能继续撤销啦'
            });
            return ;
        }
        let lastStep = this._Steps.pop();
        let cover = this.pencilCover;
        this.pencilCover = true;
        for(let i = lastStep.length-1 ; i>=0;i--){
            //逐一撤销
            let item = lastStep[i];
            switch(item.type){
                case 'pcl':
                    if(item.bf == null){
                        this._delData(item.x,item.y,false);
                    }else{
                        this._setData(item.x,item.y,item.bf,false);
                    }
                break;
                case 'eraser':
                    this._setData(item.x,item.y,item.bf,false);
                break;
                case 'ltrb':
                    this._L = item.l;
                    this._T = item.t;
                    this._R = item.r;
                    this._B = item.b;
                break;
                case 'bgc':
                    this.setBackgroundColor(item.bc,false);
                break;
            }
        }
        this.pencilCover = cover;
        this.OnChange();
        opt._success();
    }

    /*
        设置点位信息
        opt = {
            tx,      //提供用户在画布中所触发的点位位置（并非相对位置）
            ty,
            lt,        //上一次点位用于补间 默认为null
        }
    */
    setData(opt) {
        opt = opt || {};
        format.callback(opt);
        let x=0,y=0;    //点位序列（相对位置）
        let pos = this.getPosByTouchPos(opt.tx,opt.ty);
        x = pos.x;
        y = pos.y;
        opt._success({
            x:x,
            y:y
        });
        if(opt.lt != null){
            let xl, xr, yt, yb;
            if (opt.lt.x < x) { //从左到右边  
                xl = opt.lt.x;
                xr = x;
            } else { //从右到左
                xr = opt.lt.x;
                xl = x;
            }
            if (opt.lt.y < y) {
                yt = opt.lt.y;
                yb = y;
            } else {
                yb = opt.lt.y;
                yt = y;
            }
            let arry = [];
            //求出直线方程
            if(xr == xl){
                for (let i = xl; i <= xr; i++) {
                    for (let j = yt; j <= yb; j++) {
                        if(i == x)
                            this._setData(i, j, this.pencilColor);
                    }
                }
            } else {
                let k = (y - opt.lt.y) / (x - opt.lt.x);
                let jiao = Math.abs(Math.atan(k));
                if(jiao > Math.PI/4){
                    //y轴逐减
                    if(y < opt.lt.y){
                        for(let yi = y+1 ; yi<= opt.lt.y ;yi++){
                            let xi = parseInt((yi-y)/k+x);
                            this._setData(xi, yi, this.pencilColor);
                        }
                    } else {
                        for (let yi = opt.lt.y+1; yi <= y; yi++) {
                            let xi = parseInt((yi - y) / k + x);
                            this._setData(xi, yi, this.pencilColor);
                        }
                    }
                }else{
                    //x轴逐减
                    if (x < opt.lt.x) {
                        for (let xi = x+1; xi <= opt.lt.x; xi++) {
                            let yi = parseInt(k * (xi - x) + y + 0.5);
                            this._setData(xi, yi, this.pencilColor);
                        }
                    } else {
                        for (let xi = opt.lt.x+1; xi <= x; xi++) {
                            let yi = parseInt(k * (xi - x) + y + 0.5);
                            this._setData(xi, yi, this.pencilColor);
                        }
                    }
                }
            }

        }else
            this._setData(x, y, this.pencilColor);

        this.OnChange();
    }

    _setData(x,y,c,record = true){
        if(this._Data.length == 0){
            this._L = x-1;
            this._R = x+1;
            this._T = y-1;
            this._B = y+1;
        }else{
            if (x < this._L)
                this._L = x;
            if (x > this._R)
                this._R = x;
            if (y < this._T)
                this._T = y;
            if (y > this._B)
                this._B = y;
        }
        for (let i in this._Data) {
            let itm = this._Data[i];
            if(itm.x == x && itm.y == y)
            {
                if(this.pencilCover){
                    if(record)
                        this._record({
                            type:'pcl',
                            x:x,
                            y:y,
                            bf:itm.c,
                            aft:c
                        });
                    itm.c = c;
                }
                return ;
            }
        }
        if (record)
            this._record({
                type: 'pcl',
                x: x,
                y: y,
                bf: null,
                aft: c
            });
        this._Data.push({x:x,y:y,c:c});
    }

    /*
        清除点位信息
        opt = {
            tx,      //提供用户在画布中所触发的点位位置（并非相对位置）
            ty,
            lt,        //上一次点位用于补间 默认为null
        }
    */
    delData(opt){
        opt = opt || {};
        format.callback(opt);
        let x = 0, y = 0;    //点位序列（相对位置）
        let pos = this.getPosByTouchPos(opt.tx, opt.ty);
        x = pos.x;
        y = pos.y;
        opt._success({
            x: x,
            y: y
        });
        if (opt.lt != null) {
            let xl, xr, yt, yb;
            if (opt.lt.x < x) { //从左到右边  
                xl = opt.lt.x;
                xr = x;
            } else { //从右到左
                xr = opt.lt.x;
                xl = x;
            }
            if (opt.lt.y < y) {
                yt = opt.lt.y;
                yb = y;
            } else {
                yb = opt.lt.y;
                yt = y;
            }
            let arry = [];
            //求出直线方程
            if (xr == xl) {
                for (let i = xl; i <= xr; i++) {
                    for (let j = yt; j <= yb; j++) {
                        if (i == x)
                            this._delData(i, j);
                    }
                }
            } else {
                let k = (y - opt.lt.y) / (x - opt.lt.x);
                let jiao = Math.atan(k);
                let sin = Math.abs(Math.sin(jiao * 2));
                let ds = sin * 0.99999;
                //console.log(ds);
                for (let i = xl; i <= xr; i++) {
                    for (let j = yt; j <= yb; j++) {
                        let y2 = k * (i - x) + y;
                        if (Math.abs(y2 - j) <= ds) {
                            this._delData(i, j);
                        }
                    }
                }
            }

        }
        this._delData(x, y);
        this.OnChange();
    }

    trash() {
        this._Data = [];    //点位序列
        this._L = 0;        //左右上下 界
        this._R = 0;
        this._T = 0;
        this._B = 0;
        this._backgroundColor = null;
        this.stepSettle();
        this._Steps = [];

        this._photoData = [];
        this._photoShow = false;

        this.OnChange();
    }

    _delData(x, y,record = true) {
        for(let i =0;i < this._Data.length;i++){
            let item = this._Data[i];
            if(item.x == x && item.y == y){
                //记录
                if (record)
                    this._record({
                        type:'eraser',
                        x:x,
                        y:y,
                        bf:item.c
                    });
                //清除
                for(let j=i+1;j<this._Data.length;j++){
                    this._Data[j-1] = this._Data[j];
                }
                this._Data.pop();
                return;
            }
        }
    }

    /*
        取色器
        优先取色涂层 其次取色参考底图
        opt={
            tx,
            ty
        }
    */
    pick(opt) {
        opt = opt || {};
        format.callback(opt);
        let pos = this.getPosByTouchPos(opt.tx, opt.ty);
        let nowpt = this._find(pos.x, pos.y);
        if(nowpt.c != null){
            opt._success({
                origin:'maincanvas',
                color:nowpt.c
            });
        }else{
            //检测参考底图
            let x = pos.x - this._photoX,y =pos.y - this._photoY;
            if( x< 0 || x>this._photoWidth || y<0 || y>this._photoHeight){
                opt._fail({
                    code:400,
                    errmsg:'没有颜色可以拾取'
                });
                return ;
            }
            let c = this._photoData[y][x];
            opt._success({
                origin: 'photocanvas',
                color: c[4],
                r: c[0],
                g: c[1],
                b: c[2],
                a: c[3]
            });
        }

    }

    /*
        改进的油漆桶填充算法
        opt = {
            tx,
            ty
        }
    */
    bucket(opt) {
        opt = opt || {};
        format.callback(opt);
        //判断是否颜色相同
        let pos = this.getPosByTouchPos(opt.tx, opt.ty);
        let nowpt = this._find(pos.x, pos.y);
        if (nowpt.c == this.pencilColor) {
            opt._success({
                code: 400
            });
            this.OnChange();
            return;
        }

        if(this._Data.length == 0){
            //设置背景颜色
            this.setBackgroundColor(this.pencilColor);
            opt._success({
                code: 400
            });
            this.OnChange();
            return;
        }

        let points = [];    //已知点序列

        //根据数据重新组建二维数组提高搜索效率
        let map = [];
        let w = this._R - this._L+1,h = this._B - this._T+1;
        for(let ix = 0;ix < w ;ix++){
            map.push([]);
            for(let iy=0;iy<h;iy++){
                map[ix].push([null]);
            }
        }
        //扫描出Data中最左上角落的元素
        let ell = this._Data[0].x;
        let elt = this._Data[0].y;
        for(let i in this._Data){
            let item = this._Data[i];
            if(item.x < ell)
                ell = item.x;
            if(item.y < elt)
                elt = item.y;
        }
        //则左上角坐标为 ell.x , ell.y
        //再次扫描Data填入二维数组
        for(let i in this._Data){
            let item = this._Data[i];
            let ix = item.x - ell;
            let iy = item.y - elt;
            map[ix][iy] = [item.c,item];
        }
        console.log(this._Data);
        console.log(map);

        let queue = [];
        function pass(x, y) {
            if (x < 0 || x >= map.length || y < 0 || y >= map[0].length){
                //超界
                if(nowpt.c == null)
                    return 1;
                return 2;
            }
            for (let i in points) {
                if (points[i][0] == x && points[i][1] == y)
                    return 2;   //已存在
            }
            for (let i in queue) {
                if (queue[i][0] == x && queue[i][1] == y)
                    return 2;   //已存在
            }
            let c = map[x][y][0];
            if (c != nowpt.c) { //颜色不符
                return 2;
            } else {              //继续递归
                return 3;
            }
        }

        let l4 = [[1, 0], [-1, 0], [0, 1], [0, -1]];
        let l8 = [[1, 0], [-1, 0], [0, 1], [0, -1], [1, 1], [1, -1], [-1, 1], [-1, -1]];
        let lt = l8;
        if (nowpt.c == null) {
            lt = l4;
        }

        //使用新组成的Map进行扫描
        let bg = false;
        let x = pos.x - ell, y = pos.y - elt;
        queue.push([x, y]);
        while (queue.length != 0) {
            //扫描循环 找到延伸的n点放入队列
            let p = queue.pop();
            points.push(p);
            for (let i in lt) {
                let itm = lt[i];
                let r = pass(p[0] + itm[0], p[1] + itm[1]);
                if (r === 3) {
                    //继续递归
                    queue.push([p[0] + itm[0], p[1] + itm[1]]);
                } else if (r == 1) {
                    bg = true;
                    queue = [];
                    break;
                }
            }
        }
        if (bg) {
            this.setBackgroundColor(this.pencilColor);
        } else {
            let cover = this.pencilCover;
            this.pencilCover = true;
            for (let i in points) {
                let itm = points[i];
                this._setData(itm[0] + ell, itm[1]+elt, this.pencilColor);
            }
            this.pencilCover = cover;
        }
        opt._success({
            code: 400
        });
        this.OnChange();

    }

    /*
        油漆桶填充算法
        opt = {
            tx,     //屏幕中触发的位置
            ty
        }
    */
    bucket2(opt) {
        opt = opt || {};
        format.callback(opt);
        let pos = this.getPosByTouchPos(opt.tx, opt.ty);
        let nowpt = this._find(pos.x, pos.y);
        if (nowpt.c == this.pencilColor) {
            opt._success({
                code: 400
            });
            this.OnChange();
            return;
        }
        let points = [];        //已知点序列
        let that = this;
        function pass(x, y) {
            if (x < that._L - 1 || x > that._R + 1 || y < that._T - 1 || y > that._B + 1)
                return 1;   //超界

            for (let i in points) {
                let itm = points[i];
                if (itm[0] == x && itm[1] == y)
                    return 2;   //已存在
            }

            let p = that._find(x, y);
            if (p.c != nowpt.c) { //颜色不符
                return 2;
            } else {              //继续递归
                return 3;
            }
        }

        let l4 = [[1, 0], [-1, 0], [0, 1], [0, -1]];
        let l8 = [[1, 0], [-1, 0], [0, 1], [0, -1], [1, 1], [1, -1], [-1, 1], [-1, -1]];
        let lt = l8;
        if (nowpt.c == null) {
            lt = l4;
        }


        //待检测点位
        let bg = false;
        let x = pos.x, y = pos.y;
        let queue = [];
        queue.push([x, y]);
        while (queue.length != 0) {
            //扫描循环 找到延伸的n点放入队列
            let p = queue.pop();
            points.push(p);
            for (let i in lt) {
                let itm = lt[i];
                let r = pass(p[0] + itm[0], p[1] + itm[1]);
                if (r === 3) {
                    //继续递归
                    queue.push([p[0] + itm[0], p[1] + itm[1]]);
                } else if (r == 1) {
                    bg = true;
                    break;
                }
            }
        }
        if (bg) {
            this.setBackgroundColor(this.pencilColor);
        } else {
            let cover = this.pencilCover;
            this.pencilCover = true;
            for (let i in points) {
                let itm = points[i];
                this._setData(itm[0], itm[1], this.pencilColor);
            }
            this.pencilCover = cover;
        }
        opt._success({
            code: 400
        });
        this.OnChange();
    }

    /*
        寻找特定点位坐标
    */
    _find(x,y){
        for(let i in this._Data){
            let itm = this._Data[i];
            if(itm.x == x && itm.y == y)
                return itm;
        }
        return {c:null};
    }

    /*
        设置背景颜色
    */
    setBackgroundColor(c,record = true){
        if(record)
            this._record({
                type: 'bgc',
                bc: this._backgroundColor,
                ac: c
            });
        this._backgroundColor = c;
        this.OnChange();
    }


    /*
        获取保存数据

        {
            'data':'{...}', //点位数据字符串
        }
    */
    save(opt) {
        opt = opt || {};
        format.callback(opt);

        let data = {
            'data': this._Data,
            'view' : {
                x:this._x,
                y:this._y,
                px:this._px,
                l:this._L,
                t:this._T,
                r:this._R,
                b:this._B,
                bgc:this._backgroundColor
            },
            'guide':{
                'indexid':this._guideId,
                'data':this._gData,
                'guideStatus':this._guide
            }
        }
        opt._success({
            code:200,
            data:JSON.stringify(data)
        });
    }

    /*
        恢复数据
        opt={
            data
        }
    */
    load(opt) {
        opt = opt || {};
        format.callback(opt);
        let data = JSON.parse(opt.data);
        this._Data = data.data;
        this._x = data.view.x;
        this._y = data.view.y;
        this._px =data.view.px;
        this._L = data.view.l;
        this._R = data.view.r;
        this._T = data.view.t;
        this._B = data.view.b;
        this._backgroundColor = data.view.bgc;

        if (data['guide'] != null) {
            this._guide = data.guide.guideStatus;
            this._gData = data.guide.data;
            this._guideId = data.guide.indexid;
        }

        this.OnChange();
        opt._success();
    }

    /*
        辅助线模块
        this._guideId = 0;      //自增的索引
        this._guide = true;    //是否开启辅助线模式
        this._gData = [];       //辅助线数据
    */
    guideStatus(v = null){
        if(v===null){
            return this._guide;
        }
        this._guide = v;
        this.OnChange();
    }
    /*
        新增一个辅助线，并返回该辅助线的操作ID
    */
    createGuideLine(p,v){
        let id = this._guideId++;
        let off = p == 'h'? this._y-this._px : this._x;
        this._gData.push({
            'p':p,
            'v':this._calGuideLinePx(off+v),
            'id':id
        });
        return id;
    }

    /*
        移动一个辅助线
    */
    moveGuideLine(id,v){
        let item = null;
        for(let i in this._gData){
            if(this._gData[i].id == id){
                item = this._gData[i];
                break;
            }
        }
        if(item == null)
            return;
        let off = item.p == 'h' ? this._y - this._px  : this._x;
        item.v = this._calGuideLinePx(off+v);
        this.OnChange();
        return ;
    }
    
    _calGuideLinePx(v){
        return parseInt((v+this._px/2)/this._px);
    }

    /*
        排序
    */
    sortGuide(){
        for (let i = 0;i< this._gData.length;i++) {
            for(let j = i+1 ; j< this._gData.length ;j++){
                if (this._gData[i].v > this._gData[j].v){
                    let a = this._gData[i];
                    this._gData[i] = this._gData[j];
                    this._gData[j] = a;
                }
            }
        }
        this.OnChange();
    }

    /*
        拾取
    */
    getGuideLineId(x,y){
        //先检测水平线
        y += (this._y - this._px );
        let v = this._calGuideLinePx(y);
        console.log(v);
        let item = this._getGuideLine('h', v);
        console.log(item);
        if(item != null)
            return item;
        x += this._x;
        v = this._calGuideLinePx(x);
        item = this._getGuideLine('v',v);
        return item;
    }

    _getGuideLine(p,v){
        for(let i in this._gData){
            if (this._gData[i].p == p && this._gData[i].v == v)
                return this._gData[i];
        }
        return null;
    }

    /*
        删除
    */
    delGuide(id){
        for(let i = 0;i < this._gData.length;i++){
            if(this._gData[i].id == id){
                //found
                for(let j = i+1;j<this._gData.length;j++){
                    this._gData[j-1] = this._gData[j];
                }
                this._gData.pop();
                return ;
            }
        }
    }

    /*
        获取参考底图状态
    */
    photoStatus(s = null){
        if(s === null){
            if (this._photoData.length == 0)
                return {
                    has: false,
                    show: this._photoShow
                }
            return {
                has: true,
                show: this._photoShow
            }
        }else{
            this._photoShow = s;
            this.OnChange();
        }
    }

    /*
        设置像素参考图
        {
            data:res.data,
            height:res.height,
            width:res.width,
            x:data.l,
            y:data.t,
            type:0/1
        }
    */
    setPhotoData(opt) {
        opt = opt || {};
        format.callback(opt);
        this._photoData = [];
        this._photoWidth = opt.width;
        this._photoHeight = opt.height;
        let pos = this.getPosByTouchPos(opt.x, opt.y);
        pos.x++;
        pos.y++;
        this._photoX = pos.x;
        this._photoY = pos.y;
        if(opt.type == 0){ //非临摹
            this.stepSettle();
            let index = 0;
            let cover = this.pencilCover;
            this.pencilCover = true;
            for(let y =0;y<opt.height;y++){
                for(let x =0;x<opt.width;x++){
                    let color = opt.data[index + 3] == 255 ? 'rgb(' + opt.data[index++] + ',' + opt.data[index++] + ',' + opt.data[index++] + ')' : 'rgba(' + opt.data[index++] + ',' + opt.data[index++] + ',' + opt.data[index++] + ',' + opt.data[index+1] / 255 + ')';
                    index++;
                    this._setData(x + pos.x, y + pos.y, color,true);
                }
            }
            this.pencilCover = cover;
        }else if(opt.type == 1){ //临摹
            //转换为二维数组
            let index = 0;
            for (let y = 0; y < opt.height; y++) {
                this._photoData[y] = [];
                for (let x = 0; x < opt.width; x++) {
                    let color = [opt.data[index++], opt.data[index++], opt.data[index++], opt.data[index++]/255];
                    let rgb ='';
                    if(color[3] == 1)
                        rgb = 'rgb('+color[0]+','+color[1]+','+color[2]+')';
                    else
                        rgb = 'rgba(' + color[0] + ',' + color[1] + ',' + color[2] + ','+color[3]+')';
                        color.push(rgb);
                        
                    this._photoData[y].push(color);
                }

            }
            this._photoShow = true;
        }
        this.OnChange();
        opt._success();
    }

}