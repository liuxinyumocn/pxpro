import El from '../Base/El.js';

export default class Grid extends El{
    //网格线的宽度不收放大缩小控制

    constructor() {
        super();
        this._lineColor = '#c8c8c8';    //线条颜色
        this._px = 10;              //格子宽度
        this._width = 10;           //视野宽度
        this._height = 10;          //视野高度
        this._offsetx = 0;          //左偏移量
        this._offsety = 0;          //右偏移量

        this._hnum = 0; //水平线数量
        this._vnum = 0; //垂直线数量

        this._lineWidth = 0.3; //不支持变更

    }

    /*
        设置表现数据
        opt = {
            lineColor :
            px
            width
            height
            offsetx
            offsety
        }
    */
    setData(opt) {
        if (opt.lineColor)
            this._lineColor = opt.lineColor;
        if (opt.px)
            this._px = opt.px;
        if (opt.width)
            this._width = opt.width;
        if (opt.height)
            this._height = opt.height;
        if (opt.offsetx)
            this._offsetx = opt.offsetx;
        if (opt.offsety)
            this._offsety = opt.offsety;
        this._offsetx = this._offsetx % this._px;
        this._offsety = this._offsety % this._px;
        this._vnum = parseInt(this._width/this._px) +3;
        this._hnum = parseInt(this._height/this._px) +3;
    }

    Draw(ctx){
        if(this._px <= 2)
            return true;
        ctx.beginPath();
        ctx.lineWidth = this._lineWidth;
        for (let i = 0; i < this._vnum;i++) { //垂直线数量
            let x = (i-1) * this._px - this._offsetx;
            ctx.moveTo(x,0);
            ctx.lineTo(x,this._height);
        } 
        for (let i = 0; i < this._hnum; i++) { //水平线数量
            let y = (i - 1) * this._px - this._offsety;
            ctx.moveTo(0, y);
            ctx.lineTo(this._width, y);
        }
        ctx.setStrokeStyle(this._lineColor);
        ctx.stroke();
        //ctx.draw(true);
        return true;
    }
}