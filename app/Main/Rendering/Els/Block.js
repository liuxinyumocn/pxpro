import El from '../Base/El.js';

export default class Block extends El {

    constructor() {
        super();

        /*
            对象格式应为：
            {
                x:0,
                y:0,
                c:'#ccc'
            }
        */
        this._data = [];    //格子序列
        this._px = 10;      //格子宽度

        this._offsetx = 0;          //水平偏移量
        this._offsety = 0;          //垂直偏移量
    }

    /*
        设置表现数据
        opt = {
            data:  格子序列
            px
            offsetx:
            offsety
        }
    */
    setData(opt) {
        this._data = opt.data;
        this._px = opt.px;
        this._offsety = opt.offsety;
        this._offsetx = opt.offsetx;
    }

    Draw(ctx) {
        let wd = this._px+0.25;
        for(let i in this._data){
            let itm = this._data[i];
            ctx.setFillStyle(itm.c);
            let x1,y1;
            x1 = itm.x * this._px - this._offsetx;
            y1 = itm.y * this._px - this._offsety;

            ctx.fillRect(x1, y1, wd, wd);
        }
        //ctx.draw(true);
        return true;
    }
}