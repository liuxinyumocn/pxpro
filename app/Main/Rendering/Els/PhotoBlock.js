import El from '../Base/El.js';

export default class PhotoBlock extends El {

    constructor() {
        super();

        this._data = [];
        this._px = 0;
        this._offsety = 0;
        this._offsetx = 0;
        this._w = 0;
        this._h = 0;
        this._posX = 0;
        this._posY = 0;
    }

    /*
        设置表现数据
        opt = {
            data:arr2,
            w:this._photoWidth,
            h:this._photoHeight,
            px: this._px,
            offsetx: this._x,
            offsety: this._y,
            posX:this._photoX,
            posY:this._photoY
        }
    */
    setData(opt) {
        if(opt.data.length == 0)
        {
            this._visible = false;
            return;
        }else
            this._visible = opt.show;

        this._data = opt.data;
        this._px = opt.px;
        this._offsety = opt.offsety;
        this._offsetx = opt.offsetx;
        this._w = opt.w;
        this._h = opt.h;
        this._posX = opt.posX;
        this._posY = opt.posY;
    }

    Draw(ctx) {
        let wd = this._px *0.8;
        let owd = this._px *0.1;

        for (let y = 0; y < this._h; y++) {
            for (let x = 0; x < this._w; x++) {
                let c = this._data[y][x];
                ctx.setFillStyle(c[4]);
                let x1, y1;
                x1 = (x + this._posX) * this._px - this._offsetx + owd;
                y1 = (y + this._posY) * this._px - this._offsety + owd;
                ctx.fillRect(x1, y1, wd, wd);
            }
        }

        return true;
    }
}