import El from '../Base/El.js';

export default class BackgroundColor extends El {

    constructor(w=1,h=1) {
        super();
        this._color = null;
        this._w = w;
        this._h = h;
    }

    setColor(color = null) {
        this._color = color;
    }

    getColor(){
        return this._color;
    }

    Draw(ctx) {
        if(this._color != null){
            ctx.setFillStyle(this._color);
            ctx.fillRect(0,0,this._w,this._h);
            //ctx.draw(true);
        }
        return true;
    }
}