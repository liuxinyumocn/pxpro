import El from '../Base/El.js';
import format from '../../../lib/format.js';

export default class CenterPhoto extends El {

    constructor(w, h) {
        super();

        this._w = w;
        this._h = h;

        this._picw = 0;
        this._pich = 0;

        this._config = {
            w:1,
            h:1,
            l:1,
            t:1,
            path:''
        }
       
    }

    getData(){
        return this._config;
    }

    loadPhoto(opt) {
        opt = opt || {};
        format.callback(opt);
        wx.getImageInfo({
            src: opt.url,
            success:(res)=>{
                this._picw = res.width;
                this._pich = res.height;

                //计算成像位置
                if(this._picw/this._pich > this._w / this._h){
                    //图像宽度高 (横向理解)
                    let w = this._w *0.85;
                    let h = this._pich * w / this._picw;
                    this._config = {
                        w: w,
                        h: h,
                        l:(this._w - w)/2,
                        t:(this._h-h)/2,
                        path:opt.url
                    }
                } else {
                    //图像高度高 (纵向理解)
                    let h = this._h * 0.85;
                    let w = this._picw * h / this._pich;
                    this._config = {
                        w: w,
                        h: h,
                        l: (this._w - w) / 2,
                        t: (this._h - h) / 2,
                        path: opt.url
                    }
                }
                opt._success();
            },
            fail:(res)=>{
                opt._fail({
                    code:400,
                    errmsg:'图像无法识别'
                });
            }
        })
    }

    Draw(ctx) {
        ctx.drawImage(this._config.path, this._config.l, this._config.t, this._config.w, this._config.h);
    }
}