import El from '../Base/El.js';

export default class Guide extends El {
    //网格线的宽度不收放大缩小控制

    constructor(w,h) {
        super();
        this._config = {
            'lineColor': 'rgb(255,141,253)',     //线条颜色
            'px': 10,                   //格子宽度
            'width': 10,                //视野宽度
            'height': 10,               //视野高度
            'ruleWidth': 20,            //标尺宽度
            'ruleColor': 'white',         //标尺颜色
            'lineWidth':1,              //线条宽度
            'x':0,
            'y':0
        }
        this._config['width'] = w;
        this._config['height'] = h;

        this._data = [];
    }

    /*
        设置表现数据
        opt = {
            data:this._gData,
            x:this._x,
            y:this._y,
            px
        }
        data =
            {
                p:'h/v',    //水平或垂直
                v:123,      //水平或垂直的偏移量
                id:1
            }
    */
    setData(opt){
        this._config['px'] = opt.px;
        this._config['x'] = opt.x;
        this._config['y'] = opt.y;
        let data = [];
        for(let i in opt.data){
            let a = {
                p: opt.data[i].p,
                v: opt.data[i].v * opt.px,
                px:opt.data[i].v,
                id: opt.data[i].id
            }
            data.push(a);
        }
        this._data = data;
    }


    setAttr(opt) {
        for (let i in opt) {
            this._config[i] = opt[i];
        }
    }

    Draw(ctx) {
        ctx.setShadow(0, 0, 10, '#d3d3d3');
        ctx.setFillStyle(this._config['ruleColor']);
        ctx.fillRect(0, 0, this._config['width'], this._config['ruleWidth']);
        ctx.fillRect(this._config['width']-this._config['ruleWidth'], 0, this._config['ruleWidth'], this._config['height']);
        ctx.setShadow(0, 0, 0, 'rgba(0,0,0,0)');
        ctx.fillRect(0, 0, this._config['width'], this._config['ruleWidth']);

        //绘制辅助线
        ctx.lineWidth = this._config['lineWidth'];
        ctx.beginPath();
        ctx.setShadow(0, 0, 2, '#d3d3d3');

        //计算出当前视野中包含的辅助线
        let v = [] , h = [];
        let l = this._config['x'],
            r = l+this._config['width'],
            t = this._config['y'],
            b=this._config['height']+t;

        for(let i in this._data){
            let item = this._data[i];
            if(item.p == 'v'){   //垂直
                    v.push(item);
            } else {  //水平
                    h.push(item);
            }
        }

        //计算文字位置
        let vt = [],ht = [];

        ctx.setFillStyle(this._config['lineColor']);
        ctx.setFontSize(12);
        for (let i = 1; i < v.length; i++) {
            let text = (v[i].px - v[i - 1].px) + 'px';
            let w = ctx.measureText(text).width;
            let pxw = v[i].v - v[i - 1].v;
            let visible = w + 10 > pxw ? false : true;
            if (!visible)
                continue;
            vt.push({
                text: text,
                v: (pxw - w) / 2 + v[i - 1].v - l
            });
        }
        for (let i = 1; i < h.length; i++) {
            let text = (h[i].px - h[i - 1].px) + 'px';
            let w = ctx.measureText(text).width;
            let pxw = h[i].v - h[i - 1].v;
            let visible = w + 10 > pxw ? false : true;
            if (!visible)
                continue;
            ht.push({
                text: text,
                v: (pxw - w) / 2 + h[i - 1].v - t
            });
        }


        for (let i in v) {
            if (v[i].v < l || v[i].v > r-this._config['ruleWidth'])
                continue;
            ctx.moveTo(v[i].v-l, 0);
            ctx.lineTo(v[i].v-l, this._config['height']);
        }

        for (let i in h) {
            if (h[i].v < t + this._config['ruleWidth'] || h[i].v > b)
                continue;
            ctx.moveTo(0, h[i].v-t);
            ctx.lineTo(this._config['width'], h[i].v-t);
        }

        ctx.setStrokeStyle(this._config['lineColor']);
        ctx.stroke();
        ctx.setShadow(0, 0, 0, 'rgba(0,0,0,0)');

        if(vt.length != 0){
            for (let i in vt) {
                ctx.fillText(vt[i].text, vt[i].v, 13);
            }
        }

        if(ht.length != 0){
            ctx.rotate(Math.PI / 2);
            for (let i in ht) {
                ctx.fillText(ht[i].text, ht[i].v, 13-this._config['width']);
            }
            ctx.rotate(Math.PI / -2);
        }

        //ctx.draw(true);
        return true;
    }
}