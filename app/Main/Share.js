import format from '../lib/format.js';
import config from '../config.js';
import vip from '../user/vip/vip.js';

export default class Share{

    constructor(canvasid){
        this._canvasid = canvasid;
        this._ctx = wx.createCanvasContext(canvasid,this);

    }

    /*
        生成图像数据
         opt={
            imgwidth:123,
            imgheight:123,
        }
    */
    getPhoto(opt) {
        opt = opt || {};
        format.callback(opt);
        console.log(this._ctx);
        console.log(this._ctx.draw);
        this._ctx.draw(true);
        setTimeout(function () {
            wx.canvasToTempFilePath({
                canvasId: this._canvasid,
                x: 0,
                y: 0,
                width: opt.imgwidth,
                height: opt.imgheight,
                fileType: 'jpg',
                quality: 1,
                success: (res) => {
                    opt._success(res);
                },
                fail: (res) => {
                    console.log(res);
                }
            }, this)
        }.bind(this), 300);
    }

    /*
        生成分享卡片

        opt={
            username:'',
            avatar:'',
            imgwidth:123,
            imgheight:123,
            imgData
        }

    */
    genCard(opt) {
        opt = opt || {};
        format.callback(opt);
        console.log(opt);
        //添加其他元素
        //预加载图像
        this._loadImg({
            urls:[
                opt.avatar,
                config.pxpro + 'mini/share/gen/id/' + getApp().data.user.getID(),
                '/app/src/water.png'
            ],
            success:(res)=>{
                //用户头像
                let avatar_w = opt.imgwidth * 0.085;
                this._ctx.drawImage(res[0].tempFilePath, opt.imgwidth * 0.03175, opt.imgheight / 2 - avatar_w / 2, avatar_w, avatar_w);
                //分享二维码
                let qd = avatar_w * 1.3;
                this._ctx.drawImage(res[1].tempFilePath, opt.imgwidth * 0.36, opt.imgheight / 2 + avatar_w /3, qd, qd);

                let vipd = vip.getVIPData();
                if (!vipd.vip || vipd.expdes[0] < 2){
                    //水印
                    let w = opt.imgwidth * 0.2;
                    let h = w * 0.199;
                    this._ctx.drawImage(res[2].url, opt.imgwidth * 0.03175, opt.imgheight * 0.15, w, h);
                    this._ctx.drawImage(res[2].url, opt.imgwidth * 0.254, opt.imgheight * 0.35, w, h);
                }
            
                //文字内容
                this._ctx.setFontSize(23);
                this._ctx.setFillStyle('#000');
                this._ctx.fillText(opt.nickname, opt.imgwidth * 0.03175 + avatar_w * 1.1, opt.imgheight / 2 + avatar_w*0.15);

                this._ctx.setFontSize(13);
                this._ctx.setFillStyle('#000');
                this._ctx.fillText(this.getText(), opt.imgwidth * 0.03175, opt.imgheight / 2 + avatar_w * 1.1);

                this._ctx.draw(true);
                setTimeout(function () {
                    wx.canvasToTempFilePath({
                        canvasId: this._canvasid,
                        x: 0,
                        y: 0,
                        width: opt.imgwidth,
                        height: opt.imgheight + avatar_w * 4,
                        fileType: 'jpg',
                        quality: 1,
                        success: (res) => {
                            opt._success(res);
                        },
                        fail: (res) => {
                            console.log(res);
                        }
                    }, this)}.bind(this),300);
            },
            fail:(res)=>{

            }
        });
    }

    getText(){
        let a = [
            '在这张没有限制的画布上，尽情发挥吧！',
            '艺术家们！来这里尽情发挥想象吧！',
            '来《像素大师》欣赏艺术家们的创作。',
            '人人都是艺术家，快来！'
        ];
        return a[parseInt(a.length * (Math.random()-0.00001))];
    }

    /*
        预加载所有所需图像资源
        opt={
            urls:[
                '',''
            ]
        }
    */
    _loadImg(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.urls = opt.urls || [];
        let finished = 0;
        let data = [];
        for(let i=0 ;i<opt.urls.length;i++){
            let item = opt.urls[i];
            data.push({
                url: item,
                tempFilePath: '',
                res: null
            });
            wx.getImageInfo({
                src: item,
                success:(res)=>{
                    data[i].res = res;
                    data[i].tempFilePath = res.path;
                    finished++;
                    if(finished == opt.urls.length){
                        opt._success(data);
                    }
                },
                fail:(res)=>{
                    console.log(res);
                    opt._fail({
                        code:400,
                        errmsg:'加载失败请稍后重试'
                    });
                }
            })
        }

    }
}