import format from '../lib/format.js';
/*
    文件系统
*/

export default class File{

    constructor(dir){   //提供一个独占的目录头
        this._fileManager = wx.getFileSystemManager();

        this._dir = wx.env.USER_DATA_PATH+'/'+dir;

        this._fileManager.access({
            path:this._dir,
            success:(res)=>{
                //console.log('已存在独占目录');
            },
            fail: (res) => {
                //创建目录
                this._fileManager.mkdir({
                    dirPath:this._dir,
                    recursive:true,
                    success:(res)=>{
                       //console.log('独占目录创建成功') 
                    }
                });
            }
        });
    }

    //读取目录列表
    readDir(opt) {
        opt = opt || {};
        format.callback(opt);
        this._fileManager.readdir({
            dirPath:this._dir,
            success:(res)=>{
                opt._success(res);
            },
            fail: (res) => {
                opt._fail(res);
            }
        });
    }

    //获取文件数量
    getCount(opt) {
        opt = opt || {};
        format.callback(opt);
        this._fileManager.readdir({
            dirPath: this._dir,
            success: (res) => {
                opt._success(res.files.length);
            },
            fail: (res) => {
                opt._fail(res);
            }
        });
    }

    //getInfo
    getInfo(opt) {
        opt = opt || {};
        format.callback(opt);
        this._fileManager.getFileInfo({
            filePath: this._dir+'/'+opt.file,
            success: (res) => {
                opt._success(res);
            },
            fail: (res) => {
                opt._fail(res);
            }
        });
    }

    /*
        读取文件
        opt={
            file:'a.txt',
            absolute:false
        }
    */
    readFile(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.absolute = opt.absolute || false;
        let path = opt.absolute ? opt.file : this._dir + '/' + opt.file;
        this._fileManager.access({
            path: path,
            success: (res) => {
                this._fileManager.readFile({
                    filePath: path,
                    encoding: 'utf8',
                    success: (res) => {
                        opt._success({
                            code: 200,
                            data:res.data
                        });
                    }
                });
            },
            fail: (res) => {
                opt._fail({
                    code:400,
                    errmsg:'文件不存在'
                });
            }
        });
    }

    /*
        移动文件
        opt={
            oldPath
            newPath
        }
    */
    moveFile(opt) {
        opt = opt || {};
        format.callback(opt);
        this._fileManager.rename({
            oldPath: this._dir + '/'+opt.oldPath,
            newPath: this._dir + '/'+opt.newPath,
            success: (res) => {
                opt._success({
                    code: 200,
                    data: res.data
                });
            },
            fail: (res) => {
                opt._fail({
                    code: 400,
                    errmsg: '重命名失败'
                });
            }
        })
    }


    /*
        删除文件
    */
    del(opt) {
        opt = opt || {};
        format.callback(opt);
        let path = this._dir + '/' + opt.file;
        this._fileManager.access({
            path: path,
            success: (res) => {
                //删除文件
                this._fileManager.unlink({
                    filePath: path,
                    success: (res) => {
                        opt._success({
                            code:200
                        });
                    },
                    fail: (res) => {
                        opt._fail({
                            'code': 400,
                            'errmsg': '失败'
                        });
                    }
                });
            },
            fail: (res) => {
                opt._fail({
                    code:400,
                    errmsg:'文件不存在'
                });
            }
        });
    }

    /*
        保存文件
    */
    saveFile(opt) {
        opt = opt || {};
        format.callback(opt);
        let path = this._dir + '/' + opt.file;
        this._fileManager.access({
            path: path,
            success: (res) => {
                //删除文件
                this._fileManager.unlink({
                    filePath: path,
                    success:(res)=>{
                        this._saveFile(opt);
                    },
                    fail:(res)=>{
                        opt._fail({
                            'code':400,
                            'errmsg':'失败'
                        });
                    }
                });
            },
            fail: (res) => {
                this._saveFile(opt);
            }
        });
    }

    _saveFile(opt) {
        opt = opt || {};
        format.callback(opt);
        let path = this._dir + '/' + opt.file;
        this._fileManager.writeFile({
            filePath:path,
            data:opt.data,
            encoding:'utf8',
            success:(res)=>{
                opt._success({
                    code:200,
                    path:path
                });
            },
            fail:(res)=>{
                opt._fail({
                    code:400,
                    errmsg:'保存失败'
                });
            }
        });

    }

}