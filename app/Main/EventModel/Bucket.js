/*
    Bucket 控制模型
*/

export default class Bucket {

    constructor(editer) {
        this._editer = editer;
        this._accessEvent = false;
        this._lt = null;

        this._touchnum = 0;
        this._op = {  //参考点
            x: 0,
            y: 0
        }
        this._dis = 0;  //参考距离

        this._accessBucket = false;
        this._bucketing = false;
    }

    start(touches) {
        if (touches.length == 1) {    //单点触控
            this._accessBucket = true;
            this._accessEvent = true;
            this._op = touches[0];
            this._touchnum = touches.length;
        } else if (touches.length == 2) {  //两点触控
            this._accessBucket = false;
            this._op = {
                x: (touches[0].x + touches[1].x) / 2,
                y: (touches[0].y + touches[1].y) / 2
            }
            let a = touches[0].x - touches[1].x;
            let b = touches[0].y - touches[1].y;
            this._dis = Math.sqrt(a * a + b * b);
        } else {
            this._accessEvent = false;
        }
    }

    end(touches) {
        //console.log(this._accessEvent,this._accessBucket);
        this._accessEvent = false;
        this._lt = null;
        if(touches.length == 2){
            this._accessBucket = false;
        }
        if (touches.length == 1 && this._accessBucket){
            this._editer.stepSettle();  //结算此前步骤
            this._bucket(touches[0]);
        }
        return;
    }

    move(touches) {
        if (!this._accessEvent)
            return;
        if (touches.length == 1) {    //单点触控
            this._accessBucket=false;
            let offset = {
                x: touches[0].x - this._op.x,
                y: touches[0].y - this._op.y
            }
            this._op = touches[0];
            this._editer.setViewData({
                offset: offset
            });
        } else if (touches.length == 2) {
            //缩放
            let a = touches[0].x - touches[1].x;
            let b = touches[0].y - touches[1].y;
            let dis = Math.sqrt(a * a + b * b);
            let scale = dis / this._dis;
            scale = (scale - 1) * 0.5 + 1;
            this._dis = dis;
            //移动
            let _op = {
                x: (touches[0].x + touches[1].x) / 2,
                y: (touches[0].y + touches[1].y) / 2
            }
            let offset = {
                x: (_op.x - this._op.x) * scale,
                y: (_op.y - this._op.y) * scale
            }
            this._op = _op;
            this._editer.setViewData({
                offset: offset,
                scale: {
                    x: _op.x,
                    y: _op.y,
                    scale: scale
                }
            });
        } else {
            this._accessEvent = false;
        }
    }

    cancel(touches) {
        
    }

    _bucket(touch) {
        if(this._bucketing){
            wx.showLoading({
                title: '请稍后',
            })
            return ;
        }
        wx.showLoading({
            title: '正在填充',
        })
        this._editer.bucket2({
            tx: touch.x,
            ty: touch.y,
            success:(res)=>{
                this._bucketing = false;
                wx.hideLoading();
            }
        });
    }

}