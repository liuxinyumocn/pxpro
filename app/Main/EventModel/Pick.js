/*
    Pick 控制模型
*/

export default class Pick {

    constructor(editer, opt) {
        this._editer = editer;
        this._opt = opt;
        
        this._accessEvent = false;
        this._touchnum = 0;
        this._op = {  //参考点
            x: 0,
            y: 0
        }
        this._dis = 0;  //参考距离

    }

    start(touches) {
        this._accessEvent = true;
        this._touchnum = touches.length;
        if (touches.length == 1) {    //单点触控
            this._op = touches[0];
            this._pick(touches[0]);
        } else if (touches.length == 2) {  //两点触控
            this._op = {
                x: (touches[0].x + touches[1].x) / 2,
                y: (touches[0].y + touches[1].y) / 2
            }
            let a = touches[0].x - touches[1].x;
            let b = touches[0].y - touches[1].y;
            this._dis = Math.sqrt(a * a + b * b);
        } else {

        }
    }

    end(touches) {
        this._accessEvent = false;
        return;

    }

    move(touches) {
        if (!this._accessEvent)
            return;
        if (touches.length == 1) {    //单点触控
            let offset = {
                x: touches[0].x - this._op.x,
                y: touches[0].y - this._op.y
            }
            this._op = touches[0];
            this._editer.setViewData({
                offset: offset
            });
        } else if (touches.length == 2) {
            //缩放
            let a = touches[0].x - touches[1].x;
            let b = touches[0].y - touches[1].y;
            let dis = Math.sqrt(a * a + b * b);
            let scale = dis / this._dis;
            scale = (scale - 1) * 0.5 + 1;
            this._dis = dis;
            //移动
            let _op = {
                x: (touches[0].x + touches[1].x) / 2,
                y: (touches[0].y + touches[1].y) / 2
            }
            let offset = {
                x: (_op.x - this._op.x) * scale,
                y: (_op.y - this._op.y) * scale
            }
            this._op = _op;
            this._editer.setViewData({
                offset: offset,
                scale: {
                    x: _op.x,
                    y: _op.y,
                    scale: scale
                }
            });
        } else {

        }
    }

    cancel(touches) {

    }

    _pick(touch) {
        this._editer.pick({
            tx: touch.x,
            ty: touch.y,
            success: (res) => {
                this._opt._success(res);
            },
            fail:(res)=>{
                this._opt._fail(res);
            }
        });
    }
}