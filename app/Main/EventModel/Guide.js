/*
    辅助线 控制模型

    从上标尺开始发生move行为则创建水平辅助线，将水平辅助线拖拽至上标尺则删除辅助线
    右侧标尺同理

*/

export default class Guide {

    constructor(editer) {
        this._editer = editer;

        this._accessEvent = false;  //是否开始本意行为
        
        this._width = editer._width;
        this._height = editer._height;

        this._ruleWidth = 20;

        this._currentIndex = 0; //当前操作线ID

        this._p = 'h';

    }

    start(touches) {
        if (touches.length == 1) {    //单点触控
            //判断是否是新增起点
            if (touches[0].y <= this._ruleWidth && touches[0].y > 0) {
                this._accessEvent = true;
                //新增水平辅助线
                this._p = 'h';
                this._currentIndex = this._editer.createGuideLine('h',touches[0].y);
            } else if (touches[0].x >= this._width - this._ruleWidth && touches[0].x <= this._width) {
                this._accessEvent = true;
                //新增垂直辅助线
                this._p = 'v';
                this._currentIndex = this._editer.createGuideLine('v', touches[0].x);
            }else{
                //检测是否采集到已有辅助线上
                let re = this._editer.getGuideLineId(touches[0].x,touches[0].y);
                if(re != null){
                    this._accessEvent = true;
                    this._p = re.p;
                    this._currentIndex = re.id;
                }
            }
        } else if (touches.length == 2) {  //两点触控
           
        } else {

        }
    }

    end(touches) {
        if(this._accessEvent == true){
            //判断删除
            if(this._p == 'h'){
                if (touches[0].y <= this._ruleWidth && touches[0].y > 0){
                    //删除
                    this._editer.delGuide(this._currentIndex);
                }
            } else if (this._p == 'v') {
                if (touches[0].x >= this._width - this._ruleWidth && touches[0].x <= this._width) {
                    //删除
                    this._editer.delGuide(this._currentIndex);
                }
            }
        }

        this._accessEvent = false;
        //排序
        this._editer.sortGuide();
        return;

    }

    move(touches) {
        if(!this._accessEvent)
            return;
        if (touches.length == 1) {    //单点触控
            let v = this._p == 'h' ? touches[0].y : touches[0].x;
            this._editer.moveGuideLine(this._currentIndex, v);
        } else if (touches.length == 2) {
            
        } else {

        }
    }

    cancel(touches) {

    }

}