/*
    Pencil 控制模型

    单手

*/

export default class Pencil {

    constructor(editer) {
        this._editer = editer;
        this._accessEvent = false;
        this._lt = null;

        this._needback = false;

        this._op = {  //参考点
            x: 0,
            y: 0
        }
        this._dis = 0;  //参考距离
    }

    start(touches) {
        if (touches.length == 1) {    //单点触控
            this._accessEvent = true;
            this._editer.stepSettle();  //结算此前步骤
            this._pencil(touches[0]);
            this._needback = true;
        } else if (touches.length == 2) {  //两点触控
            if(this._needback)
                this._editer.back();
            this._op = {
                x: (touches[0].x + touches[1].x) / 2,
                y: (touches[0].y + touches[1].y) / 2
            }
            let a = touches[0].x - touches[1].x;
            let b = touches[0].y - touches[1].y;
            this._dis = Math.sqrt(a * a + b * b);
        } else {
            this._accessEvent = false;
        }
    }

    end(touches) {
        this._accessEvent = false;
        this._lt = null;
        this._needback = false;
        return;
    }

    move(touches) {
        if (!this._accessEvent)
            return;
        if (touches.length == 1) {    //单点触控
            this._pencil(touches[0]);
        } else if (touches.length == 2) {
            //缩放
            let a = touches[0].x - touches[1].x;
            let b = touches[0].y - touches[1].y;
            let dis = Math.sqrt(a * a + b * b);
            let scale = dis / this._dis;
            scale = (scale - 1) * 0.5 + 1;
            this._dis = dis;
            //移动
            let _op = {
                x: (touches[0].x + touches[1].x) / 2,
                y: (touches[0].y + touches[1].y) / 2
            }
            let offset = {
                x: (_op.x - this._op.x) * scale,
                y: (_op.y - this._op.y) * scale
            }
            this._op = _op;
            this._editer.setViewData({
                offset: offset,
                scale: {
                    x: _op.x,
                    y: _op.y,
                    scale: scale
                }
            });
        } else {
            this._accessEvent = false;
        }
    }

    cancel(touches) {

    }

    _pencil(touch){
        this._editer.setData({
            tx:touch.x,
            ty:touch.y,
            lt: this._lt,
            success:(res)=>{
                this._lt = res;
            }
        });
    }

}