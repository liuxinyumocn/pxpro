import format from '../lib/format.js';
import Rendering from 'Rendering.js';
import Editer from 'Editer.js';
import File from 'File.js';
import vip from '../user/vip/vip.js';

//Rendering Els
import Grid from 'Rendering/Els/Grid.js';
import Block from 'Rendering/Els/Block.js';
import BackgroundColor from 'Rendering/Els/BackgroundColor.js';
import Guide from 'Rendering/Els/Guide.js';
import CenterPhoto from 'Rendering/Els/CenterPhoto.js';
import PhotoBlock from 'Rendering/Els/PhotoBlock.js';

//Event Model
import Hand from 'EventModel/Hand.js';      //手型交互模式
import Pencil from 'EventModel/Pencil.js';  //铅笔交互模式
import Eraser from 'EventModel/Eraser.js';  //橡皮交互模式
import Bucket from 'EventModel/Bucket.js';  //油漆桶交互模式
import GuideModel from 'EventModel/Guide.js';    //辅助线交互模式
import Pick from 'EventModel/Pick.js';      //取色器交互模式

export default class App{

    /*
        opt = {
            canvasid,

            //创作者信息
            author:{
                author:'作者昵称',
                avatar:'头像地址',
                sex:0/1/2,  //性别
            },
            file:''    //本地文件路径
            tempfile:'' //临时文件路径
        }
    */
    constructor(opt){
        wx.showLoading({
            title: '请稍后',
        })
        this._Editer = null;
        this._Rendering = null;
        this._File = new File('localworks');

        //场景基本元素
        this._RBrid = null;
        this._RBlock = null;
        this._RPhotoBlock = null;
        this._RBackground = null;
        this._RGuide = null;
        this._RCenterPhoto = null;

        //交互模式
        this._EventModel = null;

        //作者信息
        this._authorInfo = {
            path:'',
            author:'',
            createtimestamp:0,
            edittimestamp:0,
            sex:0,
            avatar:''
        }

        //参考图导入模式
        this._phototype = 0;

        this.init(opt);
    }

    init(opt){
        opt = opt || {};
        format.callback(opt);
        opt.filePath = opt.filePath || '';
        opt.tempfile = opt.tempfile || '';
        //初始化设计器
        this._Editer = new Editer();
        this._Editer.OnChange = this._EditerOnChange.bind(this);
        this._Rendering = new Rendering(opt.canvasid, opt.shareCanvas);
        this._Rendering.OnLoad = function () {
            this._Editer._width = this._Rendering._width;
            this._Editer._height = this._Rendering._height;

            //创建场景基本元素
            this._RBrid = new Grid();
            this._RBlock = new Block();
            this._RPhotoBlock = new PhotoBlock();
            this._RBackground = new BackgroundColor(this._Rendering._width,this._Rendering._height);
            this._RGuide = new Guide(this._Rendering._width, this._Rendering._height);
            this._RCenterPhoto = new CenterPhoto(this._Rendering._width, this._Rendering._height);

            //设置可视状态
            this._RBrid.Visible(true);
            this._RBlock.Visible(true);
            this._RBackground.Visible(true);
            this._RGuide.Visible(true);
            this._RCenterPhoto.Visible(false);
            this._RPhotoBlock.Visible(false);

            this._Rendering.push(this._RBackground, this._RCenterPhoto, this._RPhotoBlock,this._RBlock, this._RBrid,this._RGuide);
            setTimeout(this._Editer.OnChange.bind(this),200);

            this._recoverEdit(opt);

        }.bind(this);
        this._Rendering.OnRenderBegin = this._onRenderBegin.bind(this);

        //设置当前交互模式为手型交互模式
        this.handModel();

    }

    /*
        渲染前数据准备
    */
    _onRenderBegin(){
        //网格设置
        this._Editer.getViewData({
            success:(res)=>{
                this._RBrid.setData({
                    px: res.px,
                    width: this._Rendering._width,
                    height: this._Rendering._height,
                    offsetx: res.brid.x,
                    offsety: res.brid.y
                });
                this._RBlock.setData({
                    data: res.block.data,
                    px:res.px,
                    offsetx: res.block.offsetx,
                    offsety: res.block.offsety
                });
                this._RPhotoBlock.setData(res.photo);
                this._RBackground.setColor(res.backgroundColor);
                if(res.guide !== null){
                    this._RGuide.Visible(true);
                    this._RGuide.setData(res.guide);
                }else{
                    this._RGuide.Visible(false);
                }
            }
        });
        
        //方块设置
    
    }

    /*
        恢复异常退出的临时文件数据
    */
    _recoverEdit(opt){
        let file = 'TempProject.pxpro';
        let absolute = false;
        if (opt.tempfile != ''){
            absolute = true;
            file = opt.tempfile;
        }else if (opt.file != '' && opt.file != file) {  //打开工程
            file = opt.file;
            this._authorInfo.path = file;
        }
        this._File.readFile({
            file: file,
            absolute: absolute,
            success: (res) => {
                let data = JSON.parse(res.data);
                this._Editer.load({
                    data: data.editer,
                    success: (res) => {
                        wx.hideLoading();
                        opt._success();
                    },
                    fail: (res) => {

                    }
                });
            },
            fail: (res) => {
                wx.hideLoading();
            }
        });
    }

    /*
        Editer发生行为变化
    */
    _EditerOnChange(){
        this._Rendering.draw();
    }

    /*
        用户交互
    */
    touchstart(touches) {
        this._EventModel.start(touches);
    }

    touchend(touches) {
        this._EventModel.end(touches);
    }

    touchmove(touches) {
        this._EventModel.move(touches);
    }

    touchcancel(touches) {
        this._EventModel.cancel(touches);
    }
    handModel(){
        this._EventModel = new Hand(this._Editer);
    }
    pencilModel(){
        this._EventModel = new Pencil(this._Editer);
    }
    pickModel(opt) {
        opt = opt || {};
        format.callback(opt);
        this._EventModel = new Pick(this._Editer,opt);
    }
    eraserModel() {
        this._EventModel = new Eraser(this._Editer);
    }
    bucketModel(){
        this._EventModel = new Bucket(this._Editer);
    }
    guideModel(){
        this._EventModel = new GuideModel(this._Editer);
    }
    editBack(opt){
        this._Editer.back(opt);
    }

    /*
        设置
    */
    setPencilColor(rgb){
        this._Editer.pencilColor = rgb;
    }

    setPencilCover(status){
        this._Editer.pencilCover = status;
    }

    trash(){   //清空画布
        this._Editer.trash();
        this._RCenterPhoto.Visible(false);
        this._EditerOnChange();
    }

    suitview(opt) { //最适视野
        this._Editer.suitview(opt);
    }

    //删除本地文件
    delLocalFile(opt) {
        opt = opt || {};
        format.callback(opt);
        if(this.isTemporary()){
            opt._success({
                code:200
            });
            return ;
        }
        let path = this._authorInfo.path;
        opt.file = path;
        this._File.del({
            file:path,
            success: (res) => {
                this._authorInfo.path = '';
                opt._success(res);
            },
            fail:(res)=>{
                opt._fail(res);
            }
        });
    }
    /*
        文件
        {
            'title':'',
            'createtimestamp':123,
            'edittimestamp':123,
            'author':'system',
            'sex':0/1/2,
            'avatar':'https://....',
            'canvas':{
                'data':[
                    [x,y,'rgb(1,1,1)'],
                    [..]
                ],
                'view':{
                    'px':10,
                    'x':1,
                    'y':2
                }
            }
        }

        opt = {
            title:''
        }

        如果不是临时文件，则设置无效直接存储
        如果是临时文件没有设置title则保存为临时文件，反之存储至设置的title（不可重名）
    */
    save(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.title = opt.title || '';
        let data = {
            'title': '',
            'createtimestamp': this._authorInfo.createtimestamp,
            'edittimestamp': new Date().getTime(),
            'author': this._authorInfo.author,
            'sex': this._authorInfo.sex,
            'avatar': this._authorInfo.avatar,
            'editer': null
        }
        let path = 'TempProject.pxpro';
        let del = false;

        if (this.isTemporary()) { //是临时文件
            data['createtimestamp'] = data['edittimestamp'];
            if(opt.title != ''){    //设置了标题
                //判断本地作品数量
                this._File.getCount({
                    success:(res)=>{
                        let v = vip.getVIPData();
                        let text = '';
                        if (!v.vip && res > 1){
                            text = '您还不是我们的会员，本地项目已达上限，是否前往免费激活？';
                        }else if(v.expdes[0] < 3 && res > 5){
                            text = '您的会员等级最多可以保存5个本地项目，是否前往升级？';
                        }
                        if(text != ''){
                            wx.showModal({
                                title: '提示',
                                content: text,
                                success:(res)=>{
                                    if(res.confirm){
                                        vip.open();
                                    }else{
                                        opt._fail({code:400,errmsg:'保存失败'});
                                    }
                                }
                            })
                            return ;
                        }
                        //可以保存
                        data['title'] = opt.title;
                        path = opt.title + '.pxpro';
                        this._authorInfo.path = path;
                        //保存成功后清除临时工程
                        del = true;
                        this._save(opt, path, data, del);
                    },
                    fail:(res)=>{
                        opt._fail({
                            code:400,
                            errmsg:'系统异常'
                        });
                        return ;
                    }
                });
            }else{   //未设置标题
                this._save(opt, path, data, del);
            }
        }else{  //不是临时文件
            path = this._authorInfo.path;
            this._save(opt,path,data,del);
        }
    }

    _save(opt,path,data,del){
        //获取Editer的保存数据 这里获取的是画布数据 与作者信息无关
        this._Editer.save({
            success: (res) => {
                data.editer = res.data;
                this._File.saveFile({
                    file: path,
                    data: JSON.stringify(data),
                    success: (res) => {
                        console.log('保存成功', res);
                        if (del) {
                            this._File.del({
                                file: 'TempProject.pxpro'
                            });
                        }
                        opt._success(res);
                    },
                    fail: (res) => {
                        opt._fail(res);
                    }
                });
            }
        });
    }

    rename(opt) {
        opt = opt || {};
        format.callback(opt);
        this._File.moveFile({
            oldPath:this.getWorkTitle().file,
            newPath:opt.title+'.pxpro',
            success: (res) => {
                this._authorInfo.path = opt.title + '.pxpro';
                opt._success({
                    code: 200
                });
            },
            fail: (res) => {
                opt._fail(res);
            }
        });
    }

    /*
        获取当前文件名
    */
    getWorkTitle(){
        if (this._authorInfo.path == '') {
            return {
                title: 'TempProject',
                file: 'TempProject.pxpro'
            };
        }
        let title = this._authorInfo.path.substr(0, this._authorInfo.path.indexOf('.pxpro'));
        return {
            title:title,
            file: this._authorInfo.path
        };
    }

    /*
        是否是TempProject
    */
    isTemporary(){
        if(this._authorInfo.path == ''){
            return true;
        }
        return false;
    }

    /*
        获取网格开关状态
    */
    gridStatus(v){
        let r = this._RBrid.Visible(v);
        this._EditerOnChange();
        return r;
    }

    /*
        获取辅助线开关状态
    */
    guideStatus(v){
        return this._Editer.guideStatus(v);
    }

    /*
        获取画布数据
    */
    getCanvasData(opt) {
        opt = opt || {};
        format.callback(opt);
        //判断创作内容数量
        if(this._Editer._Data.length < 10){
            opt._fail({
                code:400,
                errmsg:'创作的内容太少，请继续创作一些'
            })
            return;
        }
        let a = this._RBrid.Visible();
        if(this._RBackground.getColor() != null){
            this._RBrid.Visible(false);
        }else{
            this._RBrid.Visible(true);
        }
        //关闭辅助线
        let b = this._Editer.guideStatus();
        this._Editer.guideStatus(false);
        this._Rendering.getCanvasData(opt);
        this._RBrid.Visible(a);
        this._Editer.guideStatus(b);
    }

    /*
        获取参考底图状态
    */
    photoStatus(s){
        return this._Editer.photoStatus(s);
    }

    /*
        参考图导入
        opt= url,type=1/0  临摹模式 ／ 直接生成
    */
    loadPhoto(opt) {
        opt = opt || {};
        format.callback(opt);
        this._phototype = opt.type;
        this._RCenterPhoto.loadPhoto({
            url:opt.url,
            success: (res) => {
                this._RCenterPhoto.Visible(true);
                this._EditerOnChange();
                wx.showToast({
                    title: '双指调整调整方格密度后确认完成',
                    icon:'none'
                })
                opt._success();
            }
        }); //调整模式模块
    }

    /*
        type 0 平滑 1 锐利
    */
    photoConfirm(opt) {
        opt = opt || {};
        format.callback(opt);
        //生成像素图
        let data = this._RCenterPhoto.getData();
        this._RCenterPhoto.Visible(false);
        //获取当前坐标数据
        this._Editer.getViewData({
            success: (res) => {
                console.log(res);
                //生成像素数据组
                let w,h;
                w = parseInt(data.w/res.px);
                h = parseInt(data.h/res.px);
                this._Rendering.getPhotoData({
                    data:data,
                    width:w,
                    height:h,
                    type:opt.type,
                    success:(res)=>{
                        this._Editer.setPhotoData({
                            data:res.data,
                            height:res.height,
                            width:res.width,
                            x:data.l,
                            y:data.t,
                            type:this._phototype,
                            success:(res)=>{
                                opt._success();
                            }
                        });
                    },
                    fail:(res)=>{
                        opt._fail(res);
                    }
                });
            }
        });
    }
}