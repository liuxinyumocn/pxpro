import format from '../lib/format.js';

export default class Rendering {

    constructor(canvasid, shareCanvas) {
        //提供canvasid进行引擎初始化
        this._canvasid = canvasid;
        this._shareCanvas = shareCanvas;
        this._ctx = null;
        this._shareCtx = null;

        //场景数据
        this._data = [];

        this._drawing = false; //是否正在触发自动渲染
        this._autodraw = false; //是否自动触发渲染

        //回调事件
        this.OnLoad = function () { }  //引擎初始化完成后回调
        this.OnRenderBegin = function(){ } //渲染前的回调，用于组织当前视图数据

        //init
        this.init();
    }

    init() {
        wx.createSelectorQuery().select('#' + this._canvasid).fields({
            size: true
        }, function (res) {
            this._width = res.width;
            this._height = res.height;

            //获取画布句柄
            this._ctx = wx.createCanvasContext(this._canvasid, this);
            this._shareCtx = wx.createCanvasContext(this._shareCanvas, this);

            this.OnLoad();
        }.bind(this)).exec();

    }

    /*
        向场景中添加元素
    */
    push(){
        for(let i in arguments){
            this._data.push(arguments[i]);
        }
        this.draw();
    }

    /*
        渲染场景
    */
    draw(){
        if(this._drawing){
            this._autodraw = true;
            return;
        }
        this._draw();
    }

    _draw(){

        this.OnRenderBegin();
        //this._ctx.draw();
        for (let i in this._data) {
            if(this._data[i]._visible)
                this._data[i].Draw(this._ctx);
        }
        this._ctx.draw();
        this._autodraw =false;
        this._drawing = true;
        setTimeout(function () {
            if (this._autodraw) {
                this._draw();
            }else{
                this._drawing = false;
            }
        }.bind(this),17);
    }

    /*
        渲染分享图场景
    */
    drawShare(opt){
        opt = opt || {};
        format.callback(opt);
        this.OnRenderBegin();
        this._shareCtx.scale(2,2);
        this._shareCtx.setFillStyle('white');
        this._shareCtx.fillRect(0, 0, this._width*2, this._height*2);

        for (let i in this._data) {
            if (this._data[i]._visible)
                this._data[i].Draw(this._shareCtx);
        }

        this._shareCtx.setFillStyle('white');
        this._shareCtx.fillRect(0, this._height, this._width * 2,100);

        this._shareCtx.draw(false,function(){
            opt._success({
                width:this._width*2,
                height:this._height*2
            });
        }.bind(this));
    }

    getCanvasData(opt) {
        this.drawShare(opt);
    }

    /*
        生成参考图像素数据
        {
            data:config,
            width:w,
            height:h,
            type:0/1 平滑 锐利
        }
    */
    getPhotoData(opt) {
        opt = opt || {};
        format.callback(opt);

        if(opt.type == 0){ //平滑模式
            this._shareCtx.drawImage(opt.data.path, 0, 0, opt.width, opt.height);
            this._shareCtx.draw(false, function () {
                wx.canvasGetImageData({
                    canvasId: this._shareCanvas,
                    x: 0,
                    y: 0,
                    width: opt.width,
                    height: opt.height,
                    success: (res) => {
                        opt._success(res);
                    },
                    fail: (res) => {
                        opt._fail({
                            code: 400,
                            errmsg: '系统错误'
                        });
                    }
                }, this)
            }.bind(this));
        }else if(opt.type == 1){ //锐利模式
            //确定渲染画面尺寸
            let rate = parseInt(opt.data.w / opt.width);
            let w1 = opt.width * rate,h1 = opt.height *rate;
            this._shareCtx.drawImage(opt.data.path,0,0,w1,h1);
            this._shareCtx.draw(false,function(){
                wx.canvasGetImageData({
                    canvasId: this._shareCanvas,
                    x: 0,
                    y: 0,
                    width: w1,
                    height: h1,
                    success:(res)=>{
                        //锐利计算
                        //rate 是单元格边长
                        let index = 0;
                        let fd = [];
                        let fdata = [];
                        for(let f=0;f<opt.height;f++){ //f行
                            fd=[];
                            for(let j=0;j<opt.width;j++){
                                fd.push(new block());
                            }
                            for (let i = 0; i < rate; i++) {  //每个格子i行
                                for (let j = 0; j < opt.width; j++) {  //j列
                                    for (let g = 0; g < rate; g++) {  //每个格子的g列
                                        fd[j].put([res.data[index++], res.data[index++], res.data[index++], res.data[index++]]);
                                    }
                                }
                            }
                            //得出每个格子的众数
                            for(let j=0;j<fd.length;j++){
                                let a = fd[j].getM();
                                fdata.push(...a);
                            }
                        }
                        //console.log(fdata);
                        opt._success({
                            data:fdata,
                            width:opt.width,
                            height:opt.height
                        });
                    },
                    fail: (res) => {
                        opt._fail({
                            code: 400,
                            errmsg: '系统错误'
                        });
                    }
                })
            }.bind(this));


        }else{
            opt._fail({
                code:400,
                errmsg:'未知处理模式'
            });
        }

    }
}

class block{
    constructor(){
        this.arr=[];
    }
    put(arr){   //arr = [r,g,b,a]
        for(let i in this.arr){
            let item = this.arr[i].c;
            if (item[0] == arr[0] && 
                item[1] == arr[1] && 
                item[2] == arr[2] && 
                item[3] == arr[3]  ){
                    item.n++;
                    return;
            }
        }
        this.arr.push({
            c:arr,
            n:1
        });
    }
    getM(){
        let max = 0 , c=[0,0,0,0];
        for(let i in this.arr){
            if(this.arr[i].n > max){
                c = this.arr[i].c;
            }
        }
        return c;
    }
}