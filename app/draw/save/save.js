import format from '../../lib/format.js';

class Save{

    constructor(){
        this._opt = null;
    }

    open(opt) {
        opt = opt || {};
        format.callback(opt);
        this._opt = opt;
        this._opt.title = this._opt.title || '';

        wx.navigateTo({
            url: '../save/view/page',
        })

    }

    finished(title){
        console.log(title);
        if (title == '临时作品'){
            wx.showToast({
                title: '不能使用系统保留名',
                icon:'none'
            })
            return ;
        }
        wx.navigateBack({})
        setTimeout(function(){
            this._opt._success({
                title: title,
                code: 200
            });
        }.bind(this),500);
    }

}

export default new Save();