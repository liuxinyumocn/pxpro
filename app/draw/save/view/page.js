// app/draw/save/view/page.js
import tool from '../../../lib/tool.js';
import save from '../save.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        title:'',
        defaulttitle:'2019-1-1 13:00'
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        let dt = tool.getDate();
        if(save._opt.title != "")
            dt = save._opt.title
        this.setData({
            defaulttitle: dt
        });
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    input:function(res){
        this.data.title = res.detail.value;
    },
    finished:function(){
        if (this.data.title == '')
        {
            save.finished(this.data.defaulttitle);
        }else{
            save.finished(this.data.title);
        }
    }
})