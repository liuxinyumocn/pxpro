// app/draw/palette/view/page.js
import palette from '../palette.js';
import vip from '../../../user/vip/vip.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        currentColor:'rgb(255,255,255)',
        r:255,
        g:255,
        b:255,
        a:1,
        f:'#ffffff',
        slider_left:1,
        m_left:0,
        m_top:0,
        recent: [ ],
        collect:[ ],
        often:[ //常用色
            {
                r:255,
                g:255,
                b:255,
                id:0
            },
            {
                r: 0,
                g: 0,
                b: 0,
                id: 1
            },
            {
                r: 255,
                g: 0,
                b: 0,
                id: 2
            },
            {
                r: 255,
                g: 128,
                b: 0,
                id: 3
            },
            {
                r: 255,
                g: 255,
                b: 0,
                id: 4
            },
            {
                r: 0,
                g: 255,
                b: 0,
                id: 5
            },
            {
                r: 0,
                g: 128,
                b: 255,
                id: 6
            },
            {
                color: '#0000FF',
                r: 0,
                g: 0,
                b: 255,
                id: 7
            },
            {
                r: 128,
                g: 0,
                b: 255,
                id: 8
            }
        ]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        palette.load({
            mcanvas: 'mcanvas',
            sid:'s',
            success:(res)=>{
                this.setPalette(palette._lastrgb[0],palette._lastrgb[1],palette._lastrgb[2]);
                this.setData({
                    currentColor: this.torgb(palette._lastrgb[0], palette._lastrgb[1], palette._lastrgb[2]),
                    f: this.to16(palette._lastrgb[0], palette._lastrgb[1], palette._lastrgb[2]),
                    r: palette._lastrgb[0],
                    g: palette._lastrgb[1],
                    b: palette._lastrgb[2]
                });
            }
        });

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        //加载近期使用色
        this.initRecent();
        //加载收藏色
        this.initCollect();
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    smove: function (res) {
        palette.smove({
            touch: res.changedTouches[0],
            success: (res) => {
                this.setData({
                    currentColor: this.torgb(res.r, res.g, res.b),
                    r:res.r,
                    g:res.g,
                    b: res.b,
                    f: this.to16(res.r, res.g, res.b),
                    slider_left: res.slider_left
                });
            }
        });
    },
    mmove:function(res){
        palette.mmove({
            touch: res.changedTouches[0],
            success: (res) => {
                this.setData({
                    currentColor: this.torgb(res.r,res.g,res.b),
                    r: res.r,
                    g: res.g,
                    b: res.b,
                    f: this.to16(res.r, res.g, res.b),
                    m_left:res.m_left,
                    m_top:res.m_top
                });
            }
        });
    },
    to16(r,g,b){
        return palette.to16(r,g,b);
    },
    torgb(r,g,b){
        return 'rgb(' + r + ',' + g + ',' + b + ')';
    },
    confrim:function(){
        //确定设置
       palette.confrim([this.data.r,this.data.g,this.data.b]);
    },
    sliderr:function(res){
        let value = res.detail.value;
        this.setData({
            currentColor: this.torgb(value, this.data.g, this.data.b),
            r: value,
            f: this.to16(value, this.data.g, this.data.b)
        });
        if(res.type == "change"){
            this.setPalette(this.data.r,this.data.g,this.data.b);
        }
    },
    sliderg: function (res) {
        let value = res.detail.value;
        this.setData({
            currentColor: this.torgb(this.data.r,value, this.data.b),
            g: value,
            f: this.to16(this.data.r, value, this.data.b)
        });
        if (res.type == "change") {
            this.setPalette(this.data.r, this.data.g, this.data.b);
        }
    },
    sliderb: function (res) {
        let value = res.detail.value;
        this.setData({
            currentColor: this.torgb(this.data.r, this.data.g,value),
            b: value,
            f: this.to16(this.data.r, this.data.g, value)
        });
        if (res.type == "change") {
            this.setPalette(this.data.r, this.data.g, this.data.b);
        }
    },
    setPalette:function(r,g,b){
        palette.setPalette({
            r:r,
            g:g,
            b:b,
            success:(res)=>{

            }
        });
    },
    initRecent: function () {
        palette.loadRecent({
            success:(res)=>{
                let v = [];
                for(let i in res.data){
                    v.push({
                        r: res.data[i][0],
                        g: res.data[i][1],
                        b: res.data[i][2],
                        id:i
                    });
                }
                this.setData({
                    recent:v
                });
            }
        })
    },
    initCollect:function(){
        palette.loadCollect({
            success: (res) => {
                let v = [];
                for (let i in res.data) {
                    v.push({
                        r: res.data[i][0],
                        g: res.data[i][1],
                        b: res.data[i][2],
                        id: i
                    });
                }
                this.setData({
                    collect: v
                });
            }
        });
    },
    recentTap:function(res){
        let vipd = vip.getVIPData();
        if(!vipd.vip){
            wx.showModal({
                title: '提示',
                content: '使用近期及收藏色需要激活会员才可以使用，是否前往免费激活？',
                success:(res)=>{
                    if(res.confirm)
                        vip.open();
                }
            })
            return ;
        } else if (vipd.expdes[0] < 2) {
            wx.showModal({
                title: '提示',
                content: '使用近期及收藏色需要2级会员，是否前往升级？',
                success: (res) => {
                    if (res.confirm)
                        vip.open();
                }
            })
            return;
        }

        let data = res.currentTarget.dataset;
        this.setData({
            currentColor: this.torgb(data.r, data.g, data.b),
            r:data.r,
            g:data.g,
            b:data.b,
            f: this.to16(data.r, data.g, data.b)
        });
        this.setPalette(data.r, data.g, data.b);
    },
    delRecent:function(){
        wx.showModal({
            title: '提示',
            content: '是否清空最近使用颜色列表？',
            success:(res)=>{
                if(res.confirm){
                    palette.delRecent();
                    this.initRecent();
                }
            }
        })
    },
    setCollect: function (res) {
        let data = res.currentTarget.dataset;
        palette.setCollect({
            r:this.data.r,
            g: this.data.g,
            b: this.data.b,
            id:data.id,
            success:(res)=>{
                this.initCollect();
            }
        })
    }
})