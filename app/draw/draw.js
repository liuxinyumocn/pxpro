import format from '../lib/format.js';
import App from '../Main/App.js';
import palette from 'palette/palette.js';
import Share from '../Main/Share.js';
import config from '../config.js';
import ajax from '../lib/ajax.js';
import task from '../user/task.js';
class Draw{

    constructor(){
        this._App = null;
        this._opt = null;
        this._user = null;
    }

    launch(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.file = opt.file || '';
        this._opt = opt;
        this._user = getApp().data.user;
        wx.navigateTo({
            url: '/app/draw/view/page',
        });
    }

    /*
        关联上屏Canvas
        opt={
            id:
        }
    */
    bindCanvas(opt){
        opt = opt || {};
        format.callback(opt);
        this._App = new App({
            canvasid:opt.id,
            file:this._opt.file,
            shareCanvas: opt.shareCanvas
        });
    }

    touchstart(touches){
        if(this._App)
            this._App.touchstart(touches);
    }

    touchend(touches) {
        if(this._App)
            this._App.touchend(touches);
    }

    touchmove(touches) {
        if(this._App)
            this._App.touchmove(touches);
    }

    touchcancel(touches) {
        if(this._App)
            this._App.touchcancel(touches);
    }

    handModel() {
        if(this._App)
            this._App.handModel();
    }

    pencilModel() {
        if (this._App)
            this._App.pencilModel();
    }

    setPencilCover(opt) {
        opt = opt || {};
        format.callback(opt);
        if(this._App){
            this._App.setPencilCover(opt.status);
            opt._success({status:opt.status});
        }else{
            opt._fail();
        }     
    }

    pickModel(opt) {
        if (this._App)
            this._App.pickModel(opt);
    }

    bucketModel(){
        if(this._App)
            this._App.bucketModel();
    }

    eraserModel(){
        if (this._App)
            this._App.eraserModel();
    }
    paletteSelect(opt) {
        opt = opt || {};
        format.callback(opt);
        //设置颜色
        palette.open({
            success:(res)=>{
                    this.setPencilColor(res.rgb);
                opt._success(res);
            }
        });
    }

    setPencilColor(rgb) {
        //设置画笔颜色
        if (this._App)
            this._App.setPencilColor(rgb);

    }

    trash(){
        //清空画布
        if (this._App)
            this._App.trash();
    }
    suitview(opt){
        //最适视野
        if (this._App)
            this._App.suitview(opt);
    }

    editBack(opt){
        //编辑回退
        if (this._App)
            this._App.editBack(opt);
    }

    save(opt){
        //保存
        if (this._App)
            this._App.save(opt);
    }

    rename(opt) {
        //重命名
        if (this._App)
            this._App.rename(opt);
    }

    photoStatus(s){
        console.log(s);
        if(this._App)
            return this._App.photoStatus(s);
    }

    loadPhoto(opt){
        if(this._App)
            this._App.loadPhoto(opt);
    }

    photoConfirm(opt) {
        if (this._App)
            this._App.photoConfirm(opt);
    }

    /*
        作品名称
    */
    getWorkTitle(){
        if(this._App)
            return this._App.getWorkTitle();
    }

    isTemporary(){
        if(this._App)
            return this._App.isTemporary();
        return false;
    }

    gridStatus(s){
        if(this._App)
            return this._App.gridStatus(s);
    }

    guideModel() {
        if (this._App)
            this._App.guideModel();
    }

    guideStatus(s) {
        if (this._App)
            return this._App.guideStatus(s);
    }

    genShareCard(opt) {
        opt = opt || {};
        format.callback(opt);

        //获得当前画布的所有数据
        if(this._App){
            this._App.getCanvasData({
                success:(res)=>{
                    //生成卡片
                    let share = new Share(opt.canvasid);
                    let info = this._user.getUserPublicData();
                    share.genCard({
                        nickname: info.nickname,
                        avatar: info.avatar,
                        imgwidth: res.width,
                        imgheight: res.height,
                        success:(res)=>{
                            //首次创建朋友圈海报
                            task.checkSharePoster({
                                success:(res)=>{
                                    wx.showModal({
                                        title: '提示',
                                        content: '您已完成首次创建朋友圈海报的任务，快去朋友圈分享你的杰作吧～奖励已经到账了噢',
                                        showCancel:false
                                    })
                                }
                            });
                            opt._success(res);
                        }
                    });
                },
                fail: (res) => {
                    opt._fail(res);
                }
            });

        }

    }

    genUploadPhoto(opt) {
        opt = opt || {};
        format.callback(opt);
        //获得当前画布的所有数据
        if (this._App) {
            this._App.getCanvasData({
                success: (res) => {
                    //生成卡片
                    let share = new Share(opt.canvasid);
                    share.getPhoto({
                        imgwidth: res.width,
                        imgheight: res.height,
                        success: (res) => {
                            opt._success(res);
                        }
                    });
                },
                fail:(res)=>{
                    opt._fail(res);
                }
            });

        }
    }

    /*
        删除本地
    */
    delLocal(){
        this.trash();
        this._App.delLocalFile();
    }
}

export default new Draw();