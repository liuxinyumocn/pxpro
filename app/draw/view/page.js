// app/draw/view/page.js
import config from '../../config.js';
import draw from '../draw.js';
import Control from 'Control.js';
import save from '../save/save.js';
import upload from '../../user/upload/upload.js';
import vip from '../../user/vip/vip.js';

let control = null;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        shareWidth: 1,
        shareHeight: 1,
        colorBlock:'rgb(0,0,0)',
        backPic:'src/back.png',
        bucketPic:'src/bucket.png',
        eraserPic:'src/eraser.png',
        loadPic:'src/load.png',
        movePic:'src/move.png',
        palettePic:'src/palette.png',
        penPic:'src/pen.png',
        pencilPic:'src/pencil.png',
        photoPic: 'src/photo.png',
        savePic: 'src/save.png',
        pickPic: 'src/pick.png',
        linePic: 'src/line.png',
        trashPic: 'src/trash.png',
        sharePic: 'src/share.png',
        outPic: 'src/out.png',
        pencilCover:false,          //是否为覆盖笔
        photoConfirm:false,         //参考图导入
        alertVIP:true               //model提示用户
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        control = new Control(this);
        //接入Canvas渲染引擎
        draw.bindCanvas({
            id:'canvas',
            shareCanvas:'shareCanvas'
        });
        this.handModel();
        this.initShareCanvas();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
        //保存工程
        draw.save({
            success: (res) => {

            }
        });
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        //保存工程
        draw.save({
            success: (res) => {

            }
        });
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    initShareCanvas:function(){
        this.setData({
            shareWidth: config.systemInfo.windowWidth*2.5,
            shareHeight:config.systemInfo.windowHeight*2.5
        });
    },
    onShareAppMessage: function () {

    },
    touchstart:function(res){
        draw.touchstart(res.touches);
    },
    touchmove: function (res) {
        draw.touchmove(res.touches);

    },
    touchend: function (res) {
        draw.touchend(res.changedTouches);

    },
    touchcancel: function (res) {
        draw.touchcancel(res.touches);
    },
    handModel:function(){
        control.exclude();
        draw.handModel();
        control.move(true);
    },
    handlong:function(){ //恢复到最适视野
        draw.suitview();
    },
    pencilModel: function () {
        control.exclude();
        draw.pencilModel();
        control.pencil(true);
    },
    pencilCover:function(){ //覆盖模式
        draw.setPencilCover({
            status: !this.data.pencilCover,
            success: (res) => {
                control.pencilCover(res.status);
            }
        });
    },
    eraserModel: function () {
        control.exclude();
        draw.eraserModel();
        control.eraser(true);
    },
    pickModel: function () {   //取色器模式
        if (this.alertVip('取色器工具', 2)) {
            control.exclude();
            draw.pickModel({
                success: (res) => {    //颜色变化回调
                    this.setData({
                        colorBlock: res.color
                    });
                    draw.setPencilColor(res.color);
                }
            });
            control.pick(true);
        }
    },
    bucketModel:function(){
        if(this.alertVip('油漆桶工具',2)){
            control.exclude();
            draw.bucketModel();
            control.bucket(true);
        }
    },
    paletteSelect:function(){
        if (this.alertVip('调色盘工具', 1)) {
            draw.paletteSelect({
                currentColor: this.data.colorBlock,
                success: (res) => {
                    console.log(res);
                    this.setData({
                        colorBlock: res.rgb
                    });
                }
            });
        }
    },
    selectPhoto:function(){ //导入参考图
        if (this.alertVip('参考图工具', 3)) {
            let res = draw.photoStatus();
            if (res.has) {
                let t = res.show ? '关闭临摹底图' : '开启临摹底图';
                wx.showActionSheet({
                    itemList: [t, '关闭并选用新的图片'],
                    success: (res2) => {
                        if (res2.tapIndex == 0) {
                            draw.photoStatus(!res.show);
                        } else {
                            draw.photoStatus(false);
                            this._selectPhoto();
                        }
                    }
                })
            } else {
                this._selectPhoto();
            }
        }
    },
    _selectPhoto:function(){
        wx.chooseImage({
            count: 1,
            success: function (res) {
                wx.showActionSheet({
                    itemList: ['直接生成像素图', '创建临摹底图'],
                    success: (res2) => {
                        wx.showLoading({
                            title: '请稍后',
                        })
                        draw.loadPhoto({
                            url: res.tempFilePaths[0],
                            type: res2.tapIndex,
                            success: (res) => {
                                wx.hideLoading();
                                this.setData({
                                    'photoConfirm': true
                                });
                            }
                        });
                    }
                })
            }.bind(this),
        })
    },
    photoConfirm:function(){    //缺人调整图像
        wx.showActionSheet({
            itemList: ['平滑','锐利'],
            success:(res)=>{
                wx.showLoading({
                    title: '请稍后',
                })
                draw.photoConfirm({
                    type:res.tapIndex,  //0 平滑 1 锐利
                    success: (res) => {
                        wx.hideLoading();
                        this.setData({
                            'photoConfirm':false
                        });
                    }
                });
            }
        })
    },
    trash:function(){   //清空画布
        wx.showModal({
            title: '提示',
            content: '真的要放弃当前的创作嘛?',
            success:(res)=>{
                if(res.confirm){
                    draw.trash();
                }
            }
        })
    },
    editBack:function(){
        draw.editBack({
            fail:(res)=>{
                wx.showToast({
                    title: res.errmsg,
                    icon:'none'
                })
            }
        });
    },
    save:function(){
        if(draw.isTemporary()){
            //起一个名字
            save.open({
                success:(res)=>{
                    draw.save({
                        title:res.title,
                        success:(res)=>{
                            wx.showToast({
                                title: '保存成功',
                            })
                        },
                        fail:(res)=>{
                            wx.showToast({
                                title: res.errmsg,
                                icon:'none'
                            })
                        }
                    });
                }
            });
        }else{
            //保存工程
            draw.save({
                success: (res) => {
                    wx.showToast({
                        title: '已保存',
                    })
                }
            });
        }
    },
    rename:function(){
        if(draw.isTemporary()){
            this.save();
        }else{
            //重命名
            let title = draw.getWorkTitle();
            save.open({
                title:title.title,
                success:(res)=>{
                    draw.rename({
                        title:res.title,
                        success:(res)=>{
                            wx.showToast({
                                title: '完成'
                            })
                        }
                    });
                }
            });
        }
    },
    lineModel:function(){
        //获取网格状态
        let grid = draw.gridStatus();
        let text = grid ? '关闭网格' : '开启网格';

        let guide = draw.guideStatus();
        let text2 = guide ? '管理辅助线' : '开启辅助线模式';

        wx.showActionSheet({
            itemList: [
                text,
                text2
            ],
            success:(res)=>{
                if(res.tapIndex == 0){
                    draw.gridStatus(!grid);
                }else if(res.tapIndex == 1){
                    if (!this.alertVip('辅助线工具', 2))
                        return;
                    if(guide){
                        //管理辅助线
                        wx.showActionSheet({
                            itemList: ['调整模式','关闭辅助线模式'],
                            success:(res)=>{
                                if (res.tapIndex == 1) {
                                    draw.guideStatus(!guide);
                                    this.handModel();
                                }else if(res.tapIndex == 0){
                                    control.exclude();
                                    draw.guideModel();
                                    control.line(true);
                                }
                            }
                        })
                    }else{
                        //开启辅助线
                        draw.guideStatus(!guide);
                        control.exclude();
                        draw.guideModel();
                        control.line(true);
                    }
                }
            }
        })

    },
    gen:function(){
        wx.showActionSheet({
            itemList: ['分享给朋友','生成朋友圈海报'],
            success:(res)=>{
                if (res.tapIndex == 1) {
                    wx.showActionSheet({
                        itemList: ['自动调整视野','采用当前视野'],
                        success:(res)=>{
                            if(res.tapIndex == 0){
                                draw.suitview({
                                    success: (res) => {
                                        this._gen();
                                    }
                                });
                            }else if(res.tapIndex == 1){
                                this._gen();
                            }
                        }
                    })
                }else if(res.tapIndex == 0){
                    wx.showModal({
                        title: '提示',
                        content: '分享给朋友是需要先发布作品噢～具体操作：点击下方倒数第二个按钮，进行作品发布！',
                        showCancel:false
                    })
                }
            }
        })
    },
    _gen: function () {
        draw.genShareCard({
            canvasid: 'shareCanvas',
            type: 'current',
            success: (res) => {
                wx.previewImage({
                    urls: [res.tempFilePath]
                });
            },
            fail:(res)=>{
                wx.showToast({
                    title: res.errmsg,
                    icon:'none'
                })
            }
        })
    },
    upload: function () {
        if (!this.alertVip('发布功能', 1)) 
            return;
        wx.showModal({
            title: '提示',
            content: '发布前要为作品创建缩略图作为封皮，是否现在发布？',
            success:(res)=>{
              if(res.confirm){
                  wx.showActionSheet({
                      itemList: ['自动调节视野', '采用当前视野'],
                      success: (res) => {
                          if (res.tapIndex == 0) {
                              draw.suitview({
                                  success: (res) => {
                                      this._upload();
                                  }
                              });
                          } else if (res.tapIndex == 1) {
                              this._upload();
                          }
                      }
                  })
              }
            }
        })
    },
    _upload:function(){
        //先保存
        draw.save({
            success: (res) => {
                draw.genUploadPhoto({
                    canvasid: 'shareCanvas',
                    type: 'current',
                    success: (res2) => {
                        let img = res2.tempFilePath;
                        //获取本地文件地址
                        upload.open({
                            img: img,
                            file: res.path,
                            success:(res)=>{
                                //删除本地文件
                                draw.delLocal();
                            }
                        });
                    },
                    fail:(res)=>{
                        wx.showToast({
                            title: res.errmsg,
                            icon:'none'
                        })
                    }
                });
            }
        });
    },
    alertVip:function(str,level){
        let data = vip.getVIPData();
        if(!data.vip){
            if(this.data.alertVIP){
                wx.showModal({
                    title: '提示',
                    content: '使用'+str+'需要激活会员才可以使用，是否前往免费激活？',
                    success:(res)=>{
                        if(res.confirm){
                            vip.open();
                        }else{
                            this.data.alertVIP = false;
                        }
                    }
                })
            } else {
                wx.showToast({
                    title: '使用' + str + '需要激活会员，请前往个人中心免费激活',
                    icon: 'none'
                })
            }
        }else{
            if (data.expdes[0] >= level)
                return true;
            if (this.data.alertVIP) {
                wx.showModal({
                    title: '提示',
                    content: '使用' + str + '需要'+level+'级会员才可以使用，是否前往升级？',
                    success: (res) => {
                        if (res.confirm) {
                            vip.open();
                        } else {
                            this.data.alertVIP = false;
                        }
                    }
                })
            } else {
                wx.showToast({
                    title: '使用'+str+'需要'+level+'级会员，请先升级',
                    icon:'none'
                })
            }
        }
        return false;

        wx.showModal({
            title: '',
            content: '',
        })
    }
})