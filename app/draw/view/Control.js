
/*
    按钮状态器
*/
export default class Control {
    constructor(that) {
        this.that = that;
    }

    /*
        关闭互斥状态
    */
    exclude() {
        this.pencil();
        this.move();
        this.eraser();
        this.bucket();
        this.pen();
        this.line();
        this.pick();
    }

    pencil(v = false) {
        if (!v) {
            this.that.setData({
                pencilPic: 'src/pencil.png'
            });
        } else {
            this.that.setData({
                pencilPic: 'src/pencil2.png'
            });
        }
    }

    pencilCover(v=false){
        this.that.setData({
            pencilCover:v
        });
    }

    back(v = false) {
        if (!v) {
            this.that.setData({
                backPic: 'src/back.png'
            });
        } else {
            this.that.setData({
                backPic: 'src/back2.png'
            });
        }
    }
    bucket(v = false) {
        if (!v) {
            this.that.setData({
                bucketPic: 'src/bucket.png'
            });
        } else {
            this.that.setData({
                bucketPic: 'src/bucket2.png'
            });
        }
    }
    eraser(v = false) {
        if (!v) {
            this.that.setData({
                eraserPic: 'src/eraser.png'
            });
        } else {
            this.that.setData({
                eraserPic: 'src/eraser2.png'
            });
        }
    }
    load(v = false) {
        if (!v) {
            this.that.setData({
                loadPic: 'src/load.png'
            });
        } else {
            this.that.setData({
                loadPic: 'src/load2.png'
            });
        }
    }
    move(v = false) {
        if (!v) {
            this.that.setData({
                movePic: 'src/move.png'
            });
        } else {
            this.that.setData({
                movePic: 'src/move2.png'
            });
        }
    }
    palette(v = false) {
        if (!v) {
            this.that.setData({
                palettePic: 'src/palette.png'
            });
        } else {
            this.that.setData({
                palettePic: 'src/palette2.png'
            });
        }
    }
    photo(v = false) {
        if (!v) {
            this.that.setData({
                photoPic: 'src/photo.png'
            });
        } else {
            this.that.setData({
                photoPic: 'src/photo2.png'
            });
        }
    }
    pen(v = false) {
        if (!v) {
            this.that.setData({
                penPic: 'src/pen.png'
            });
        } else {
            this.that.setData({
                penPic: 'src/pen2.png'
            });
        }
    }
    save(v = false) {
        if (!v) {
            this.that.setData({
                savePic: 'src/save.png'
            });
        } else {
            this.that.setData({
                savePic: 'src/save2.png'
            });
        }
    }
    trash(v = false) {
        if (!v) {
            this.that.setData({
                trashPic: 'src/trash.png'
            });
        } else {
            this.that.setData({
                trashPic: 'src/trash2.png'
            });
        }
    }
    line(v = false) {
        if (!v) {
            this.that.setData({
                linePic: 'src/line.png'
            });
        } else {
            this.that.setData({
                linePic: 'src/line2.png'
            });
        }
    }
    pick(v = false) {
        if (!v) {
            this.that.setData({
                pickPic: 'src/pick.png'
            });
        } else {
            this.that.setData({
                pickPic: 'src/pick2.png'
            });
        }
    }
    out(v = false) {
        if (!v) {
            this.that.setData({
                outPic: 'src/out.png'
            });
        } else {
            this.that.setData({
                outPic: 'src/out2.png'
            });
        }
    }
    share(v = false) {
        if (!v) {
            this.that.setData({
                sharePic: 'src/share.png'
            });
        } else {
            this.that.setData({
                sharePic: 'src/share2.png'
            });
        }
    }
}