import format from '../../lib/format.js';
import ajax from '../../lib/ajax.js';
import config from '../../config.js';

class Search {

    constructor() {
        this._opt = {};
        this._opusData = [];
    }

    /*
        opt={
            keyword
        }
    */
    open(opt) {
        opt = opt || {};
        format.callback(opt);
        this._opt = opt;
        this._opusData = [];
        //过滤不合法词
        let ill = ['[',']',',','"'];
        for(let i in ill){
            if(opt.keyword.indexOf(ill[i]) != -1){
                wx.showToast({
                    title: '关键词中不能带有特殊符号噢',
                    icon:'none'
                })
                return;
            }
        }


        getApp().data.user.wxlogin({
            login: true,
            success: (res) => {
                wx.navigateTo({
                    url: '/app/home/search/view/page?keyword=' + opt.keyword,
                })
            }
        })

    }

    setKeyword(id) {
        this._opt.keyword = id;
    }

    getOpusList(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.page = opt.page || 1;
        ajax.post({
            url: config.pxpro + 'mini/opus/search',
            data: {
                keyword: this._opt.keyword,
                page: opt.page
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    for (let i in res.list) {
                        let c = false;
                        for (let j in this._opusData) {
                            if (this._opusData[j].id == res.list[i].id) {
                                c = true;
                                break;
                            }
                        }
                        if (!c) {
                            this._opusData.push(res.list[i]);
                        }
                    }
                    opt._success(this._opusData);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

}

export default new Search();