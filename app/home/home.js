import format from '../lib/format.js';
import ajax from '../lib/ajax.js';
import config from '../config.js';
import userInfoCache from '../user/userInfoCache.js';
import browser from '../../domoe/browser/browser.js';

class Home{
    constructor() {

        this._album = [];
        this._avatars = [];

        this._bannerData = [];
    }

    load(opt) {
        opt = opt || {};
        format.callback(opt);

        this._album = [];
        this._avatars = [];
    }

    clear(){
        this._album = [];
        this._avatars = [];
    }

    getHomeList(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.page = opt.page || 1;
        ajax.post({
            url: config.pxpro + 'mini/home',
            data: {
                page: opt.page
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    this._avatars.push(...res.album.avatars);
                    let len = res.album.list.length;
                    let finished = 0;
                    for (let i in res.album.list) {
                        let index = 0;
                        let c = false;
                        for (let j in this._album) {
                            if (this._album[j].id == res.album.list[i].id) {
                                c = true;
                                len--;
                                break;
                            }
                        }
                        if (!c) {
                            this._album.push(res.album.list[i]);
                            let index = this._album.length-1;
                            //加载用户头像数据
                            userInfoCache.getUserInfo({
                                id:res.album.list[i].owner,
                                success:(res)=>{
                                    finished++;
                                    this._album[index].avatar = res.avatar;
                                    this._album[index].nickname = res.nickname;
                                    this._album[index].sex = res.gender;
                                    if (finished == len) {
                                        opt._success({
                                            album:this._album
                                        });
                                    }
                                },
                                fail:(res)=>{
                                    opt._fail({
                                        code:400,
                                        errmsg:'网络开小差了，重新尝试下'
                                    });
                                }
                            });
                        }else{
                            if (len == 0) {
                                opt._success({
                                    album: this._album
                                });
                            }
                        }
                    }
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

    /*
        获取缩略图
    */
    getAvatar(id) {
        for (let i in this._avatars) {
            if (this._avatars[i].id == id)
                return this._avatars[i].avatar;
        }
    }

    /*
        获取Banner数据
        这将可能产生2次success回调，若产生两次，则第一次为历史缓存数据
    */
    getBanner(opt) {
        opt = opt || {};
        format.callback(opt);
        let cache = false;
        wx.getStorage({
            key: 'banner',
            success: (res) => {
                cache = true;
                opt._success(this.parserBanner(res.data));
            },
            complete: (res) => {
                ajax.post({
                    url:config.pxpro + 'mini/home/banner',
                    success:(res)=>{
                        res = res.data;
                        if(res.code == 200){
                            wx.setStorage({
                                key: 'banner',
                                data: res,
                            })
                            opt._success(this.parserBanner(res));
                        } else {
                            this.bannerFail(opt, cache);
                        }
                    },
                    fail:(res)=>{
                        this.bannerFail(opt,cache);
                    }
                })
            }
        })
    }

    bannerFail(opt,cache = false) {
        //使用系统默认的Banner
        if (!cache) {
            opt._success(this.parserBanner({
                code: 200,
                list: []
            }));
        }

    }

    parserBanner(data){
        this._bannerData = data.list;
        return data;
    }

    tapBanner(id){
        for(let i in this._bannerData){
            if(this._bannerData[i].id == id){
                let json = JSON.parse(this._bannerData[i].json);
                switch(json.type){
                    case 'url':
                        browser.open(json.value);
                    break;
                    case 'tab':
                        wx.switchTab({
                            url: json.value,
                        })
                    break;
                }
                return ;
            }
        }
    }
}

export default new Home();