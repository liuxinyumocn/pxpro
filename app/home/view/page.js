// app/home/view/page.js
import opus from '../../user/opus/opus.js';
import search from '../search/search.js';
import home from '../home.js';
import format from '../../lib/format.js';
import myopus from '../../user/myopus/myopus.js';
import ad from '../../lib/ad.js';
import task from '../../user/task.js';
import report from '../../user/report.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        homePageIndex: 0,
        loadHomeing: false,
        homeList: [],
        lastTimestamp:0,
        banner:[]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        console.log(decodeURIComponent(options.scene));
        this.onPullDownRefresh();
        this.initBanner();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        let now = new Date().getTime();
        if (now - this.data.lastTimestamp < 30000 && this.data.homePageIndex == 0) {
            wx.stopPullDownRefresh();
            return;
        }
        this.data.lastTimestamp = now;
        home.clear();
        this.data.homePageIndex = 0;
        this.nextPageHome({
            complete: (res) => {
                wx.stopPullDownRefresh();
            },
            // fail:(res)=>{
            //     wx.showToast({
            //         title: res.errmsg,
            //         icon:'none'
            //     })
            // }
        });

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        //console.log('首页加载更多');
        this.nextPageHome({
            complete:(res)=>{
                //console.log(res);
            }
        });
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    launch: function () {
        draw.launch({
            file:''
        });
    },
    look:function(){
        opus.open({
            id:14
        });
    },
    open:function(res){
        opus.open({
            id:res.detail.value
        });
    },
    search:function(res){
        let keyword = res.detail.value.trim();
        if(keyword == ''){
            wx.showToast({
                title: '请输入关键词',
                icon:'none'
            })
            return ;
        }
        if(keyword.length > 16)
        {
            wx.showToast({
                title: '关键词太长啦',
                icon: 'none'
            })
            return;
        }
        search.open({keyword:keyword});
    },
    nextPageHome: function (opt) {
        opt = opt || {};
        format.callback(opt);
        if (this.data.loadHomeing){
            opt._fail({code:400,errmsg:'正在加载请耐心等待'});
            return;
        }
        wx.showNavigationBarLoading();
        this.data.loadHomeing = true;
        setTimeout(function(){
            if(this.data.loadHomeing == true){
                this.data.loadHomeing = false;
                wx.hideNavigationBarLoading();
            }
        }.bind(this),4000);
        home.getHomeList({
            page: this.data.homePageIndex + 1,
            success: (res) => {
                this.data.homePageIndex++;
                this.loadHome(res);
                opt._success(res);
                wx.hideNavigationBarLoading();
            },
            fail: (res) => {
                wx.hideNavigationBarLoading();
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
                opt._fail(res);
                if(this.data.homeList.length == 0)
                    this.data.lastTimestamp = 0;
            },
            complete: (res) => {
                this.data.loadHomeing = false;
            }
        });
    },
    loadHome: function (res) {
        let album = res.album;
        //加载Album
        let arr = [];
        for (let i =0 ;i<album.length;i++) {
            let item = album[i];
            let cell = {
                content: item.content,
                id: item.id,
                opus: [],
                nickname:item.nickname,
                sex:item.sex,
                avatar:item.avatar,
                owner:item.owner,
                type:1
            }
            let arr2 = JSON.parse(item.opus);
            for (let j in arr2) {
                cell.opus.push({
                    id: arr2[j],
                    avatar: home.getAvatar(arr2[j])
                });
            }
            arr.push(cell);
            if((i+3) % 3 == 0){
                let ad2 = ad.getBanner('home');
                if(ad2 != '')
                    arr.push({
                        type:2,
                        ad: ad2
                    });
            }
        }
        this.setData({
            homeList: arr
        });
        if(arr.length >= 10){
            task.checkAlbum({
                success:(res)=>{
                    wx.showToast({
                        title: '今日任务已完成！',
                    })
                }
            });
        }
    }, openOpus:function(res){
        let id = res.target.dataset.id;
        opus.open({id:id});
    }, openUser: function (res) {
       let id = res.target.dataset.userid;
       myopus.open({id:id})
    },
    initBanner:function(){
        home.getBanner({
            success:(res)=>{
                this.setData({
                    banner:res.list
                });   
            }
        });
    },
    onbanner:function(res){
        let id = res.currentTarget.dataset.id;
        home.tapBanner(id);
    },
    report:function(res){
        let id = res.currentTarget.dataset.album;
        report.upload({
            fromc: 'album',
            fromid: id,
            success: (res) => {
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
            },
            fail: (res) => {
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
            }
        });
    }
})