import browser from '../../domoe/browser/browser.js';
import config from '../config.js';
class Doc {
    open(opt) {
        browser.open(config.pxpro + 'doc?id=' + opt);
    }
}

export default new Doc();

/*
    u1  首页“查看使用指南”
    u2  学校选择页面“没有想要的校区？点这里反馈”
    u3  提现页面 “提现说明”
    u4  配送类型选择页面 “以下配送方案不可用，为什么？”
    u5  配送类型选择页面 “如何准确选择配送类型？”
    u6  券包页面 “如何获得代金券“
*/