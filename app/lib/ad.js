/*
    广告管理器
*/
import format from 'format.js';

class Video{
    constructor(opt,id){
        this.ob = wx.createRewardedVideoAd({
            adUnitId: id
        });
        this.ob.onLoad(() => {
            opt._success({
                code: 200,
                video: this
            });
        })
        this.ob.onError((res) => {
            opt._fail(res);
        })
        this.ob.onClose((res) => {
            this._onclose(res);
        })
        this._opt = null;
    }

    reload(){
        this.ob.reload();
    }

    destroy(){
        this.ob.offLoad(function(res){
            console.log(res);
        });
        this.ob.offClose(function (res) {
            console.log(res);
        });
        this.ob.destroy(function (res) {
            console.log(res);
        });
    }

    _onclose(status){
        if (status && status.isEnded || status === undefined) {
            // 正常播放结束，下发奖励
            this._opt._success({
                code:200,
                errmsg:'播放完成下发奖励'
            });
        } else {
            // 播放中途退出，进行提示
            this._opt._fail({
                code: 400,
                errmsg: '播放未完成'
            });
        }
    }
    show(opt) {
        opt = opt || {};
        format.callback(opt);
        this._opt = opt;
        this.ob.show().catch(err => {
            // 失败重试
            this.ob.load()
                .then(() => this.ob.show())
        })
    }
}

class Interstitial{

    constructor(opt, id) {
        this.ob = wx.createInterstitialAd({
            adUnitId: id
        });
        this.ob.onLoad(() => {
            opt._success({
                code: 200,
                interstitial: this
            });
        })
        this.ob.onError((res) => {
            opt._fail(res);
        })
        this.ob.onClose((res) => {
            this._onclose(res);
        })
        this._opt = null;
    }
    _onclose(status) {
        this._opt._success({
            code: 200,
            errmsg: '关闭广告'
        });
    }
    show(opt) {
        opt = opt || {};
        format.callback(opt);
        this._opt = opt;
        this.ob.show().catch(err => {
            opt._fail({
                code:400,
                errmsg:err.errMsg
            });
        })
    }
}

class Ad{
    constructor() {
        this._banner = {    //横幅广告位
            'home': [
                '',
                // 'adunit-806a4ccf0ae62903',
                // 'adunit-6d90dd28bd5cf7ad',
                // 'adunit-b7493850afe3f027',
                // 'adunit-2a617d9aa907eb10',
                // 'adunit-8380cd526e3b0e85',
                // 'adunit-7ae31081f13bff17'
            ],
            'opus': [
                 '',
                // 'adunit-806a4ccf0ae62903',
                // 'adunit-6d90dd28bd5cf7ad',
                // 'adunit-b7493850afe3f027',
                // 'adunit-2a617d9aa907eb10',
                // 'adunit-8380cd526e3b0e85',
                // 'adunit-7ae31081f13bff17'
            ]
        };
        this._video = {
            'long':[
               //'',// 
               'adunit-a1d2e770938f1a42'
            ],
            'short':[
                //'',// 
                'adunit-b721859ed42ee142'
            ]
        };   //激励视频广告位
        this._interstitial = {
            'rank':[
                '',//'adunit-6ce04ac37f433cd0'
            ]
        }; //插屏广告位
        this._num = {
            'home':0,
            'opus':0,
            'long':0,
            'short':0,
            'rank':0
        };
    }

    /*
        获取一个Banner的广告位
        不同页面的广告位
    */
    getBanner(pos = 'home') {
        let index = this._num[pos] + 1;
        if (index >= this._banner[pos].length)
            index = 0;
        this._num[pos] = index;
        return this._banner[pos][index];
    }

    getVideoId(type = 'long') {
        let index = this._num[type] + 1;
        if (index >= this._video[type].length)
            index = 0;
        this._num[type] = index;
        return this._video[type][index];
    }

    getInterstitialId(type = '') {
        let index = this._num[type] + 1;
        if (index >= this._interstitial[type].length)
            index = 0;
        this._num[type] = index;
        return this._interstitial[type][index];
    }

    createVideoAd(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.type = opt.type || 'long';
        let id = this.getVideoId(opt.type);
        if (wx.createRewardedVideoAd) {
            let ob = new Video(opt, id);
        } else {
            opt._fail({
                code: 400,
                errmsg: '创建失败'
            })
        }
    }
    
    createInterstitialAd(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.type = opt.type || 'rank';
        let id = this.getInterstitialId(opt.type);
        if (wx.createInterstitialAd && id != '') {
            let ob = new Interstitial(opt, id);
        } else {
            opt._fail({
                code: 400,
                errmsg: '创建失败'
            })
        }
    }
}

export default new Ad();