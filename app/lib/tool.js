

class Tool{

    /*
        数值转换
        0～999
        1000~100000  1k ~ 100k
        1000000 1m
    */
    getNum(v){
        if(v < 10000)
            return v;
        if (v < 100000000){
            let p = parseInt(v / 1000);
            return p/10 + 'w';
        }
        let p = parseInt(v / 10000000);
        return p / 10 + '亿';
    }

    getDate(v) {
        let date = v == null ? new Date() : new Date(v * 1000);
        let str = "";
        str += date.getFullYear();
        str += "-";
        str += (date.getMonth() + 1);
        str += "-";
        str += (date.getDate());
        str += " ";
        let h = date.getHours();
        if (h < 10) {
            h = "0" + h;
        }
        str += h;
        str += ":";
        let m = date.getMinutes();
        if (m < 10)
            m = "0" + m;
        str += m;
        return str;
    } 
    
    getDateYMD(v) {
        let date = v == null ? new Date() : new Date(v * 1000);
        let str = "";
        str += date.getFullYear();
        str += "-";
        str += (date.getMonth() + 1);
        str += "-";
        str += (date.getDate());
        return str;
    }

    getDateText(v){
        let timestamp = new Date().getTime() / 1000;
        if(timestamp > v ){
            let dis = timestamp - v;
            if(dis < 60)
                return '刚刚';
            if(dis < 60*60)
                return parseInt(dis/60)+'分钟前';
            if (dis < 3600 * 24)
                return parseInt(dis / 3600) + '小时前';
            if (dis < 3600 * 24 * 3)
                return parseInt(dis / 3600 / 24) + '天前';
            return this.getDateYMD(v);
        }
    }

    getCoyeRight(v = 1) {
        if (v == 1) {
            return {
                title: '临摹',
                color: '#3eabdd'
            }
        }
        if (v == 2) {
            return {
                title: '同人',
                color: '#b53cce'
            }
        }
        if (v == 3) {
            return {
                title: '原创',
                color: '#12c974'
            }
        }
    }
    
    getFee(v) {
        let fee = v;
        let sfee = fee + "";
        //元位
        let y = "0";
        if (fee > 99)
            y = parseInt(fee / 100) + "";
        //从个位起每3位增加1个逗号
        let length = y.length;
        let buff = "";
        for (let i = length; i > 0; i--) {
            if ((length - i) % 3 == 0 && i != length) {
                buff = "," + buff;
            }
            buff = y.substr(i - 1, 1) + buff;
        }
        y = buff;
        //角位
        let j = "0";
        if (fee > 9) {
            j = sfee.substr(-2, 1);
        }
        //分位
        let f = "0";
        if (fee > 0) {
            f = sfee.substr(-1, 1);
        }
        let balance = y + "." + j + f;
        return balance;
    }

    /*
        生成随机字符串
    */
    getNonceStr(len = 16){
        let char = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        let str = "";
        for (let i = 0; i < len; i++) {
            let pos = parseInt(char.length *(Math.random()-0.00001));
            str += char.substr(pos,1);
        }
        return str;
    }
}
export default new Tool();