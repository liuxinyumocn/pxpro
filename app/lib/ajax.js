
/**
 * PHP Session模块
 * 
*/

import format from 'format.js';

class Ajax {

    constructor() {
        this.Cookies = [];
    }

    /**
     * {
     *      url:"",
     *      data:"",
     *      success:function(){},
     *      fail:function(){}
     * }
     * 
    */
    post(opt) {
        opt = opt || {};
        format.callback(opt);
        let domain = this.domain(opt.url);
        let Cookie = this.getCookie(domain);
        wx.request({
            url: opt.url || "",
            method: 'POST',
            data: opt.data || {},
            header: {
                "content-type": "application/x-www-form-urlencoded",
                "Cookie": Cookie
            },
            success: function (r) {
                this.parseHeader(r.header, domain);
                opt._success(r);
            }.bind(this),
            fail: function (r) {
                opt._fail(r)
            }
        })
    }

    get(opt) {
        opt = opt || {};
        format.callback(opt);
        let domain = this.domain(opt.url);
        let Cookie = this.getCookie(domain);
        wx.request({
            url: opt.url || "",
            method: 'GET',
            data: opt.data || {},
            header: {
                "content-type": "application/x-www-form-urlencoded",
                "Cookie": Cookie
            },
            success: function (r) {
                this.parseHeader(r.header,domain);
                opt._success(r);
            }.bind(this),
            fail: function (r) {
                opt._fail(r)
            }
        })
    }

    random() {
        //随机数生成器
        return Math.random();
    }

    parseHeader(header, domain = "") {
        if (domain == "")
            return;
        if (header['Set-Cookie'] != null) {
            let Cookie = header['Set-Cookie'];
            this.insert(domain, Cookie);
        }
    }

    domain(url = "") {
        let index = url.indexOf("//");
        if (url.length <= 0 || index <= -1) {
            return "";
        }
        let end = url.indexOf("/", index + 2);
        if (end == -1)
            end = url.length;
        return url.substr(index + 2, end - index - 2);
    }

    getCookie(domain){
        let ob = this.search(domain);
        let buff = "";
        if(ob == null)
             return "";
        let arr = ob.value;
        for(let i in arr){
            let item = arr[i];
            if(item.key != ""){
                buff += (item.key+"="+item.value+";");
            }else{
                buff += item.value;
            }
        }
        // console.log(buff);
        return buff;
    }

    search(key) {
        for (let i in this.Cookies) {
            let item = this.Cookies[i];
            if (item.key == key)
                return item;
        }
        return null;
    }

    insert(domain, cookieStr) {
        if(cookieStr == "")
            return;
        //key 是域名
        let domainCookie = this.search(domain);
        //value Cookie 字符串
        let cookies = this.parseCookie(cookieStr);
        if (domainCookie == null)
            this.Cookies.push({
                key: domain,
                value: cookies
            });
        else {
            let befArrCookie = domainCookie.value;//此前cookies
            for(let j in cookies){  //对现在要添加的Cookie遍历
                let nowitem = cookies[j];   //新的cookie
                let hv = false;
                for (let i in befArrCookie) { 
                    let item = befArrCookie[i];
                    if (item.key == nowitem.key){
                        befArrCookie[i].value = nowitem.value;
                        hv = true;
                    }
                    if(!hv){
                        befArrCookie.push(nowitem);
                    }
                }
            }
        }
        //console.log(this.Cookies);
    }

    /*
        将Cookie字符串解析成JSON键值对
    */
    parseCookie(str){
        let arr = [];
        let arrCookie = str.split(";");
        for(let i in arrCookie){
            let item = arrCookie[i];
            let c = item.split("=");
            if(c.length == 2){
                arr.push({
                    key:c[0],
                    value:c[1]
                });
            }else if(c.length == 1){
                arr.push({
                    key:"",
                    value:c[0]
                })
            }
        }
        return arr;
    }

    clear(){
        this.Cookies = [];
    }
}

export default new Ajax();