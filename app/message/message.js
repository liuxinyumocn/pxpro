import format from '../lib/format.js';
import ajax from '../lib/ajax.js';
import config from '../config.js';

class Message {

    constructor() {
        this._opt = {};
        this._Data = [];
        this._notifyData = null;

        this._RedDot=[0,0,0,0,0];

    }

    refreshCommentChange(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/notify/refreshCommentChange',
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    opt._success(res);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

    refreshFollowChange(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/notify/refreshFollowChange',
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    opt._success(res);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });

    }
    refreshLikeChange(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/notify/refreshLikeChange',
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    opt._success(res);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });

    }

    /*
        获取用户顶部数据
    */
    loadHeaderData(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/notify/getHeaderData',
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    this.showRedDot(1, res.followchange);
                    this.showRedDot(2, res.likechangenum);
                    this.showRedDot(3, res.commentchangenum);
                    this.showRedDot(4, res.newnotifynum);
                    opt._success(res);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

    getDataList(opt) {
        opt = opt || {};
        format.callback(opt);
        let reddot = 0;
        opt.page = opt.page || 1;
        ajax.post({
            url: config.pxpro + 'mini/chat/list',
            data: {
                page: opt.page
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    for (let i in res.list) {
                        let c = false;
                        for (let j in this._Data) {
                            if (this._Data[j].id == res.list[i].id) {
                                c = true;
                                break;
                            }
                        }
                        if (!c) {
                            let a = res.list[i];
                            if (res.self == a.auser) {
                                a.user = a.buser;
                                a.haveunread = a.aunread;
                            } else {
                                a.user = a.auser;
                                a.haveunread = a.bunread;
                            }
                            if (a.haveunread != 0)
                                reddot += a.haveunread;
                            this._Data.push(a);
                        }
                    }
                    this.showRedDot(0, reddot);
                    opt._success(this._Data);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

    showRedDot(index,num){
        this._RedDot[index] = num;
        let total = 0;
        for(let i in this._RedDot){
            total += this._RedDot[i];
        }
        if(total>0){
            wx.showTabBarRedDot({
                index: 3,
            })
        }else{
            wx.hideTabBarRedDot({
                index: 3,
            })
        }
    }

}

export default new Message();