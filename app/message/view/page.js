// app/message/view/page.js
import message from '../message.js';
import userInfoCache from '../../user/userInfoCache.js';
import tool from '../../lib/tool.js';
import format from '../../lib/format.js';
import myopus from '../../user/myopus/myopus.js';
import chat from '../chat/chat.js';
import friend from '../../user/friend/friend.js';
import notify from '../notify/notify.js';
import like from '../like/like.js';
import comment from '../comment/comment.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        list:[],
        listIndex:0,
        loading:false,
        followchange:0,
        newlikenum:0,
        newcommentnum:0,
        newnotifynum:0,
        userCancel:true
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        //this.nextPage()
        wx.hideShareMenu({})
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.onPullDownRefresh();
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        this.data.listIndex = 0;
        message._Data = [];
        this.nextPage({
            complete: (res) => {
                wx.stopPullDownRefresh();
            }
        });
        this.data.userCancel = false;
        this.loadHeadData();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.nextPage();
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    nextPage: function (opt) {
        opt = opt || {};
        format.callback(opt);
        if (this.data.loading)
            return;
        getApp().data.user.wxlogin({
            login: this.data.userCancel,
            success:(res)=>{
                wx.showNavigationBarLoading();
                this.data.loading = true;
                message.getDataList({
                    page: this.data.listIndex + 1,
                    success: (res) => {
                        wx.hideNavigationBarLoading();
                        this.data.listIndex++;
                        this.loadData(res);
                        opt._success(res);
                    },
                    fail: (res) => {
                        wx.hideNavigationBarLoading();
                        wx.showToast({
                            title: res.errmsg,
                            icon: 'none'
                        })
                        opt._fail(res);
                    },
                    complete: (res) => {
                        this.data.loading = false;
                    }
                });
            }
        });
    },
    loadData: function (res) {
        let arr = [];
        let finished = 0;
        for (let i = 0; i < res.length; i++) {
            let item = res[i];
            let cell = {
                avatar: '',
                id: item.id,
                nickname: '',
                date: tool.getDateText(item.timestamp),
                content: item.content.length > 16 ? item.content.substr(0, 15) + '...' : item.content,
                tip: item.haveunread == 0 ? false : true,
                sex:0,
                userid:item.user
            }
            arr.push(cell);
            userInfoCache.getUserInfo({
                id: item.user,
                success: (res2) => {
                    finished++;
                    arr[i].avatar = res2.avatar;
                    arr[i].sex = res2.gender;
                    arr[i].nickname = res2.nickname;
                    if(finished == res.length)
                        this.setData({
                            list: arr
                        });
                }
            });
        }
    },
    openUserChat:function(res){
        let user = res.currentTarget.dataset.userid;
        chat.open({
            userid:user
        });
    },
    openUser: function (res) {
        let user = res.currentTarget.dataset.userid;
        myopus.open({
            id:user
        })
    },
    loadHeadData:function(){
        message.loadHeaderData({
            success:(res)=>{
                res.followchange = res.followchange > 99 ? 99 : res.followchange;
                res.likechangenum = res.likechangenum > 99 ? 99 : res.likechangenum; 
                res.commentchangenum = res.commentchangenum > 99 ? 99 : res.commentchangenum;
                res.newnotifynum = res.newnotifynum > 99 ? 99 : res.newnotifynum;
                this.setData({
                    followchange:res.followchange,
                    newlikenum: res.likechangenum,
                    newcommentnum: res.commentchangenum,
                    newnotifynum: res.newnotifynum
                });
            }
        });
    },
    openfans: function () {
        message.refreshFollowChange();
        friend.open({
            type: 0
        });
    }, opennotify:function(){
        notify.open();
    },
    openliked: function () {
        message.refreshLikeChange();
        like.open();
    }, openComment: function () {
        message.refreshCommentChange();
        comment.open();
    }
})