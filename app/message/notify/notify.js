import format from '../../lib/format.js';
import ajax from '../../lib/ajax.js';
import config from '../../config.js';
import browser from '../../../domoe/browser/browser.js';

class Notify {

    constructor() {
        this._opt = {};
        this._data = [];
    }


    open(opt) {
        opt = opt || {};
        format.callback(opt);
        this._opt = opt;
        this._data = [];
        getApp().data.user.wxlogin({
            login: true,
            success: (res) => {
                wx.navigateTo({
                    url: '/app/message/notify/view/page',
                })
            }
        })

    }

    click(opt) {
        opt = opt || {};
        format.callback(opt);
        //标记已读
        ajax.post({
            url: config.pxpro + 'mini/notify/setRead',
            data: {
                id: opt.id
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    for(let i in this._data){
                        if(this._data[i].id == opt.id){
                            this._data[i].isread = 1;
                            opt._success(this._data);
                            return;
                        } 
                    }
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });

        for(let i in this._data){
           if(this._data[i].id == opt.id){
               let json = JSON.parse(this._data[i].content);
               if (json.url != '') {
                   browser.open(json.url);
               }
               return ;
           } 
        }

    }

    getList(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.page = opt.page || 1;
        ajax.post({
            url: config.pxpro + 'mini/notify/getList',
            data: {
                page: opt.page
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    for (let i in res.list) {
                        let c = false;
                        for (let j in this._data) {
                            if (this._data[j].id == res.list[i].id) {
                                c = true;
                                break;
                            }
                        }
                        if (!c) {
                            this._data.push(res.list[i]);
                        }
                    }
                    opt._success(this._data);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

}

export default new Notify();