// app/message/chat/view/page.js
import chat from '../chat.js';
import format from '../../../lib/format.js';
import ad from '../../../lib/ad.js';
import userInfoCache from '../../../user/userInfoCache.js';
import tool from '../../../lib/tool.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        scrollTop:0,
        ad:'',
        inputContent:'',
        loading:false,
        mynickname:'',
        myavatar:'',
        youravatar:'',
        yournickname:'',
        youruserid:0,
        myuserid:0,
        list:[],
        listenStatus:false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        chat.setuserid(options.userid);
        setTimeout(function(){
            this.toUnder();
        }.bind(this),500);
        chat._Data = [];
        this.render();
        this.initAd();
        this.startListen();

        //获取我的头像数据
        let myid = getApp().data.user.getID();
        userInfoCache.getUserInfo({
            id:myid,
            success:(res)=>{
                this.setData({
                    mynickname:res.nickname,
                    myavatar:res.avatar,
                    myuserid:myid
                });
            }
        })
        //获取对方的头像数据
        userInfoCache.getUserInfo({
            id: options.userid,
            success: (res) => {
                this.setData({
                    yournickname: res.nickname,
                    youravatar: res.avatar,
                    youruserid:options.userid
                });
                wx.setNavigationBarTitle({
                    title: res.nickname,
                })
            }
        })
    },
    toUnder:function(){
        this.setData({
            scrollTop: 999999
        });
    },
    initAd:function(){
        this.setData({
            ad: ad.getBanner()
        });
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        console.log('取消监听');
        this.data.listenStatus = false;
    },
    startListen:function(){ //监听对话
        this.data.listenStatus = true;
        function load(){
            if (this.data.listenStatus){
                chat.getUnreadMsg({
                    success: (res) => {
                        this.loadData(res);
                        this.toUnder();
                    },
                    complete:(res)=>{
                        setTimeout(load.bind(this), 3000);
                    }
                });
            }
        }
        load.bind(this)();
    },
    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    loadmore:function(){
        wx.showNavigationBarLoading();
        this.render({
            complete: (res) => {
                wx.hideNavigationBarLoading();
            }
        });
    },
    render:function(opt){
        opt = opt || {};
        format.callback(opt);
        if (this.data.loading)
            return;
        wx.showNavigationBarLoading();
        this.data.loading = true;
        chat.getDataList({
            success: (res) => {
                wx.hideNavigationBarLoading();
                this.loadData(res);
                opt._success(res);
            },
            fail: (res) => {
                wx.hideNavigationBarLoading();
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
                opt._fail(res);
            },
            complete: (res) => {
                this.data.loading = false;
            }
        });
    },
    inputContent:function(res){
        let text = res.detail.value;
        this.data.inputContent = text.trim();
    },
    send: function () {
        if (this.data.inputContent.length > 60) {
            wx.showToast({
                title: '短对话限制在60字内噢～',
                icon: 'none'
            })
            return;
        }
        if (this.data.inputContent.length == 0) {
            wx.showToast({
                title: '您还没有输入内容呢',
                icon: 'none'
            })
            return;
        }
        wx.showNavigationBarLoading();
        chat.sent({
            content:this.data.inputContent,
            success:(res)=>{
                //加载新消息
                this.loadData(res)
                this.setData({
                    inputContent:'',
                    scrollTop: 999999
                });
            },
            fail:(res)=>{
                wx.showToast({
                    title: res.errmsg,
                    icon:'none'
                })
            },
            complete:(res)=>{
                wx.hideNavigationBarLoading();
            }
        });
    },
    loadData: function (res) {
        let arr = [];
        for (let i = res.length-1; i >= 0; i--) {
            let item = res[i];
            let json = JSON.parse(item.content);
            let cell = {
                date: tool.getDateText(item.timestamp),
                content: json.content,
                self: item.self,
                id:item.id
            }
            arr.push(cell);
        }
        this.setData({
            list: arr
        });
        if(arr.length == 0){
            wx.showModal({
                title: '文明交流提醒',
                content: '像素大师提醒您，交流过程中要文明用语，不得进行语言攻击、不得交流损害国家等发言，交流中通过长按聊天内容可进行内容举报，像素大师对于不文明行为将进行严厉打击，文明社区的建设需要大家的共同努力！',
                showCancel: false,
                success: (res) => {
                    this.toUnder();
                }
            })
        }
    },
    menu:function(res){
        let content = res.currentTarget.dataset.content;
        let id = res.currentTarget.dataset.id;
        if(id != -1){
            wx.showActionSheet({
                itemList: ['举报不良发言','复制'],
                success:(res)=>{
                    if(res.tapIndex == 0){
                        chat.msgreport({
                            msgid:id,
                            success:(res)=>{
                                wx.showModal({
                                    title: '提示',
                                    content: '系统成功接收您的反馈，像素大师提倡文明交流，对于不文明行为将严厉打击',
                                    showCancel:false
                                })
                            },
                            fail:(res)=>{
                                wx.showToast({
                                    title: res.errmsg,
                                    icon:'none'
                                })
                            }
                        });
                    } else if (res.tapIndex == 1) {
                        wx.setClipboardData({
                            data: content,
                        })
                    }
                }
            })
        }else{
            wx.showActionSheet({
                itemList: ['复制'],
                success:(res)=>{
                    if(res.tapIndex == 0){
                        wx.setClipboardData({
                            data: content,
                        })
                    }
                }
            })
        }
    }
})