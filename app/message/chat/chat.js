import format from '../../lib/format.js';
import ajax from '../../lib/ajax.js';
import config from '../../config.js';

class Chat {

    constructor() {
        this._opt = {};
        this._Data = [];
    }

    /*
        opt={
            userid
        }
    */
    open(opt) {
        opt = opt || {};
        format.callback(opt);
        this._opt = opt;
        this._Data = [];

        getApp().data.user.wxlogin({
            login: true,
            success: (res) => {
                wx.navigateTo({
                    url: '/app/message/chat/view/page?userid=' + opt.userid,
                })
            }
        })

    }

    setuserid(id) {
        this._opt.userid = id;
    }

    /*
        发布消息
        opt.content
    */
    sent(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/chat/send',
            data: {
                content: opt.content,
                type: 'text',
                id:this._opt.userid
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    this._Data.unshift({
                        id: res.id,
                        content: JSON.stringify({
                            type: 'text',
                            content: opt.content
                        }),
                        self: true
                    });
                    opt._success(this._Data);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

    getUnreadMsg(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/chat/getUnreadMessage',
            data: {
                user: this._opt.userid
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    if(res.list.length != 0){

                        for (let i in res.list) {
                            let c = false;
                            for (let j in this._Data) {
                                if (this._Data[j].id == res.list[i].id) {
                                    c = true;
                                    break;
                                }
                            }
                            if (!c) {
                                let a = res.list[i];
                                if (res.self == a.fromid) {
                                    a.self = true;
                                } else {
                                    a.self = false;
                                }
                                this._Data.unshift(a);
                            }
                        }
                        opt._success(this._Data);

                    }else{
                        opt._fail(res);
                    }
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

    getDataList(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.num = opt.num || this._Data.length;
        ajax.post({
            url: config.pxpro + 'mini/chat/getChatMessage',
            data: {
                id:this._opt.userid,
                from: opt.num
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    for (let i in res.list) {
                        let c = false;
                        for (let j in this._Data) {
                            if (this._Data[j].id == res.list[i].id) {
                                c = true;
                                break;
                            }
                        }
                        if (!c) {
                            let a = res.list[i];
                            if(res.self == a.fromid){
                                a.self = true;
                            }else{
                                a.self = false;
                            }
                            this._Data.push(a);
                        }
                    }
                    opt._success(this._Data);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

    msgreport(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/chat/reportMsg',
            data: {
                msgid: opt.msgid
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    opt._success(res);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

}

export default new Chat();