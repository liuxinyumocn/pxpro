// app/message/like/view/page.js
import like from '../like.js';
import format from '../../../lib/format.js';
import tool from '../../../lib/tool.js';
import userInfoCache from '../../../user/userInfoCache.js';
import myopus from '../../../user/myopus/myopus.js';
import opus from '../../../user/opus/opus.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        opusList: [],
        opusPageIndex: 0,
        loadOpusing: false,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        this.nextPageOpus();
        like.clear();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.nextPageOpus();
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    nextPageOpus:function(opt){
        opt = opt || {};
        format.callback(opt);
        if (this.data.loadOpusing)
            return;
        wx.showNavigationBarLoading();
        this.data.loadOpusing = true;
        like.getOpusList({
            page: this.data.opusPageIndex + 1,
            success: (res) => {
                wx.hideNavigationBarLoading();
                this.data.opusPageIndex++;
                this.loadOpus(res);
                opt._success(res);
            },
            fail: (res) => {
                wx.hideNavigationBarLoading();
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
                opt._fail(res);
            },
            complete: (res) => {
                this.data.loadOpusing = false;
            }
        });
    },
    loadOpus: function (res) {
        let arr = [];
        let finished = 0;
        for (let i =0 ;i<res.length;i++) {
            let item = res[i];
            let cell = {
                avatar: '',
                nickname:'',
                sex:'',
                date:tool.getDateText(item.timestamp),
                id: item.id,
                opusimg: like.getAvatar(item.opus),
                opusid:item.opus,
                userid:item.owner
            }
            arr.push(cell);
            userInfoCache.getUserInfo({
                id: item.owner,
                success: (res2) => {
                    arr[i].avatar = res2.avatar;
                    arr[i].nickname = res2.nickname;
                    arr[i].sex = res2.gender;
                    finished++;
                    if (finished == res.length) {
                        this.setData({
                            opusList: arr
                        })
                        return;
                    }
                }
            });
        }
    },
    openmyopus:function(res){
        let id = res.target.dataset.id;
        myopus.open({id:id})
    },
    openopus: function (res) {
        let id = res.currentTarget.dataset.id;
        opus.open({ id: id })
    }
})