import format from '../../lib/format.js';
import ajax from '../../lib/ajax.js';
import config from '../../config.js';

class Like {

    constructor() {
        this._opt = {};
        this._opusData = [];
        this._avatar = [];
    }

    clear() {
        this._opusData = [];
    }

    open(opt) {
        opt = opt || {};
        format.callback(opt);
        this._opt = opt;

        getApp().data.user.wxlogin({
            login: true,
            success: (res) => {
                wx.navigateTo({
                    url: '/app/message/like/view/page',
                })
            }
        })

    }


    getOpusList(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.page = opt.page || 1;
        ajax.post({
            url: config.pxpro + 'mini/opus/getLikedList',
            data: {
                page: opt.page
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    for (let i in res.list) {
                        let c = false;
                        for (let j in this._opusData) {
                            if (this._opusData[j].id == res.list[i].id) {
                                c = true;
                                break;
                            }
                        }
                        if (!c) {
                            this._opusData.push(res.list[i]);
                        }
                    }
                    //缩略图入库
                    this.loadOpusAvatar(res.opus);
                    opt._success(this._opusData);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

    loadOpusAvatar(opus){
        for(let i in opus){
            let c = false;
            for(let j in this._avatar){
                if(opus[i].id == this._avatar[j].id){
                    c = true;
                    break;
                }
            }
            if(!c)
                this._avatar.push(opus[i]);
        }
    }

    getAvatar(id){
        for(let i in this._avatar){
            if(this._avatar[i].id == id)
                return this._avatar[i].avatar;
        }
        return '';
    }

}

export default new Like();