import format from '../lib/format.js';
import ajax from '../lib/ajax.js';
import tool from '../lib/tool.js';
import config from '../config.js';
import userInfoCache from '../user/userInfoCache.js';

class RankOpus {

    constructor() {
        this._opt = {};
        this._PageIndex = 0;  //页面索引
        this._data = {
            index: 0,
            data: [],
            loading: false
        };

    }

    clear(m) {
        this._data = {
            index: 0,
            data: [],
            loading: false
        }
    }

    isFirst() {
        if (this._data.index == 0)
            return true;
        return false;
    }

    /*
        获取下一页数据
        opt={
            m:0/1/2
        }
    */
    getNextPage(opt) {
        opt = opt || {};
        format.callback(opt);
        if (this._data.loading) {
            opt._fail({ code: 400, errmsg: '正在拼命加载～' });
            return;
        }
        this._data.loading = true;
        setTimeout(function () {
            if (this._data.loading)
                this._data.loading = false;
        }.bind(this), 5000);
        ajax.post({
            url: config.pxpro + 'mini/rank/user',
            data: {
                m: opt.m,
                page: this._data.index + 1
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    for (let i in res.list) {
                        let c = false;
                        for (let j in this._data.data) {
                            if (this._data.data[j].id == res.list[i].id) {
                                c = true;
                                break;
                            }
                        }
                        if (!c) {
                            this._data.data.push(res.list[i]);
                        }
                    }
                    this.parseData({
                        data: this._data.data,
                        success: (res2) => {
                            opt._success({
                                third: res2.third,
                                other: res2.other,
                                header: res.header
                            });
                            this._data.index++;
                        }
                    })
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            },
            complete: () => {
                this._data.loading = false;
            }
        });
    }

    //进一步规则化数据
    /*
        先分出1－3
        再分出其余
    */
    parseData(opt) {
        opt = opt || {};
        format.callback(opt);
        let qiansan = [];
        let other = [];
        //先添加前三用户数据
        let finished = 0;
        for (let i = 0; i < opt.data.length; i++) {
            let item = opt.data[i];
            other.push({
                id: item.id,
                nickname: '',
                likenum: tool.getNum(item.likenum),
                avatar: '',
                sex: '',
                rank:i+1
            });
            userInfoCache.getUserInfo({
                id: item.id,
                success: (res) => {
                    other[i].avatar = res.avatar;
                    other[i].sex = res.gender;
                    other[i].nickname = res.nickname;
                    finished++;
                    if (finished == opt.data.length) {
                        for (let t = 0; t < 3 && t < finished; t++) {
                            qiansan.push(other.shift());
                        }
                        opt._success({
                            third: qiansan,
                            other: other
                        });
                    }
                }
            });
        }

    }


}

export default new RankOpus();