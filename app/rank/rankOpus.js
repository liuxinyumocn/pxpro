import format from '../lib/format.js';
import ajax from '../lib/ajax.js';
import tool from '../lib/tool.js';
import config from '../config.js';
import userInfoCache from '../user/userInfoCache.js';

class RankOpus {

    constructor() {
        this._opt = {};
        this._PageIndex=0;  //页面索引
        this._data = [
            {
                index: 0,
                data: [],
                loading:false
            },
            {
                index: 0,
                data: [],
                loading: false
            },
            {
                index: 0,
                data: [],
                loading: false
            }
        ];

    }

    clear(m){
        this._data[m] = {
            index: 0,
            data: [],
            loading: false
        }
    }

    isFirst(m){
        if(this._data[m].index == 0)
            return true;
        return false;
    }

    /*
        获取选择头
        当日不足上午11点 则统计昨日
        超过11点 则统计今日（1小时更新）

        周一统计上周
        周二统计本周（12小时更新）

        历史（一周更新）
    */
    getHeaderSelect(){
        let res = ['今日','本周','历史'];
        let now = new Date();
        let h = now.getHours();
        if(h < 11)
            res[0] = '昨日';
        let week = now.getDay();
        if(week == 1)
            res[1] = '上周';
        return res;
    }

    /*
        获取下一页数据
        opt={
            m:0/1/2
        }
    */
    getNextPage(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.m = opt.m || 0;
        if(this._data[opt.m].loading){
            opt._fail({code:400,errmsg:'正在拼命加载～'});
            return;
        }
        this._data[opt.m].loading = true;
        setTimeout(function(){
            if (this._data[opt.m].loading)
                this._data[opt.m].loading = false;
        }.bind(this),5000);
        ajax.post({
            url: config.pxpro + 'mini/rank/opus',
            data: {
                m: opt.m,
                page: this._data[opt.m].index+1
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    for (let i in res.list) {
                        let c = false;
                        for (let j in this._data[opt.m].data) {
                            if (this._data[opt.m].data[j].id == res.list[i].id) {
                                c = true;
                                break;
                            }
                        }
                        if (!c) {
                            this._data[opt.m].data.push(res.list[i]);
                        }
                    }
                    this.parseData({
                        data: this._data[opt.m].data,
                        success:(res2)=>{
                            opt._success({
                                third:res2.third,
                                other:res2.other,
                                header:res.header
                            });
                            this._data[opt.m].index++;
                        }
                    })
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            },
            complete:()=>{
                this._data[opt.m].loading=false;
            }
        });
    }

    //进一步规则化数据
    /*
        先分出1－3
        再分出其余
    */
    parseData(opt) {
        opt = opt || {};
        format.callback(opt);
        let qiansan = [];
        let other = [];
        //先添加前三用户数据
        let finished = 0;
        for(let i = 0;i < opt.data.length;i++){
            let item = opt.data[i];
            other.push({
                id:item.id,
                img:item.avatar,
                likenum: tool.getNum(item.likenum),
                avatar:'',
                sex:'',
                userid:item.owner
            });
            userInfoCache.getUserInfo({
                id:item.owner,
                success:(res)=>{
                    other[i].avatar = res.avatar;
                    other[i].sex = res.gender;
                    finished++;
                    if(finished == opt.data.length){
                        for(let t = 0;t<3&&t<finished;t++){
                            qiansan.push(other.shift());
                        }
                        let y = other.length % 3;
                        if(y> 0){
                            for(let i =0;i<y;i++)
                                other.pop();
                        }
                        opt._success({
                            third:qiansan,
                            other:other
                        });
                    }
                }
            });
        }

    }
   

}

export default new RankOpus();