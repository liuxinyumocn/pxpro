// app/rank/view/page.js
import ad from '../../lib/ad.js';
import rankOpus from '../rankOpus.js';
import opus from '../../user/opus/opus.js';
import myopus from '../../user/myopus/myopus.js';
import rankUser from '../rankUser.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        interstitialAd:null,
        menuStyle:[
            'selected',
            '',
            '',
            ''
        ],
        opusThird:[],
        opusOther:[],
        timeselectContent:[
            '昨日','本周','历史'
        ],
        timeselectStyle:[
            'timeselected','',''
        ],
        empty: false,
        userThird: [],
        userOther: [],
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        ad.createInterstitialAd({
            success:(res)=>{
                console.log(res);
                this.data.interstitialAd = res.interstitial;
                //this.onShow();
            },
            fail:(res)=>{
                console.log(res);
            }
        });
        this.loadHeader();
        this.renderData();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        // if(this.data.interstitialAd){
        //     this.data.interstitialAd.show({
        //         fail: (res) => {
        //             console.log(res);
        //         }
        //     });
        // }
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.renderData();
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    menu:function(res){
        let m = res.currentTarget.dataset.m;
        let arr = ['','','',''];
        arr[m] = 'selected';
        this.setData({ menuStyle: arr });
        this.renderData(true);
    },
    timeselect: function (res) {
        let m = res.currentTarget.dataset.m;
        let arr = ['', '', ''];
        arr[m] = 'timeselected';
        this.setData({ timeselectStyle: arr });
        this.renderData(true);
    },
    loadHeader:function(){
       let arr = rankOpus.getHeaderSelect();
       this.setData({
           timeselectContent:arr
       });
    },
    renderData:function(clear = false){
        
        //根据当前选择项加载数据源
        if(this.data.menuStyle[0] != ''){   //加载作品类
            if(this.data.timeselectStyle[0] !=''){ //日榜
                this.loadOpus(0,clear);
            }else if(this.data.timeselectStyle[1] != ''){ //周榜
                this.loadOpus(1, clear);
            } else { //历史总榜
                this.loadOpus(2, clear);
            }
        } else if (this.data.menuStyle[1] != '') { //加载艺术家类
            this.loadUser(clear);
        }

    },
    loadUser:function(clear){
        if(clear)
            rankUser.clear();
        if (rankUser.isFirst()) {
            this.setData({
                userThird: [],
                userOther: [],
                loading: true,
                empty: false
            });
        }
        //加载用户页面数据
        rankUser.getNextPage({
            fail: (res) => {
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
                if (rankUser.isFirst()) {
                    this.setData({
                        empty: true
                    })
                }
            },
            success: (res) => {
                this.setData({
                    userThird: res.third,
                    userOther: res.other
                });
            }
        });
    },
    loadOpus: function (m, clear){
        if(clear){
            rankOpus.clear(m);
        }
        if(rankOpus.isFirst(m)){
            this.setData({
                opusThird: [],
                opusOther: [],
                loading:true,
                empty:false
            });
        }

        //加载作品页面数据
        rankOpus.getNextPage({
            m:m,
            fail: (res) => {
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
                if (rankOpus.isFirst(m)) {
                    this.setData({
                        empty: true
                    })
                }
            },
            success: (res) => {
                if (res.third != null && res.other != null) {
                    this.setData({
                        opusThird: res.third,
                        opusOther: res.other
                    });
                }
            }
        });
    },
    openmyopus: function (res) {
        let id = res.currentTarget.dataset.userid;
        myopus.open({id:id})
    },
    openopus: function (res) {
        let id = res.currentTarget.dataset.id;
        opus.open({ id: id })
    }
})