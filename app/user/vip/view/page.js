// app/user/vip/view/page.js
import config from '../../../config.js';
import vip from '../vip.js';
import tool from '../../../lib/tool.js';
import ad from '../../../lib/ad.js';
import history from '../history/history.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        vip:true,
        isandroid:true,
        value:0,
        length:300,
        date:'-',
        level:1,
        nickname:'-',

        //购买档
        selected: ['', '', 'selected', '', '', ''],
        videoad:false,//视频广告组件
        videoObject:null,   //视频广告对象
        
        signWindow:false,
        signFinished:false,
        signDay:0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        this.setData({
            isandroid: config.system != 'iOS'
        });
        //加载视频广告组件
        this.createRewardVideo();
        this.render();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },
    help2: function (res) {
        console.log(res);
        wx.showModal({
            title: '提示',
            content: '会员由1～5个等级构成，获得对应经验值后将获得永久的会员等级成长，此外会员又由会员时长所限制，当您的会员过期时仅需要通过免费的任务途径重新获得会员时长即可继续享受会员服务。体验会员指的是具有一定时间效益的会员等级，在有效期内可享有体验会员的对应等级特权，体验会员等级不可叠加，当享受在多个体验会员或您已经是更高等级会员，将享受最高档位特权。',
            showCancel: false
        })
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        //this.onPullDownRefresh();
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        wx.showNavigationBarLoading();
        vip.getInfo({
            success: (res) => {
                this.render();
            },
            fail: (res) => {
                wx.showToast({
                    title: res.errmsg,
                    icon:'none'
                })
            },
            complete:(res)=>{
                wx.hideNavigationBarLoading();
                wx.stopPullDownRefresh();
            }
        });
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    render:function(){
        let vipdata = vip.getVIPData();
        this.setData({
            vip:vipdata.vip,
            date: tool.getDateYMD(vipdata.endtime),
            value: vipdata.exp,
            length: vipdata.expdes[2],
            level:vipdata.expdes[0],
            nickname: getApp().data.user._userinfo.data.nickname
        });
    },
    select:function(res){
        let id = res.currentTarget.dataset.pay;
        let arr = ['', '', '', '', '', ''];
        arr[id] = 'selected';
        this.setData({ selected: arr});
    },
    pay:function(){
        wx.showModal({
            title: '提示',
            content: '您购买的是会员等级，将另外赠送您一定天数的会员时长，您获得等级后通过免费途径保持会员将可以一直使用当前购买的会员等级！',
            showCancel:false,
            success:(res)=>{
                let plan = 0;
                for(let i in this.data.selected){
                    if(this.data.selected[i] != '')
                    {
                        plan = i;
                        break;
                    }
                }
                vip.applyPayOrder({
                    plan:plan,
                    success:(res)=>{
                        this.onPullDownRefresh();
                    },
                    fail:(res)=>{
                        wx.showToast({
                            title: res.errmsg,
                            icon:'none'
                        })
                    }
                });
            }
        })

    },
    createRewardVideo: function () { //先创建视频广告单例
        ad.createVideoAd({
            success: (res) => {
                this.setData({
                    videoad: true
                });
                this.data.videoObject = res.video;
            },
            fail: (res) => {
                console.log(res);
            }
        });
    },
    loadVideoAd: function () { //重新加载广告
        this.setData({
            videoad: false
        });
        if (!this.data.videoObject) {
            this.data.videoObject.reload();
        }
    },
    videoadshow:function(){ //播放视频广告
        this.data.videoObject.show({
            success:(res)=>{
                this.loadVideoAd();
                vip.applyRewardVideo({
                    success: (res) => {
                        this.onPullDownRefresh();
                        wx.showModal({
                            title: '提示',
                            content: res.errmsg,
                            showCancel: false
                        })
                    },
                    fail:(res)=>{
                        wx.showModal({
                            title: '提示',
                            content: res.errmsg,
                            showCancel:false
                        })
                    }
                });
            },
            fail:(res)=>{
                wx.showModal({
                    title: '提示',
                    content: '由于您观看中途退出，根据微信平台规则不能给予您对应奖励，是否愿意重新完整的观看一次呢',
                    success:(res)=>{
                        if(res.confirm){
                            this.videoadshow();
                        }
                    }
                })
            }
        });
    },
    followmp:function(){
        wx.previewImage({
            urls: [config.pxpro + 'static/img/help.jpg'],
        })
    },
    posterhelp: function () {
        wx.previewImage({
            urls: [config.pxpro + 'static/img/posterhelp.jpeg'],
        })
    },
    tohome: function () {
        wx.switchTab({
            url: '/app/home/view/page',
        })
    },
    touser: function () {
        wx.switchTab({
            url: '/app/user/view/page',
        })
    },
    closeSign:function(){
        this.setData({
            signWindow:false
        })
    },
    none:function(){

    },
    openSign: function () {
        wx.showLoading({
            title: '请稍后',
        })
        vip.getSignInfo({
            success: (res) => {
                wx.hideLoading();
                this.setData({
                    signDay:res.day,
                    signWindow: true,
                    signFinished: res.finished
                })
            },
            fail:(res)=>{
                wx.hideLoading();
                wx.showToast({
                    title: res.errmsg,
                })
            }
        })
    },
    signToday: function () {
        wx.showLoading({
            title: '请稍后',
        })
        vip.signToday({
            success: (res) => {
                wx.hideLoading();
                this.setData({
                    signDay: res.day,
                    signFinished: true
                })
            },
            fail: (res) => {
                wx.hideLoading();
                wx.showToast({
                    title: res.errmsg,
                })
                if (res.code == 401) {
                    this.setData({
                        signDay: res.day,
                        signFinished: true
                    })
                }
            },
            complete:(res)=>{
                this.onPullDownRefresh();
            }
        })
    },
    history:function(){
        //查看VIP获得记录
        history.open();
    }
})