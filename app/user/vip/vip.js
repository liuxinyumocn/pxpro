import format from '../../lib/format.js';
import ajax from '../../lib/ajax.js';
import config from '../../config.js';
import md5 from '../../lib/md5.js';
import tool from '../../lib/tool.js';
import browser from '../../../domoe/browser/browser.js';
class Vip {

    constructor() {
        this._opt = null;
        this._data = {
            vip:0,
            endtime:0,
            exp:0
        }
    }

    getVIPData(){
        this._data.vip = new Date().getTime()/1000 < this._data.endtime ? true : false;
        return {
            vip:this._data.vip,
            endtime:this._data.endtime,
            exp:this._data.exp,
            expdes:this.exp(this._data.exp)
        };
    }

    /*
        经验值设定
        0~300 Lv1
        300~1800 Lv2
        1800~5400 Lv3
        5400~7200 Lv4
        >7200 Lv5
    */
    exp(exp) {
        if (exp < 300)
            return [1, 0, 300];
        if (exp < 1800)
            return [2, 300, 1800];
        if (exp < 5400)
            return [3, 1800, 5400];
        if (exp < 7200)
            return [4, 5400, 7200];
        return [5, 7200, 99999];
    }

    /*
        opt={
            id
        }
    */
    open(opt) {
        opt = opt || {};
        format.callback(opt);
        this._opt = opt;
        getApp().data.user.wxlogin({
            login: true,
            success: (res) => {
                wx.navigateTo({
                    url: '/app/user/vip/view/page',
                })
            }
        })

    }

    /*
        获取用户VIP信息
    */
    getInfo(opt) {
        opt = opt || {};
        format.callback(opt);
        //网络请求
        ajax.post({
            url: config.pxpro + 'mini/vip/getInfo',
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    if(this.checkSign(res)){
                        this._data = res;
                        if (this._data.exp > 99999)
                            this._data.exp = 99999;
                        opt._success(res);
                    }else{
                        opt._fail({code:400,errmsg:'数据异常'});
                        return;
                    }
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }
        /*
            'code' => 200,
            'vip' => $vip,
            'endtime' => $res['endtimestamp'],
            'exp' => $res['exp'],
            'timestamp' => $time,
            'sign' => md5($user.$vip.$res['endtimestamp'].$res['exp'].$time.VIPTOKEN)
        */
    checkSign(res){
        let str = ''+res.vip+res.endtime+res.exp+res.timestamp+config.viptoken;
        if(res.sign == md5(str))
            return true;
        return false;
    }

    /*
        申请购买VIP
        opt = {
            plan:
        }
    */
    applyPayOrder(opt) {
        opt = opt || {};
        format.callback(opt);
        //申请交易订单
        //网络请求
        ajax.post({
            url: config.pxpro + 'mini/vip/applyVIPDealOrder',
            data:{
                plan:opt.plan
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    browser.forpay(res.order,function(res){
                        opt._success();
                    }.bind(this));
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

    //成功观看1次激励视频申请领取奖励
    applyRewardVideo(opt) {
        opt = opt || {};
        format.callback(opt);
        let timestamp = parseInt(new Date().getTime()/1000);
        let nonceStr = tool.getNonceStr();
        let sign = md5(timestamp + nonceStr + config.viptoken);
        //网络请求
        ajax.post({
            url: config.pxpro + 'mini/vip/applyRewardVideo',
            data: {
                timestamp: timestamp,
                nonceStr:nonceStr,
                sign:sign
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    opt._success(res);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });

    }

    /*
        获取签到数据
    */
    getSignInfo(opt) {
        opt = opt || {};
        format.callback(opt);
        //网络请求
        ajax.post({
            url: config.pxpro + 'mini/sign/getInfo',
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    opt._success(res);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }
    /*
       签到
   */
    signToday(opt) {
        opt = opt || {};
        format.callback(opt);
        //网络请求
        ajax.post({
            url: config.pxpro + 'mini/sign',
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    opt._success(res);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

}

export default new Vip();