import format from '../../../../lib/format.js';
import history from '../history.js';
import tool from '../../../../lib/tool.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        list:[],
        pageIndex:0,
        loading:false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        this.nextPage();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.nextPage();
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    nextPage: function (opt) {
        opt = opt || {};
        format.callback(opt);
        if (this.data.loading)
            return;
        wx.showNavigationBarLoading();
        this.data.loading = true;
        history.getList({
            page: this.data.pageIndex + 1,
            success: (res) => {
                wx.hideNavigationBarLoading();
                this.data.pageIndex++;
                this.loadData(res);
                opt._success(res);
            },
            fail: (res) => {
                wx.hideNavigationBarLoading();
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
                opt._fail(res);
            },
            complete: (res) => {
                this.data.loading = false;
            }
        });
    },
    loadData: function (res) {
        let arr = [];
        let now = new Date().getTime() / 1000;
        for (let i in res) {
            let item = res[i];
            let cell = {
                title: history.getTime(item.addtime,item.addexp),
                id: item.id,
                enddate: item.aftertime - now > 86400 * 366 * 10 ? '永不' : tool.getDateYMD(item.aftertime),
                gettype: item.gettype == 1 ? '(免费)' : '(付费)',
                date: tool.getDateYMD(item.timestamp),
                des: item.getdec
            }
            arr.push(cell);
        }
        this.setData({
            list: arr
        });
    }
})