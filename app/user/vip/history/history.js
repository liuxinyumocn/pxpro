import format from '../../../lib/format.js';
import ajax from '../../../lib/ajax.js';
import config from '../../../config.js';
import vip from '../vip.js';
class History {

    constructor() {
        this._opt = {};
        this._data = [];
    }

    /*
        opt={
            
        }
    */
    open(opt) {
        opt = opt || {};
        format.callback(opt);
        this._opt = opt;
        this._data = [];

        getApp().data.user.wxlogin({
            login: true,
            success: (res) => {
                wx.navigateTo({
                    url: '/app/user/vip/history/view/page',
                })
            }
        })

    }


    getList(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.page = opt.page || 1;
        ajax.post({
            url: config.pxpro + 'mini/vip/historyList',
            data: {
                page: opt.page
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    for (let i in res.list) {
                        let c = false;
                        for (let j in this._data) {
                            if (this._data[j].id == res.list[i].id) {
                                c = true;
                                break;
                            }
                        }
                        if (!c) {
                            this._data.push(res.list[i]);
                        }
                    }
                    opt._success(this._data);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

    /*
        计算最小时间表达单元
    */
    getTime(v,exp){
        let y,m,d,h;
        y = parseInt(v / 86400 / 366);
        if(y > 10)
            return '永久会员 + Lv5';
        let str = '';
        if(y > 0){
            v -= y*86400*366;
            str = y+'年';
        }
        m = parseInt(v / 86400/31);
        if(m > 0){
            v -= m*86400*31;
            str += m +'个月';
        }
        d = parseInt(v / 86400);
        if(d > 0){
            v -= d * 86400;
            str += d + '天';
        }
        h = parseInt(v / 3600);
        if(h > 0)
            str += h + '小时';
        str += ' + Lv'+vip.exp(exp)[0];
        return str;
    }
}

export default new History();