// app/user/upload/view/page.js
import upload from '../upload.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        img:'',

        tapInputShow:false,
        tapInputFocus:false,
        tapData:[],

        opusType:1,
        opusPrivate:false,
        opusContent:'',

        uploading:false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        this.init();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    init:function(){
        this.setData({
            img:upload._opt.img
        });
    },
    finished:function(){
        if(this.data.tapData.length == 0){
            wx.showModal({
                title: '提示',
                content: '设置与主题相吻合的标签可以让更多的人看你的作品，真的不设置标签就发布嘛？',
                success:(res)=>{
                    if(res.confirm){
                        this._finished();
                    }
                }
            })
        }else{
            //直接发布
            this._finished();
        }
    },
    _finished:function(){
        if (this.data.uploading)
            return;
        this.data.uploading = true;
        wx.showLoading({
            title: '正在发布',
        })
        upload.upload({
            content: this.data.opusContent,
            type: this.data.opusType,
            tap: this.data.tapData,
            private: this.data.opusPrivate,
            success: (res) => {
                wx.hideLoading();
                wx.showToast({
                    title: '发布成功'
                })
            },
            fail: (res) => {
                wx.hideLoading();
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
            },
            complete: (res) => {
                this.data.uploading = false;
            }
        });
    },
    tapFinished:function(res){
        let tap = res.detail.value;

        //过滤不合法词
        let ill = ['[', ']', ',', '"'];
        for (let i in ill) {
            if (tap.indexOf(ill[i]) != -1) {
                wx.showToast({
                    title: '标签中不能带有特殊符号噢',
                    icon: 'none'
                })
                return;
            }
        }

        let c = false;
        for(let i in this.data.tapData){
            if(this.data.tapData[i] == tap)
                c = true;
        }
        if(this.data.tapData.length > 5){
            wx.showToast({
                title: '最多只可以添加6个标签哦',
                icon:'none'
            })
            c=true;
        }
        if(tap != '' && !c){
            this.data.tapData.push(tap);
            this.setData({
                tapData: this.data.tapData
            });
            wx.showToast({
                title: '添加成功，长按可删除',
                icon:'none'
            })
        }
        this.setData({
            tapInputShow: false
        })

    },
    addTap:function(){
        this.setData({
            tapInputShow:true,
            tapInputFocus:true
        });
    },
    delTap:function(res){
        let tap = res.target.dataset.tap;
        let arr = [];
        for(let i in this.data.tapData){
            if(this.data.tapData[i] != tap){
                arr.push(this.data.tapData[i]);
            }
        }
        this.setData({
            tapData:arr
        })
    },
    opusType:function(res){
        let value = res.detail.value;
        this.data.opusType = value;
        //原创使用提醒
        if(value == '3'){
            wx.getStorage({
                key: 'yuanchuangtip',
                fail: function(res) {
                    wx.showModal({
                        title: '原创提醒',
                        content: '原创作品必须为您独立构思创作的作品，您声明原创后，像素大师将保留对您作品未来的相关法律追究的权利，如若产生侵权纠纷您将承担全部的法律责任。如果您是描绘他人作品请选择临摹，如果您基于他人作品的再创作，请选择同人。是否以后不再弹出该提醒？',
                        success:(res)=>{
                            if(res.confirm){
                                wx.setStorage({
                                    key: 'yuanchuangtip',
                                    data: '1',
                                })
                            }
                        }
                    })
                }
            })
        }
    },
    opusPrivate:function(res){
        let prt = res.detail.value.length == 0;
        this.data.opusPrivate = prt;
    },
    inputContent:function(res){
        let text = res.detail.value;
        this.data.opusContent = text;
    }
})