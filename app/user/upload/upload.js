import format from '../../lib/format.js';
import ajax from '../../lib/ajax.js';
import cosSDK from '../../extend/qq/cos/sdk.js';
import config from '../../config.js';
import opus from '../opus/opus.js';

class Upload {

    constructor() {
        this._opt = null;
    }

    /*
        opt={
            img
            file
        }
    */
    open(opt) {
        opt = opt || {};
        format.callback(opt);
        this._opt = opt;

        getApp().data.user.wxlogin({
            login: true,
            success: (res) => {
                wx.navigateTo({
                    url: '/app/user/upload/view/page',
                })
            }
        })

    }

    /*
        第一阶段上传
        opt{
            content:this.data.content,
            type:this.data.opusType,
            tap:this.data.tapData,
            private:this.data.opusPrivate,}
    */
    upload(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/opus/createOpus',
            data:{
                content:opt.content,
                copyright:opt.type,
                tag:JSON.stringify(opt.tap),
                private:opt.private?1:2,
                avatarFix:'jpeg'
            },
            success:(res)=>{
                res = res.data;
                if(res.code == 200){
                    //开始COS 缩略图上传
                    cosSDK({
                        temKey: res.avatarToken,
                        temPath: this._opt.img,
                        fileName:res.avatar,
                        success:(res2)=>{
                            //开始COS 文件上传
                            cosSDK({
                                temKey: res.fileToken,
                                temPath: this._opt.file,
                                fileName:res.file,
                                success:(res2)=>{
                                    //成功 汇报上传结果
                                    ajax.post({
                                        url:config.pxpro + 'mini/opus/finishedUploadOpus',
                                        data:{
                                            id:res.id
                                        },
                                        success:(res3)=>{
                                            if(res3.data.code == 200){
                                                //刷新
                                                getApp().data.user.loadUserMain();
                                                opt._success();
                                                this._opt.success();
                                                setTimeout(function () {
                                                    wx.navigateBack({
                                                        delta:999,
                                                        success:(res2)=>{
                                                            //del／／／／
                                                            setTimeout(function () {
                                                                opus.open({
                                                                    id: res.id
                                                                });
                                                            }, 500);
                                                        }
                                                    })
                                                }, 1500);
                                            }else{
                                                opt._fail(res3.data);
                                            }
                                        },
                                        fail:(res)=>{
                                            opt._fail({
                                                code:400,
                                                errmsg:'网络异常'
                                            });
                                        }
                                    })
                                },
                                fail: (res) => {
                                    opt._fail(res);
                                }
                            })
                        },
                        fail: (res) => {
                            opt._fail(res);
                        }
                    });
                }else{
                    opt._fail(res);
                }
            },
            fail:(res)=>{
                opt._fail({code:400,errmsg:'网络异常'});
            }
        });
    }

}

export default new Upload();