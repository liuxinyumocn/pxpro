import format from '../../lib/format.js';
import config from '../../config.js';
import ajax from '../../lib/ajax.js';
class Login{

    constructor(){
        this._opt = null;
    }

    open(opt) {
        opt = opt || {};
        format.callback(opt);
        this._opt = opt;
        this._opt.updateInfo = this._opt.updateInfo || false;
        this._opt.bindtel = this._opt.bindtel || false;
        wx.navigateTo({
            url: '/app/user/login/view/page',
        })
    }


    /*
        使用微信加密手机号数据快速登录
        opt = {
            encryptedData,
            iv
        }
    */
    login(opt) {
        opt = opt || {};
        format.callback(opt);
        if(this._opt.updateInfo){
            ajax.post({
                url: config.pxpro + 'mini/user/updateInfo',
                data: {
                    rawData: opt.rawData,
                    signature: opt.signature,
                },
                success: (res) => {
                    if (res.data.code == 200) {
                        getApp().data.user.loadUserInfo();
                        opt._success(res.data);
                            wx.navigateBack({})
                        setTimeout(function () {
                            this._opt._success();
                        }.bind(this), 400);
                    } else {
                        opt._fail(res.data);
                    }
                },
                fail: (res) => {
                    opt._fail({
                        code: 400,
                        errmsg: '网络异常'
                    });
                }
            });
        }else{
            ajax.post({
                url: config.pxpro + 'mini/user/loginByWXPhone',
                data: {
                    encryptedData: opt.encryptedData,
                    iv: opt.iv,
                    rawData: opt.rawData,
                    signature: opt.signature,
                },
                success: (res) => {
                    if (res.data.code == 200) {
                        let user = getApp().data.user;
                        user._userinfo.account = res.data.id;
                        if (res.data.i)
                            config.system = 'Android';
                        user.loadUserInfo()
                        user.loadUserMain();
                        opt._success(res.data);
                        wx.showToast({
                            title: '成功',
                        })
                        setTimeout(function () {
                            wx.navigateBack({})
                            setTimeout(function () {
                                this._opt._success();
                            }.bind(this), 400);
                        }.bind(this), 1100);
                    } else {
                        opt._fail(res.data);
                    }
                },
                fail: (res) => {
                    opt._fail({
                        code: 400,
                        errmsg: '网络异常'
                    });
                }
            });
        }
    }

    /*
        绑定手机号
    */
    applybindtel(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/user/applybindTel',
            data: {
                encryptedData: opt.encryptedData,
                iv: opt.iv
            },
            success: (res) => {
                if (res.data.code == 200) {
                    opt._success(res.data);
                    wx.navigateBack({})
                    setTimeout(function () {
                        this._opt._success();
                    }.bind(this), 400);
                } else {
                    opt._fail(res.data);
                }
            },
            fail: (res) => {
                opt._fail({
                    code: 400,
                    errmsg: '网络异常'
                });
            }
        });

    }

    /*
        游客访问
        opt = {
            encryptedData,
            iv
        }
    */
    unnamelogin(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/user/loginByUnnamed',
            data: {
                rawData: opt.rawData,
                signature: opt.signature,
            },
            success: (res) => {
                if (res.data.code == 200) {
                    let user = getApp().data.user;
                    user._userinfo.account = res.data.id;
                    if (res.data.i)
                        config.system = 'Android';
                    user.loadUserInfo()
                    user.loadUserMain();
                    opt._success(res.data);
                    wx.showToast({
                        title: '成功',
                    })
                    setTimeout(function () {
                        wx.navigateBack({})
                        setTimeout(function () {
                            this._opt._success();
                        }.bind(this), 400);
                    }.bind(this), 1100);
                } else {
                    opt._fail(res.data);
                }
            },
            fail: (res) => {
                opt._fail({
                    code: 400,
                    errmsg: '网络异常'
                });
            }
        });

    }
}

export default new Login();