// app/user/login/view/page.js
import login from '../login.js';
import browser from '../../../../domoe/browser/browser.js';
import config from '../../../config.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        level:1,
        phone:null,
        infobutton:'第二步：授权您的基本信息',
        unname:false,
        gettelnumber:'第一步：使用手机号登录',
        bindtel:false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        if (login._opt.updateInfo) {
            this.setData({ level: 2, infobutton: '更新您的形象（昵称、头像、性别等）' })
        }
        if (login._opt.bindtel) {
            this.setData({ bindtel: login._opt.bindtel, gettelnumber: '绑定您的手机号' })
        }
        this.data.unname = false;
    },

    /**
     * 生命周期函数--监
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    getphonenumber:function(res){
        if (res.detail.errMsg != 'getPhoneNumber:ok'){
            wx.showToast({
                title: '像素大师需要您的手机号进行登录',
                icon:'none'
            })
            return ;
        }
        this.data.phone = res.detail;
        if (this.data.bindtel) {
            wx.showLoading({
                title: '请稍后',
            })
            login.applybindtel({
                encryptedData: this.data.phone.encryptedData,
                iv: this.data.phone.iv,
                success: (res) => {
                    wx.hideLoading();
                },
                fail: (res) => {
                    wx.hideLoading();
                    wx.showToast({
                        title: res.errmsg,
                        icon: 'none'
                    })
                }
            });
            return;
        }

        wx.showToast({
            title: '还差1步就成功啦',
            icon:'none'
        })
        this.setData({
            level:2
        });
    },
    getuserinfo:function(res){
        if (res.detail.errMsg != 'getUserInfo:ok') {
            wx.showToast({
                title: '像素大师需要您公众形象',
                icon: 'none'
            })
            return;
        }
        this.data.userinfo = res.detail;
        wx.showLoading({
            title: '请稍后',
        })
        if (login._opt.updateInfo) {
            login.login({
                rawData: res.detail.rawData,
                signature: res.detail.signature,
                success: (res) => {
                    wx.hideLoading();
                },
                fail: (res) => {
                    wx.hideLoading();
                    wx.showToast({
                        title: res.errmsg,
                        icon: 'none'
                    })
                }
            });
        } else {
            if(!this.data.unname)
                login.login({
                    encryptedData: this.data.phone.encryptedData,
                    iv: this.data.phone.iv,
                    rawData: res.detail.rawData,
                    signature: res.detail.signature,
                    success: (res) => {
                        wx.hideLoading();
                    },
                    fail: (res) => {
                        wx.hideLoading();
                        wx.showToast({
                            title: res.errmsg,
                            icon: 'none'
                        })
                    }
                });
            else
                login.unnamelogin({
                    rawData: res.detail.rawData,
                    signature: res.detail.signature,
                    success: (res) => {
                        wx.hideLoading();
                    },
                    fail: (res) => {
                        wx.hideLoading();
                        wx.showToast({
                            title: res.errmsg,
                            icon: 'none'
                        })
                    }
                });
        }
    },
    argeement: function () {
        browser.open(config.pxpro + 'agreement.html');
    },
    argeement2: function () {
        browser.open(config.pxpro + 'privacy.html');
    },
    unname:function(){
        wx.showModal({
            title: '使用提示',
            content: '您在使用游客身份访问时请注意：\r\n1.游客不允许发布公开作品。\r\n2.在绑定手机号前不能切换其他手机号登录，且不能绑定已经被注册的手机号。\r\n3.使用游客前请确保未注册过。\r\n您是否继续使用游客身份？',
            success:(res)=>{
                if(res.confirm){
                    this.data.unname = true;
                    wx.showToast({
                        title: '还差1步就成功啦',
                        icon: 'none'
                    })
                    this.setData({
                        level: 2
                    });
                }
            }
        })
    }
})