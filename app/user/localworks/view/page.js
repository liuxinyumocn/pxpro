// app/user/localworks/view/page.js
import localworks from '../localworks.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        list:[]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        //this.loadlist();
        wx.hideShareMenu({})
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.loadlist();
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
        this.loadlist();
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    loadlist:function(){
        localworks.load({
            success:(res)=>{
               console.log(res);
               this.setData({
                   list:res.data
               }); 
            }
        });
    },
    del:function(res2){
        wx.showModal({
            title: '提示',
            content: '真的要删除这份作品嘛',
            success:(res)=>{
                if(res.confirm){
                    localworks.del({
                        file:res2.target.dataset.file,
                        success:(res)=>{
                            wx.showToast({
                                title: '删除成功',
                            })
                            this.onShow();
                        },
                        fail:(res)=>{
                            wx.showToast({
                                title: res.errmsg,
                                icon: 'none'
                            })    
                        }
                    });
                }
            }
        })
    },
    edit:function(res2){
        getApp().data.user.wxlogin({
            login: true,
            success: (res) => {
                localworks.edit({
                    file: res2.target.dataset.file,
                    fail: (res) => {
                        wx.showToast({
                            title: res.errmsg,
                            icon: 'none'
                        })
                    }
                });
            }
        })
    }
})