import format from '../../lib/format.js';
import File from '../../Main/File.js';
import draw from '../../draw/draw.js';

class Localwords{

    constructor(){
        this._File = new File('localworks');
    }

    open(opt) {
        opt = opt || {};
        format.callback(opt);
        wx.navigateTo({
            url: '/app/user/localworks/view/page',
        })
    }

    edit(opt) {  //编辑
        opt = opt || {};
        format.callback(opt);
        draw.launch({
            file:opt.file
        });
    }

    del(opt) {
        opt = opt || {};
        format.callback(opt);
        this._File.del(opt);
    }

    load(opt) {
        opt = opt || {};
        format.callback(opt);
        this._File.readDir({
            success:(res)=>{
                console.log(res);
                let data = [];
                let num = 0;
                if(res.files.length == 0)
                    opt._success({
                        code: 200,
                        data: data
                    });
                for(let i in res.files){
                    let item = res.files[i];
                    this._File.getInfo({
                        file:item,
                        success:(res2)=>{
                            data.push({
                                name:this.getName(item,'.pxpro'),
                                size:this.getSize(res2.size)+'M',
                                file:item
                            });
                            num++;
                            if(num == res.files.length){
                                opt._success({
                                    code:200,
                                    data: data
                                });
                            }
                        }
                    })
                }
            }
        })
    }

    getName(str,z){
        let name =  str.substr(0, str.indexOf(z));
        if(name == 'TempProject')
            return '临时作品';
        return name;
    }

    getSize(z) {
        let n = z/1024/1024;
        if(n<0.1)
            n= 0.1;
        return  parseInt(n*10)/10;
    }

}

export default new Localwords();