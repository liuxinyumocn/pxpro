// app/user/mylikes/view/page.js
import mylikes from '../mylikes.js';
import format from '../../../lib/format.js';
import tool from '../../../lib/tool.js';
import opus from '../../opus/opus.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        opusPageIndex: 0,
        loadOpusing: false,
        opusList: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        mylikes.setid(options.id);
        this.nextPageOpus();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        this.data.opusPageIndex = 0;
        mylikes._opusData = [];
        this.nextPageOpus({
            complete: (res) => {
                wx.stopPullDownRefresh();
            }
        });
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.nextPageOpus();
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    nextPageOpus: function (opt) {
        opt = opt || {};
        format.callback(opt);
        if (this.data.loadOpusing)
            return;
        wx.showNavigationBarLoading();
        this.data.loadOpusing = true;
        mylikes.getOpusList({
            page: this.data.opusPageIndex + 1,
            success: (res) => {
                wx.hideNavigationBarLoading();
                this.data.opusPageIndex++;
                this.loadOpus(res);
                opt._success(res);
            },
            fail: (res) => {
                wx.hideNavigationBarLoading();
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
                opt._fail(res);
            },
            complete: (res) => {
                this.data.loadOpusing = false;
            }
        });
    },
    loadOpus: function (res) {
        let arr = [];
        for (let i in res) {
            let item = res[i];
            let cr = tool.getCoyeRight(item.copyright);
            let cell = {
                avatar: item.avatar,
                id: item.id,
                date: tool.getDateText(item.timestamp),
                copyright: cr.title,
                copyrightColor: cr.color,
                private: item.private,
                likenum: tool.getNum(item.likenum)
            }
            arr.push(cell);
        }
        this.setData({
            opusList: arr
        });
    },
    openOpus: function (res) {
        let id = res.currentTarget.dataset.id;
        opus.open({ id: id });
    }
})