import format from '../../lib/format.js';
import ajax from '../../lib/ajax.js';
import config from '../../config.js';
import App from '../../Main/App.js';
import task from '../task.js';

class Opus {

    constructor() {
        this._opt = {};
        this._data = {}

        this._commentData = [];
    }

    /*
        opt={
            id
        }
    */
    open(opt) {
        opt = opt || {};
        format.callback(opt);
        this._opt = opt;
        this._commentData = [];
        wx.navigateTo({
            url: '/app/user/opus/view/page?id=' + opt.id,
        })

    }

    setId(id){
        this._opt.id = id;
    }

    /*
        关联上屏Canvas
        opt={
            id:
        }
    */
    bindCanvas(opt) {
        opt = opt || {};
        format.callback(opt);
        this._App = new App({
            canvasid: opt.id,
            shareCanvas: opt.shareCanvas,
            tempfile: opt.tempfile,
            success:(res)=>{
                opt._success(res);
            }
        });
    }
    touchstart(touches) {
        if (this._App)
            this._App.touchstart(touches);
    }

    touchend(touches) {
        if (this._App)
            this._App.touchend(touches);
    }

    touchmove(touches) {
        if (this._App)
            this._App.touchmove(touches);
    }

    touchcancel(touches) {
        if (this._App)
            this._App.touchcancel(touches);
    }

    handModel() {
        if (this._App) {
            this._App.handModel();
            setTimeout(function () {
                this._App.suitview();
                this._App.guideStatus(false);
                }.bind(this),50);
        }
    }

    suitView(){
        this._App.suitview();
    }

    gridStatus(s) {
        if (this._App)
            return this._App.gridStatus(s);
    }

    download(opt) {
        opt = opt || {};
        format.callback(opt);
        //获取作品信息
        ajax.post({
            url:config.pxpro + 'mini/opus/look',
            data:{
                id: this._opt.id,
            },
            success:(res)=>{
                res = res.data;
                if(res.code == 200){
                    console.log(res);
                    this._data = res;
                    //下载用户信息数据

                    //下载数据
                    wx.downloadFile({
                        url:res.data.file,
                        success:(res)=>{
                            let path = res.tempFilePath;
                            opt._success({
                                data : this._data,
                                file:path
                            });
                        },
                        fail: (res) => {
                            opt._fail({ code: 400, errmsg: '网络异常' })
                        }
                    })
                }else{
                    opt._fail(res);
                }
            },
            fail:(res)=>{
                opt._fail({code:400,errmsg:'网络异常'})
            }
        });
    }

    like(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/opus/like',
            data: {
                id: this._opt.id,
                like: opt.like
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    this._data.data.likenum += res.have;
                    opt._success({
                        data: res,
                        num: this._data.data.likenum
                    });
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

    collect(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/opus/collect',
            data: {
                id: this._opt.id,
                collect: opt.collect
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    this._data.data.collectnum += res.have;
                    opt._success({
                        data: res,
                        num: this._data.data.collectnum
                    });
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

    comment(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/comment',
            data: {
                id: this._opt.id,
                content: opt.content
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    let data = [];
                    data.push(res.data);
                    for(let i in this._commentData){
                        data.push(this._commentData[i]);
                    }
                    this._commentData = data;
                    opt._success(this._commentData);
                    task.checkComment({
                        commentid:res.data.id,
                        success:(res)=>{
                            wx.showToast({
                                title: '奖励已到账！',
                            })
                        }
                    })
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

    getCommentList(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.page = opt.page || 1;
        ajax.post({
            url: config.pxpro + 'mini/comment/getList',
            data: {
                id: this._opt.id,
                page: opt.page
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    for (let i in res.list) {
                        let c = false;
                        for (let j in this._commentData) {
                            if (this._commentData[j].id == res.list[i].id) {
                                c = true;
                                break;
                            }
                        }
                        if (!c) {
                            this._commentData.push(res.list[i]);
                        }
                    }
                    opt._success(this._commentData);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }


    del(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/opus/del',
            data: {
                id: this._opt.id
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    opt._success(res);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }
}

export default new Opus();