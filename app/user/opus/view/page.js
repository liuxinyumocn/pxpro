// app/user/opus/view/page.js
import opus from '../opus.js';
import tool from '../../../lib/tool.js';
import userInfoCache from '../../../user/userInfoCache.js';
import myopus from '../../myopus/myopus.js';
import search from '../../../home/search/search.js';
import ad from '../../../lib/ad.js';
import report from '../../report.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        ad:'',
        artistAvatar:'',
        nickname:'-',
        artistID:'',
        sex:0,
        followshow:false,
        like:{
            src:'love.png', //loved.png
            num:0
        },
        content:'',
        tag: [],
        watch:'-',
        collect:{
            src:'collect.png',
            num:'-'
        },
        date:'',
        commentnum:0,
        userCommentContent:'',
        commentPageIndex:0,
        commentList:[],
        loadList:false,
        self:false,
        copyright:0,
        loging:false,
        backhome:false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        opus.setId(options.id);
        this.setData({
            ad: ad.getBanner('opus')
        });
        this.checkLog();
        setTimeout(function(){
            this.data.loging = true;
        }.bind(this),600);
    },
    checkLog:function(){
        getApp().data.user.wxlogin({
            login: true,
            success: (res) => {
                this.data.loging = false;
                this.init();
            }
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function (res) {
        let scene = wx.getLaunchOptionsSync().scene;
        if (scene == 1008 || scene == 1007 || scene == 1101)
            this.setData({
                backhome : true
            });
        setTimeout(function(){
            if (this.data.loging && !getApp().data, user.islog()){
               wx.showModal({
                   title: '提示',
                   content: '您未能成功登录，点击确定重新登录，取消则返回主页',
                   success: (res) => {
                       if (res.confirm) {
                           this.checkLog();
                       } else {
                           wx.switchTab({
                               url: '/app/home/view/page',
                           })
                       }
                   }
               })
           }
        }.bind(this),200);
    },
    backhome: function () {
        wx.switchTab({
            url: '/app/home/view/page',
        })
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        //下一页
        this.nextPageComment();
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function (res) {
        
        
    },
    share:function(){
        console.log(1)
        wx.showShareMenu({
            withShareTicket: true
        })
    },
    touchstart: function (res) {
        opus.touchstart(res.touches);
    },
    touchmove: function (res) {
        opus.touchmove(res.touches);

    },
    touchend: function (res) {
        opus.touchend(res.changedTouches);

    },
    touchcancel: function (res) {
        opus.touchcancel(res.touches);
    },
    handModel: function () {
        opus.handModel();
    },
    centerButtonLong:function(){
        opus.suitView();
    },
    centerButton:function(){
        //获取网格状态
        let grid = opus.gridStatus();
        let text = grid ? '关闭网格' : '开启网格';

        wx.showActionSheet({
            itemList: [text,'调整到最佳视角'],
            success:(res)=>{
                if (res.tapIndex == 0) {
                    opus.gridStatus(!grid);
                }else if(res.tapIndex == 1){
                    this.centerButtonLong();
                }
            }
        })
    },
    init:function(){
        wx.showLoading({
            title: '请稍后',
        })
        //初始化
        //下载数据
        opus.download({
            fail: (res) => {
                wx.hideLoading();
                wx.showToast({
                    title: res.errmsg,
                    icon: "none"
                })
                setTimeout(function () {
                    wx.navigateBack({})
                }, 1500);
            },
            success:(res)=>{
                //加载作者头像
                userInfoCache.getUserInfo({
                    id:res.data.data.owner,
                    success:(res)=>{
                        this.setData({
                            artistAvatar:res.avatar,
                            nickname:res.nickname,
                            sex:res.gender,
                            artistID:res.id
                        });
                    },
                    fail:(res)=>{
                        wx.showToast({
                            title: res.errmsg,
                            icon:'none'
                        })
                    }
                });

                let tag = [];
                let arr = JSON.parse(res.data.data.tags);
                for(let i in arr){
                    tag.push({
                        title:arr[i],
                        id:i
                    });
                }
                
                this.setData({
                    like: {
                        src: res.data.like != 1 ? 'love.png':'loved.png',
                        num: tool.getNum(res.data.data.likenum)
                    },
                    content: res.data.data.content,
                    tag: tag,
                    watch: tool.getNum(res.data.data.watch + 1),
                    collect: {
                        src: res.data.collect != 1 ? 'collect.png' : 'collected.png',
                        num: tool.getNum(res.data.data.collectnum)
                    },
                    date: tool.getDate(res.data.data.timestamp),
                    commentnum: tool.getNum(res.data.data.comment), 
                    followshow: !res.data.self && res.data.follow == 0,
                    self:res.data.self,
                    copyright:res.data.data.copyright
                });
                wx.hideLoading();
                //接入Canvas渲染引擎
                opus.bindCanvas({
                    id: 'canvas',
                    shareCanvas: 'shareCanvas',
                    tempfile:res.file,
                    success: (res) => {
                        this.handModel();
                    }
                });
                this.nextPageComment();
            }
        });

    },
    like: function () {
        wx.showNavigationBarLoading();
        let like = this.data.like.src == 'love.png' ? 1 : 0;
        opus.like({
            like: like,
            success: (res) => {
                this.setData({
                    like: {
                        src: res.data.like != 1 ? 'love.png' : 'loved.png',
                        num: tool.getNum(res.num)
                    }
                });
            },
            fail:(res)=>{
                wx.showToast({
                    title: res.errmsg,
                    icon:'none'
                })
            },
            complete:(res)=>{
                wx.hideNavigationBarLoading();
            }
        });
    },
    collect:function(){
        wx.showNavigationBarLoading();
        let collect = this.data.collect.src == 'collect.png' ? 1 : 0;
        opus.collect({
            collect: collect,
            success: (res) => {
                wx.showToast({
                    title: res.data.collect == 1 ? '收藏成功' : '取消收藏'
                })
                this.setData({
                    collect: {
                        src: res.data.collect != 1 ? 'collect.png' : 'collected.png',
                        num: tool.getNum(res.num)
                    }
                });
            },
            fail: (res) => {
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
            },
            complete: (res) => {
                wx.hideNavigationBarLoading();
            }
        });
    },
    submitComment:function(res){
        let content = res.detail.value.trim();
        if(content.length == 0){
            wx.showToast({
                title: '说点啥再发布吧～',
                icon:'none'
            })
            return;
        }
        wx.showNavigationBarLoading();
        opus.comment({
            content:content,
            success:(res)=>{
                wx.hideNavigationBarLoading();
                wx.showToast({
                    title: '评论成功',
                })
                this.setData({ userCommentContent: '', commentnum: this.data.commentnum+1})
                //新增一个评论
                this.loadComment(res);
            },
            fail: (res) => {
                wx.hideNavigationBarLoading();
                wx.showToast({
                    title: res.errmsg,
                    icon:'none'
                })
            }
        });
    },
    nextPageComment:function(){
        if(this.data.loadList)
            return;
        wx.showNavigationBarLoading();
        this.data.loadList = true;
        opus.getCommentList({
            page:this.data.commentPageIndex+1,
            success: (res) => {
                wx.hideNavigationBarLoading();
                this.data.commentPageIndex++;
                this.loadComment(res);
            },
            fail: (res) => {
                wx.hideNavigationBarLoading();
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
            },
            complete:(res)=>{
                this.data.loadList = false;
            }
        });
    },
    loadComment:function(res){
        //渲染评论
        let data = [];
        /*
            {
                avatar
                nickname
                timetext
                content
                userid
            }
        */
        let finished = 0;
        for (let i in res) {
            data.push({
                avatar: '',
                nickname: '',
                timetext: tool.getDateText(res[i].timestamp),
                content: res[i].content,
                userid: res[i].userid,
                id: res[i].id,
                sex:0
            })
            userInfoCache.getUserInfo({
                id: res[i].userid,
                success: (res2) => {
                    for (let i in data) {
                        if (data[i].userid == res2.id) {
                            data[i].avatar = res2.avatar;
                            data[i].nickname = res2.nickname;
                            data[i].sex = res2.gender
                        }
                    }
                    if (++finished == res.length) {
                        //最终渲染
                        this.setData({
                            commentList: data
                        });
                    }
                },
                fail: (res) => {
                    wx.showToast({
                        title: res.errmsg,
                        icon: 'none'
                    })
                }
            });
        }
    },
    visit:function(res){
        let id = res.currentTarget.dataset.userid;
        myopus.open({
            id:id
        });
    },
    follow: function () {
        wx.showNavigationBarLoading();
        getApp().data.user.follow({
            id: this.data.artistID,
            follow: 1,
            success: (res) => {
                wx.showToast({
                    title: '关注成功',
                })
                this.setData({
                    followshow: false
                });
            },
            fail: (res) => {
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
            },
            complete: (res) => {
                wx.hideNavigationBarLoading();
            }
        });
    },
    tag:function(res){
        let keyword=res.target.dataset.keyword;
        search.open({
            keyword:keyword
        });
    },
    report:function(res){
        report.upload({
            fromc: 'opus',
            fromid: opus._opt.id,
            success: (res) => {
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
            },
            fail: (res) => {
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
            }
        });
    },
    del:function(res){
        wx.showModal({
            title: '提示',
            content: '真的要删除您的作品嘛～该操作不可恢复！',
            success:(res)=>{
                if(res.confirm){
                    opus.del({
                        success:(res)=>{
                            wx.showToast({
                                title: res.errmsg,
                            })
                            setTimeout(function(){
                                wx.navigateBack({})
                            }.bind(this),1300);
                        },
                        fail:(res)=>{
                            wx.showToast({
                                title: res.errmsg,
                                icon:'none'
                            })
                        }
                    });
                }
            }
        })
    }
})