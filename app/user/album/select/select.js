import format from '../../../lib/format.js';
import ajax from '../../../lib/ajax.js';
import config from '../../../config.js';

class Select {

    constructor() {
        this._opt = {};
        this._opusData = [];
    }

    /*
        opt={
            max:12
        }
    */
    open(opt) {
        opt = opt || {};
        format.callback(opt);
        this._opt = opt;
        this._opt.max = this._opt.max || 12;
        this._opusData = [];
        this._userid = 0;
        wx.navigateTo({
            url: '/app/user/album/select/view/page?max=' + opt.max,
        })

    }

    selected(list){
        wx.navigateBack({});
        let arr = [];
        if(list.length <= this._opt.max)
            arr = list;
        else{
            for (let i = 0; i < this._opt.max;i++){
                arr.push(list[i]);
            }
        }
        this._opt.success({
            code:200,
            list:arr
        });
    }

    setMax(max,id) {
        this._opt.max = max;
        this._userid = id;
    }

    getOpusList(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.page = opt.page || 1;
        ajax.post({
            url: config.pxpro + 'mini/user/getUserOpusList',
            data: {
                id: this._userid,
                page: opt.page
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    for (let i in res.list) {
                        let c = false;
                        for (let j in this._opusData) {
                            if (this._opusData[j].id == res.list[i].id) {
                                c = true;
                                break;
                            }
                        }
                        if (!c) {
                            this._opusData.push(res.list[i]);
                        }
                    }
                    opt._success(this._opusData);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

}

export default new Select();