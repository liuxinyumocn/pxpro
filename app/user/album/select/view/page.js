// app/home/select/view/page.js
import select from '../select.js';
import format from '../../../../lib/format.js';
import tool from '../../../../lib/tool.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        opusPageIndex: 0,
        loadOpusing: false,
        opusList: [],
        selectNum:0,
        selectMax:12,
        selectData:[],
        push:false
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        select.setMax(options.max, getApp().data.user._userinfo.account);
        this.nextPageOpus();
        this.setData({ selectMax: options.max })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        this.data.opusPageIndex = 0;
        select._opusData = []; 
        this.setData({ push: false, selectNum: 0 });
        this.data.selectData = [];
        this.nextPageOpus({
            complete: (res) => {
                wx.stopPullDownRefresh();
            }
        });
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.nextPageOpus();
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    nextPageOpus: function (opt) {
        opt = opt || {};
        format.callback(opt);
        if (this.data.loadOpusing)
            return;
        wx.showNavigationBarLoading();
        this.data.loadOpusing = true;
        select.getOpusList({
            page: this.data.opusPageIndex + 1,
            success: (res) => {
                wx.hideNavigationBarLoading();
                this.data.opusPageIndex++;
                this.loadOpus(res);
                opt._success(res);
            },
            fail: (res) => {
                wx.hideNavigationBarLoading();
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
                opt._fail(res);
            },
            complete: (res) => {
                this.data.loadOpusing = false;
            }
        });
    },
    loadOpus: function (res) {
        let arr = [];
        for (let i in res) {
            let item = res[i];
            let cr = tool.getCoyeRight(item.copyright);
            let cell = {
                avatar: item.avatar,
                id: item.id,
                date: tool.getDateText(item.timestamp),
                copyright: cr.title,
                copyrightColor: cr.color,
                private: item.private,
                likenum: tool.getNum(item.likenum)
            }
            arr.push(cell);
        }
        this.setData({
            opusList: arr
        });
    }, 
    change:function(res){
        let list = res.detail.value;
        this.setData({selectNum:list.length})
        if(list.length == select._opt.max){
            wx.showToast({
                title: '已达选择上限',
                icon:'none'
            })
        } else if (list.length > select._opt.max){
            wx.showToast({
                title: '超出选择将被自动忽略，请调节数量',
                icon: 'none'
            })
        }
        this.data.selectData = list;
    },
    push:function(){
        wx.showModal({
            title: '提示',
            content: '要清空当前所有选择项嘛？',
            success:(res)=>{
                if (res.confirm) {
                    this.setData({ push: false, selectNum: 0 });
                    this.data.selectData = [];
                }
            }
        })
    },
    confirm:function(){
        //为选择项添加缩略图
        let arr = [];
        for(let i in this.data.selectData){
            for(let j in this.data.opusList){
                if(this.data.opusList[j].id == this.data.selectData[i]){
                    arr.push({
                        id:this.data.selectData[i],
                        avatar:this.data.opusList[j].avatar
                    });
                    break;
                }
            }
        }
        select.selected(arr);
    }
})