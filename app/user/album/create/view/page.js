// app/user/album/create/view/page.js
import select from '../../select/select.js';
import create from '../create.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        list : [],
        value:'0',
        valueStr:'',
        valueData:0,
        content:''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        this.load();
    },
    load:function(){
        wx.showLoading({
            title: '请稍候',
        })
        create.getValue({
            success:(res)=>{
                this.setData({value:res.value})
            },
            complete:(res)=>{
                wx.hideLoading();
                if(res.code != 200){
                    wx.showToast({
                        title: res.errmsg,
                        icon:'none'
                    })
                }
            }
        });
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    add:function(){
        select.open({
            max:12-this.data.list.length,
            success:(res)=>{
                //去重
                let arr = [];
                for (let i in res.list) {
                    let c = false;
                    for(let j in this.data.list){
                        if(res.list[i].id == this.data.list[j].id){
                            c = true;
                            break;
                        }
                    }
                    if (!c)
                        arr.push(res.list[i]);
                }
                this.data.list.push(...arr);
                this.setData({list:this.data.list});
            }
        });
    },
    handle:function(res){
        let type = res.target.dataset.type;
        let id = res.target.dataset.id;
        let arr = [];
        if(type == 'del'){
            for(let i in this.data.list){
                if(this.data.list[i].id != id)
                    arr.push(this.data.list[i]);
            }
        }else{
            if(type == 'left'){
                for(let i = 0 ;i < this.data.list.length;i++){
                    if(this.data.list[i].id == id){
                        if(i-1 >= 0){
                            let a = this.data.list[i];
                            this.data.list[i] = this.data.list[i-1];
                            this.data.list[i-1] = a;
                        }
                        break;
                    }
                }
            }else if(type == 'right'){
                for (let i = 0; i < this.data.list.length; i++) {
                    if (this.data.list[i].id == id) {
                        if (i + 1 < this.data.list.length) {
                            let a = this.data.list[i];
                            this.data.list[i] = this.data.list[i + 1];
                            this.data.list[i + 1] = a;
                        }
                        break;
                    }
                }
            }
            arr = this.data.list;
        }
        this.setData({list:arr});
    },
    value:function(res){
        let value = res.detail.value;
        //只可以是1000的整数倍
        if (value < 1000 || parseInt(value / 1000) * 1000 != value) {
            wx.showToast({
                title: '曝光度必须为1000的正整数倍',
                icon:'none'
            })
            return;
        }

        if(value > this.data.value){
            wx.showToast({
                title: '您的剩余额度不足，请升级会员',
                icon:'none'
            })
            return;
        }
        this.data.valueData = value;
    },
    finished:function(){
        if (this.data.content.length == 0) {
            wx.showToast({
                title: '请对你的专辑进行一下简单的描述(60字)',
                icon: 'none'
            })
            return;
        }
        if(this.data.list.length < 3){
            wx.showToast({
                title: '专辑至少需要3个作品',
                icon:'none'
            })
            return;
        }
        if (this.data.valueData < 1000) {
            wx.showToast({
                title: '单次曝光度最小为1000',
                icon: 'none'
            })
            return;
        }
        //发布
        let arr= [];
        for(let i in this.data.list){
            arr.push(this.data.list[i].id);
        }
        wx.showLoading({
            title: '正在创建',
        })
        create.upload({
            list:arr,
            value:this.data.valueData,
            content:this.data.content,
            success:(res)=>{
                wx.hideLoading();
            },
            fail: (res) => {
                wx.hideLoading();
                wx.showToast({
                    title: res.errmsg,
                    icon:'none'
                })
            }
        });
    },
    inputContent:function(res){
        this.data.content = res.detail.value;
    }
})