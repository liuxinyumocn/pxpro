import format from '../../../lib/format.js';
import ajax from '../../../lib/ajax.js';
import config from '../../../config.js';
import task from '../../task.js';
class Create {

    constructor() {
        this._opt = {};
    }

    /*
        opt={
            max:12
        }
    */
    open(opt) {
        opt = opt || {};
        format.callback(opt);
        this._opt = opt;
        this._userid = 0;
        wx.navigateTo({
            url: '/app/user/album/create/view/page',
        })

    }

    setId( id) {
        this._userid = id;
    }

    getValue(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/album/getValue',
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    opt._success(res);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

    /*
            list:arr,
            value:this.data.valueData,
            content:this.data.content,
    */
    upload(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/album/upload',
            data:{
                list: JSON.stringify(opt.list),
                value:opt.value,
                content:opt.content
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    opt._success(res);
                    wx.navigateBack({
                        
                    })
                    this._opt._success();
                    task.checkAlbumCreate({
                        id:res.id,
                        success:(res)=>{
                            setTimeout(function(){
                                wx.showToast({
                                    title: '本周任务已完成！',
                                })
                            }.bind(this),1000);
                        }
                    });
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

}

export default new Create();