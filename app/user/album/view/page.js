// app/user/album/view/page.js
import album from '../album.js';
import format from '../../../lib/format.js';
import tool from '../../../lib/tool.js';
import create from '../create/create.js';
import opus from '../../opus/opus.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        opusPageIndex: 0,
        loadOpusing: false,
        opusList: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        album.setId(getApp().data.user._userinfo.account);
        this.nextPageOpus();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        this.data.opusPageIndex = 0;
        album._opusData = [];
        this.nextPageOpus({
            complete: (res) => {
                wx.stopPullDownRefresh();
            }
        });
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.nextPageOpus();
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    create:function(){
        create.open({
            success:(res)=>{
                this.onPullDownRefresh();
            }
        });
    },
    nextPageOpus: function (opt) {
        opt = opt || {};
        format.callback(opt);
        if (this.data.loadOpusing)
            return;
        wx.showNavigationBarLoading();
        this.data.loadOpusing = true;
        album.getOpusList({
            page: this.data.opusPageIndex + 1,
            success: (res) => {
                wx.hideNavigationBarLoading();
                this.data.opusPageIndex++;
                this.loadOpus(res);
                opt._success(res);
            },
            fail: (res) => {
                wx.hideNavigationBarLoading();
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
                opt._fail(res);
            },
            complete: (res) => {
                this.data.loadOpusing = false;
            }
        });
    },
    loadOpus: function (res) {
        let arr = [];
        for (let i in res) {
            let item = res[i];
            let cell = {
                content: item.content,
                id: item.id,
                watch: tool.getNum(item.expectwatch + item.owewatch) ,
                opus:[],
                expect:tool.getNum(item.expectwatch)
            }
            let arr2 = JSON.parse(item.opus);
            for(let j in arr2){
                cell.opus.push({
                    id:arr2[j],
                    avatar: album.getAvatar(arr2[j])
                });
            }
            arr.push(cell);
        }
        this.setData({
            opusList: arr
        });
    },
    openOpus: function (res) {
        let id = res.currentTarget.dataset.id;
        opus.open({ id: id });
    },
    delalbum: function (res) {
        let id = res.currentTarget.dataset.id;
        wx.showModal({
            title: '提示',
            content: '真的要删除该专辑嘛？未使用完的曝光度将不能被退还',
            success:(res)=>{
                if(res.confirm){
                    album.del({
                        id: id,
                        success: (res) => {
                            this.loadOpus(res);
                        },
                        fail: (res) => {
                            wx.showToast({
                                title: res.errmsg,
                                icon: 'none'
                            })
                        }
                    });
                }
            }
        })
    }
})