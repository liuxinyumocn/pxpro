import format from '../../lib/format.js';
import ajax from '../../lib/ajax.js';
import config from '../../config.js';

class Album {

    constructor() {
        this._opt = {};
        this._opusData = [];
        this._avatars = [];
    }

    /*
        opt={
            max:12
        }
    */
    open(opt) {
        opt = opt || {};
        format.callback(opt);
        this._opt = opt;
        this._opusData = [];
        this._avatars = [];

        getApp().data.user.wxlogin({
            login:true,
            success:(res)=>{
                wx.navigateTo({
                    url: '/app/user/album/view/page',
                })
            }
        })

    }

    setId(id) {
        this._userid = id;
    }

    del(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/album/del',
            data: {
                id: opt.id
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    let arr = [];
                    for(let i in this._opusData){
                        if(this._opusData[i].id == opt.id){
                            continue;
                        }
                        arr.push(this._opusData[i]);
                    }
                    this._opusData = arr;
                    opt._success(this._opusData);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

    getOpusList(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.page = opt.page || 1;
        ajax.post({
            url: config.pxpro + 'mini/album/getList',
            data: {
                page: opt.page
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    for (let i in res.list) {
                        let c = false;
                        for (let j in this._opusData) {
                            if (this._opusData[j].id == res.list[i].id) {
                                c = true;
                                break;
                            }
                        }
                        if (!c) {
                            this._opusData.push(res.list[i]);
                        }
                    }
                    this._avatars.push(...res.avatars);
                    opt._success(this._opusData);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

    getAvatar(id){
        for(let i in this._avatars){
            if(this._avatars[i].id == id)
                return this._avatars[i].avatar;
        }
    }

}

export default new Album();