// app/user/view/page.js
import locallworks from '../localworks/localworks.js';
import draw from '../../draw/draw.js';
import tool from '../../lib/tool.js';
import wallet from '../wallet/wallet.js';
import config from '../../config.js';
import vip from '../vip/vip.js';
import friend from '../friend/friend.js';
import mylike from '../mylikes/mylikes.js';
import album from '../album/album.js';
import order from '../order/order.js';
import setting from '../setting/setting.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        log:false,
        nickname:'',
        avatar:'',
        fans:0,
        follow:0,
        opus:0,
        liked:0,
        viptip:'免费获得VIP'
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        //渲染
        this.render();
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        getApp().data.user.wxlogin({
            login: true,
            success: (res) => {
                getApp().data.user.loadUserMain({
                    complete:(res)=>{
                        wx.stopPullDownRefresh();
                        this.render();
                    }
                });
            }
        })
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    localworks:function(){
        locallworks.open();
    },
    render:function(){
        let vipt = vip.getVIPData();
        let user = getApp().data.user;
        this.setData({
            log:user.islog(),
            nickname:user._userinfo.data.nickname,
            avatar:user._userinfo.data.avatar,
            fans: tool.getNum(user._userinfo.main.fans),
            follow: tool.getNum(user._userinfo.main.follow),
            liked: tool.getNum(user._userinfo.main.liked),
            opus: tool.getNum(user._userinfo.main.opus),
            viptip: vipt.vip == 0 ? '免费获得VIP' : 'VIP' + vipt.expdes[0]
        });
    },
    login:function(){
        let user = getApp().data.user;
        user.wxlogin({
            success:(res)=>{
                if(res.code == 201)
                    user.login({
                        success: (res) => {
                            this.render();
                        }
                    });
                else
                    this.render();
            },
            fail:(res)=>{
                wx.showToast({
                    title: res.errmsg,
                    icon:'none'
                })
            }
        });
    },
    create:function(){
        getApp().data.user.wxlogin({
            login:true,
            success:(res)=>{
                draw.launch({
                    file: ''
                });
            }
        });
    },
    myopus:function(){  //打开我的作品列表
        getApp().data.user.openMyOpus();
    },
    wallet:function(){
        getApp().data.user.wxlogin({
            success:(res)=>{
                if(res.code != 200){
                    wx.showToast({
                        title: '请先登录后再使用钱包噢',
                        icon:'none'
                    })
                    return ;
                }
                wallet.open();
            },
            fail:(res)=>{
                wx.showToast({
                    title: res.errmsg,
                    icon:'none'
                })
            }
        });
    },
    openvip:function(){
        vip.open();
    },
    myfans:function(){
        friend.open({
            type:0
        });
    },
    myfollow: function () {
        friend.open({
            type: 1
        });
    },
    myvisitor: function () {
        friend.open({
            type: 2
        });
    }, mylikes:function(){
        mylike.open({
            id: getApp().data.user._userinfo.account
        });
    }, album:function(){
        album.open({
        });
    }, order:function(){
        order.open({});
    }, setting:function(){
        setting.open();
    }
})