// app/user/setting/view/page.js
import setting from '../setting.js';
import login from '../../login/login.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        notify:0,
        stranger:0,
        fastlogin:0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        this.loadSetting();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    loadSetting:function(){
        wx.showLoading({
            title: '请稍后',
        })
        setting.loadSetting({
            success:(res)=>{
                wx.hideLoading();
                this.setData({
                    stranger: res.stranger === undefined ? this.data.stranger : res.stranger,
                    notify: res.notify === undefined ? this.data.notify : res.notify,
                    fastlogin: res.fastlogin === undefined ? this.data.fastlogin : res.fastlogin 
                })
            },
            fail: (res) => {
                wx.hideLoading();
                wx.showToast({
                    title: res.errmsg,
                    icon:'none'
                })
                setTimeout(function(){
                    wx.navigateBack({})
                }.bind(this),1500);
            }
        });
    },
    switchChange:function(res){
        let key = res.currentTarget.dataset.key;
        let value = res.detail.value;
        wx.showLoading({
            title: '请稍后',
        })
        setting.setSetting({
            key:key,
            value:value,
            success: (res) => {
                wx.hideLoading();
                this.setData({
                    stranger: res.stranger === undefined ? this.data.stranger : res.stranger,
                    notify: res.notify === undefined ? this.data.notify : res.notify,
                    fastlogin: res.fastlogin === undefined ? this.data.fastlogin : res.fastlogin
                })
            },
            fail: (res) => {
                wx.hideLoading();
                wx.showToast({
                    title: res.errmsg,
                    icon:'none'
                })
                let json = '{"' + key + '":' + ((!value) ? 1 : 0) + '}';
                this.setData(JSON.parse(json));
            }
        });
    },
    updateUserInfo:function(){
        login.open({
           updateInfo:true,
           success:(res)=>{
               wx.showToast({
                   title: '更新完成',
               })
           },
           fail:(res)=>{
               wx.showToast({
                   title: res.errmsg,
                   icon:'none'
               })
           }
        });
    },
    logout:function(res){
        if(this.data.fastlogin == 1){
            wx.showModal({
                title: '提示',
                content: '退出登录将导致关闭自动登录，下次访问时需要重新使用手机号码登录，是否继续退出？',
                success:(res)=>{
                    if(res.confirm){
                        setting.setSetting({
                            key:'fastlogin',
                            value:false,
                            success:(res)=>{
                                this._logout();
                            },
                            fail:(res)=>{
                                wx.showToast({
                                    title: res.errmsg,
                                    icon:'none'
                                })
                            }
                        });
                    }
                }
            })
            return;
        }
        wx.showModal({
            title: '提示',
            content: '真的要立即退出当前登录的账号嘛',
            success:(res)=>{
                if(res.confirm){
                    this._logout();
                }
            }
        })
    },
    _logout:function(){
        getApp().logout();
        wx.showToast({
            title: '退出成功！',
        })
        setTimeout(function () {
            wx.navigateBack({})
        }.bind(this), 1500);
    },
    bindtel:function(){
        setting.isbindedtel({
            success:(res)=>{
                if(res.bind == 1){
                    wx.showToast({
                        title: '您已经绑定手机号啦，无需绑定',
                        icon:'none'
                    })
                    return;
                }else{
                    //去绑定页面
                    login.open({
                        bindtel: true,
                        success: (res) => {
                            wx.showToast({
                                title: '绑定成功',
                            })
                        },
                        fail: (res) => {
                            wx.showToast({
                                title: res.errmsg,
                                icon: 'none'
                            })
                        }
                    });
                }
            },
            fail:(res)=>{
                wx.showToast({
                    title: res.errmsg,
                    icon:'none'
                })
            }
        })

    }
})