/*
    每日任务系统
*/
import config from '../config.js';
import format from '../lib/format.js';
import ajax from '../lib/ajax.js';
import md5 from '../lib/md5.js';
import tool from '../lib/tool.js';

class Task{

    constructor(){

    }

    /*
        首次分享朋友圈
    */
    checkSharePoster(opt) {
        opt = opt || {};
        format.callback(opt);
        let user = getApp().data.user;
        if (!user.islog()) {
            opt._fail({
                code: 400,
                errmsg: '尚未登陆'
            });
            return;
        }
        wx.getStorage({
            key: 'hadgenshare_'+user.getID(),
            fail:  (res) => {
                let ns = tool.getNonceStr();
                ajax.post({
                    url: config.pxpro + 'mini/task/sharePoster',
                    data: {
                        nonceStr: ns,
                        sign: md5(ns + user.getID()+ config.viptoken)
                    },
                    success: (res) => {
                        res = res.data;
                        if (res.code == 200) {
                            //本地缓存以后不再请求
                            wx.setStorage({
                                key: 'hadgenshare_' + user.getID(),
                                value: 1
                            });
                            if(res.had == 0){
                                opt._success();
                            }
                        }
                    }
                });
            }
        })    
    }

    /*
       检查每周1个专辑的任务
       opt.id = 专辑ID
   */
    checkAlbumCreate(opt) {

        opt = opt || {};
        format.callback(opt);
        let user = getApp().data.user;
        if (!user.islog()) {
            opt._fail({
                code: 400,
                errmsg: '尚未登陆'
            });
            return;
        }
        wx.getStorage({
            key: 'albumCreatGift_' + user.getID(),
            success: (res)=> {
                let now = new Date().getTime() / 1000;
                if (res.data.timestamp < now) {
                    opt.userid = user.getID();
                    this.applyAlbumCreate(opt);
                } else {
                    opt._fail({
                        code: 400,
                        errmsg: '今日已经完成领取'
                    });
                }
            },
            fail: (res) => {
                //申请领取奖励
                opt.userid = user.getID();
                this.applyAlbumCreate(opt);
            }
        })
    }


    //申请领取奖励
    applyAlbumCreate(opt) {
        ajax.post({
            url: config.pxpro + 'mini/task/applyAlbumCreate',
            data: {
                id:opt.id
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200 || res.code == 201) {
                    //完成领取 或 已经领取
                    wx.setStorage({
                        key: 'albumCreatGift_' + opt.userid,
                        data: {
                            timestamp: res.timestamp
                        },
                    })
                    if (res.code == 200) {
                        opt._success({
                            code: 200
                        })
                    } else {
                        opt._fail({
                            code: 400,
                            errmsg: '已经领取过啦'
                        })
                    }
                }
                else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({
                    code: 400,
                    errmsg: '网络异常'
                })
            }
        });
    }

    /*
        检查每日的首页查看5个专辑的任务
    */
    checkAlbum(opt){

        opt = opt || {};
        format.callback(opt);
        let user = getApp().data.user;
        if (!user.islog()) {
            opt._fail({
                code: 400,
                errmsg: '尚未登陆'
            });
            return;
        }
        wx.getStorage({
            key: 'albumGift_' + user.getID(),
            success: (res)=> {
                let today = new Date(new Date().setHours(0, 0, 0, 0)) / 1000;
                if (res.data.timestamp < today) {
                    opt.userid = user.getID();
                    this.applyAlbum(opt);
                } else {
                    opt._fail({
                        code: 400,
                        errmsg: '今日已经完成领取'
                    });
                }
            },
            fail: (res) => {
                //申请领取奖励
                opt.userid = user.getID();
                this.applyAlbum(opt);
            }
        })
    }


    //申请领取奖励
    applyAlbum(opt) {
        let ns = tool.getNonceStr();
        ajax.post({
            url: config.pxpro + 'mini/task/applyAlbum',
            data: {
                nonceStr: ns,
                sign: md5(ns + opt.userid + config.viptoken)
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200 || res.code == 201) {
                    //完成领取 或 已经领取
                    wx.setStorage({
                        key: 'albumGift_' + opt.userid,
                        data: {
                            timestamp: res.timestamp
                        },
                    })
                    if (res.code == 200) {
                        opt._success({
                            code: 200
                        })
                    } else {
                        opt._fail({
                            code: 400,
                            errmsg: '已经领取过啦'
                        })
                    }
                }
                else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({
                    code: 400,
                    errmsg: '网络异常'
                })
            }
        });
    }

    /*
        检查每日的评论任务
        opt = {
            commentid:'' //需要提供准确的评论ID
        }

        先检查本地记录情况

    */
    checkComment(opt) {
        opt = opt || {};
        format.callback(opt);
        let user = getApp().data.user;
        if (!user.islog()){
            opt._fail({
                code:400,
                errmsg:'尚未登陆'
            });
            return;
        }
        wx.getStorage({
            key: 'commentGift_'+user.getID(),
            success: (res)=>{
                let today = new Date(new Date().setHours(0, 0, 0, 0)) / 1000;
                if(res.data.timestamp < today){
                    opt.userid = user.getID();
                    this.applyComment(opt);
                }else{
                    opt._fail({
                        code:400,
                        errmsg:'今日已经完成领取'
                    });
                }
            },
            fail:(res)=>{
                //申请领取奖励
                opt.userid = user.getID();
                this.applyComment(opt);
            }
        })

    }

    //申请领取奖励
    applyComment(opt){
        ajax.post({
            url: config.pxpro + 'mini/task/applyComment',
            data:{
                id:opt.commentid
            },
            success:(res)=>{
                res = res.data;
                if(res.code == 200 || res.code == 201){
                    //完成领取 或 已经领取
                    wx.setStorage({
                        key: 'commentGift_' + opt.userid,
                        data: {
                            timestamp:res.timestamp
                        },
                    })
                    if(res.code == 200){
                        opt._success({
                            code:200
                        })
                    }else{
                        opt._fail({
                            code:400,
                            errmsg:'已经领取过啦'
                        })
                    }
                }
                else{
                    opt._fail(res);
                }
            },
            fail:(res)=>{
                opt._fail({
                    code:400,
                    errmsg:'网络异常'
                })
            }
        });
    }

}

export default new Task();