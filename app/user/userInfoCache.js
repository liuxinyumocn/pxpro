/*
    用户数据缓存 仅负责获得用户的头像、昵称、性别等信息
*/
import format from '../lib/format.js';
import ajax from '../lib/ajax.js';
import config from '../config.js';

class UserInfoCache{
    
    constructor(){
        this._cache = [];
    
        this._queueCallback = [];
    }

    /*
        opt={id}
    */
    getUserInfo(opt) {
        opt = opt || {};
        format.callback(opt);
        //先查本地
        for(let i in this._cache){
            if(this._cache[i].id == opt.id){
                opt._success(this._cache[i]);
                return ;
            }
        }
        //检查是否存在正在执行的网络请求数据
        for(let i in this._queueCallback){
            if(this._queueCallback[i].id == opt.id){
                this._queueCallback[i].callback.push(opt);
                return;
            }
        }

        this._queueCallback.push({
            id:opt.id,
            callback:[opt]
        });
        this.request(opt,true);
    }

    request(opt,retry = false){

        //网络请求
        ajax.post({
            url: config.pxpro + 'mini/user/getUserInfo',
            data: {
                id: opt.id,
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    this._cache.push(res.data);
                    for(let i = 0;i<this._queueCallback.length;i++){
                        if(this._queueCallback[i].id == opt.id){
                            for(let j in this._queueCallback[i].callback){
                                this._queueCallback[i].callback[j]._success(res.data);
                            }
                            for(let m = i+1;m < this._queueCallback.length;m++){
                                this._queueCallback[m-1] = this._queueCallback[m];
                            }
                            this._queueCallback.pop();
                            return;
                        }
                    }
                } else {
                    if (retry)
                        this.request(opt);
                    else
                        opt._fail(res);
                }
            },
            fail: (res) => {
                if (retry)
                    this.request(opt);
                else
                    opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

}

export default new UserInfoCache();