import format from '../../lib/format.js';
import ajax from '../../lib/ajax.js';
import config from '../../config.js';

class Friend {

    constructor() {
        this._opt = {
            type:0,          // 0 粉丝 、1关注、2访客
        };
        this._opusData = [];
    }

    /*
        opt={
            id
        }
    */
    open(opt) {
        opt = opt || {};
        format.callback(opt);
        this._opt = opt;
        this._opusData = [];
        getApp().data.user.wxlogin({
            login: true,
            success: (res) => {
                wx.navigateTo({
                    url: '/app/user/friend/view/page?id=' + opt.id + '&type=' + opt.type,
                })
            }
        })

    }

    setId(type) {
        this._opt.type = type;
    }

    getType(){
        switch (this._opt.type) {
            case '0':
                return {
                    title: '粉丝',
                    url: config.pxpro + 'mini/follow/getUserFansList'
                }
            case '1':
                return {
                    title: '关注',
                    url: config.pxpro + 'mini/follow/getUserFollowList'
                }
            case '2':
                return {
                    title: '访客',
                    url: config.pxpro + 'mini/user/getUserVisitorList'
                }
        }
    }

    getList(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.page = opt.page || 1;
        ajax.post({
            url: this.getType().url,
            data: {
                page: opt.page
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    for (let i in res.list) {
                        let c = false;
                        for (let j in this._opusData) {
                            if (this._opusData[j].id == res.list[i].id) {
                                c = true;
                                break;
                            }
                        }
                        if (!c) {
                            this._opusData.push(res.list[i]);
                        }
                    }
                    opt._success(this._opusData);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

}

export default new Friend();