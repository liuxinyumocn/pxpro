// app/user/friend/view/page.js
import friend from '../friend.js';
import format from '../../../lib/format.js';
import userInfoCache from '../../userInfoCache.js';
import tool from '../../../lib/tool.js';
import myopus from '../../myopus/myopus.js';
import vip from '../../vip/vip.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        opusPageIndex: 0,
        loadOpusing: false,
        opusList: [],
        type:0,
        vip:{}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        friend.setId(options.type);
        this.setData({ type: options.type, vip: vip.getVIPData()});
        this.nextPageOpus();
        wx.setNavigationBarTitle({
            title: friend.getType().title,
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.nextPageOpus();
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    nextPageOpus: function (opt) {
        opt = opt || {};
        format.callback(opt);
        if (this.data.loadOpusing)
            return;
        wx.showNavigationBarLoading();
        this.data.loadOpusing = true;
        friend.getList({
            page: this.data.opusPageIndex + 1,
            success: (res) => {
                wx.hideNavigationBarLoading();
                this.data.opusPageIndex++;
                this.loadOpus(res);
                opt._success(res);
            },
            fail: (res) => {
                wx.hideNavigationBarLoading();
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
                opt._fail(res);
            },
            complete: (res) => {
                this.data.loadOpusing = false;
            }
        });
    },
    loadOpus: function (res) {
        let arr = [];
        let finished = 0;
        for (let i =0 ;i<res.length;i++) {
            let item = res[i];
            let cell = {
                avatar: '',
                nickname: '',
                id:item.id,
                userid:item.user,
                date: item.timestamp == null ? '' : tool.getDateText(item.timestamp),
                follow: item.follow == null ? -1 : item.follow,
                followed: item.followed == null ? -1 : item.followed
            }
            arr.push(cell);
            userInfoCache.getUserInfo({
                id:item.user,
                success:(res2)=>{
                    arr[i].avatar = res2.avatar;
                    arr[i].nickname = res2.nickname;
                    arr[i].sex = res2.gender;
                    finished++;
                    if(finished == res.length){
                        this.setData({
                            opusList:arr
                        })
                        return;
                    }
                }
            });
        }
    },
    follow: function (res) {
        wx.showNavigationBarLoading();
        getApp().data.user.follow({
            id: res.currentTarget.dataset.user,
            follow: res.currentTarget.dataset.v,
            success: (res2) => {
                for(let i in this.data.opusList){
                    if (this.data.opusList[i].userid == res.currentTarget.dataset.user){
                        this.data.opusList[i].follow = res2.follow ;
                    }
                }
                this.setData({
                    opusList:this.data.opusList
                });
            },
            fail: (res) => {
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
            },
            complete: (res) => {
                wx.hideNavigationBarLoading();
            }
        });
    },
    openOpusHome:function(res){
        myopus.open({ id: res.currentTarget.dataset.user})
    }
})