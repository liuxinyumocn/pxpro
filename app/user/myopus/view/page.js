// app/user/myopus/view/page.js
import myopus from '../myopus.js';
import format from '../../../lib/format.js';
import tool from '../../../lib/tool.js';
import friend from '../../friend/friend.js';
import opus from '../../opus/opus.js';
import chat from '../../../message/chat/chat.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        avatar:'',
        nickname:'加载中',
        sex:0,
        userid:'',
        opusnum:'-',
        likenum:'-',
        follownum:'-',
        fansnum:'-',
        followed:false,
        opusList:[],
        self:true,
        opusPageIndex:0,
        loadOpusing:false,
        followed:false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        myopus.setId(options.id);
        this.setData({
            userid : options.id
        })
        wx.showNavigationBarLoading();
        this.reflash({
            complete:(res)=>{
                wx.hideNavigationBarLoading();
            }
        });
        this.nextPageOpus();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        this.reflash({
            complete:(res)=>{
                wx.stopPullDownRefresh();
            }
        });
        this.data.opusPageIndex = 0;
        myopus._opusData = [];
        this.nextPageOpus();
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.nextPageOpus();
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    reflash: function (opt) {
        opt = opt || {};
        format.callback(opt);
        myopus.loadUserInfo({
            success:(res)=>{
                this.setData({
                    avatar:res.info.avatar,
                    nickname:res.info.nickname,
                    sex:res.info.gender,
                    opusnum: tool.getNum(res.data.opus),
                    likenum: tool.getNum(res.data.liked),
                    fansnum: tool.getNum(res.data.fans),
                    follownum: tool.getNum(res.data.follow),
                    self:res.self,
                    followed: res.follow
                });
                opt._success();
            },
            fail:(res)=>{
                opt._fail(res);
                wx.showToast({
                    title: res.errmsg,
                    icon:'none'
                })
            }
        });
    },
    nextPageOpus:function(){
        if(this.data.loadOpusing)
            return;
        wx.showNavigationBarLoading();
        this.data.loadOpusing = true;
        myopus.getOpusList({
            page: this.data.opusPageIndex + 1,
            success: (res) => {
                wx.hideNavigationBarLoading();
                this.data.opusPageIndex++;
                this.loadOpus(res);
            },
            fail: (res) => {
                wx.hideNavigationBarLoading();
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
            },
            complete: (res) => {
                this.data.loadOpusing = false;
            }
        });
    },
    loadOpus:function(res){
        let arr = [];
        for(let i in res){
            let item = res[i];
            let cr = tool.getCoyeRight(item.copyright);
            let cell = {
                avatar:item.avatar,
                id:item.id,
                date: tool.getDateText(item.timestamp),
                copyright: cr.title,
                copyrightColor:cr.color,
                private: item.private,
                likenum: tool.getNum(item.likenum)
            }
            arr.push(cell);
        }
        this.setData({
            opusList:arr
        });
    },
    openOpus:function(res){
        let id = res.currentTarget.dataset.id;
        opus.open({id:id});
    },
    follow:function(res){
        wx.showNavigationBarLoading();
        getApp().data.user.follow({
            id: this.data.userid,
            follow: res.currentTarget.dataset.v,
            success:(res)=>{
                let n = res.follow == 1 ? 1 : -1;
                this.setData({
                    followed : res.follow,
                    fansnum: tool.getNum(this.data.fansnum+n)
                });
            },
            fail:(res)=>{
                wx.showToast({
                    title: res.errmsg,
                    icon:'none'
                })
            },
            complete:(res)=>{
                wx.hideNavigationBarLoading();
            }
        });
    },
    avatar:function(){
        wx.previewImage({
            urls: [this.data.avatar],
        })
    },
    mail:function(res){
        let userid = res.currentTarget.dataset.userid;
        chat.open({
            userid : userid
        });
    }
})