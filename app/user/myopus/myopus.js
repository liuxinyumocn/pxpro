import format from '../../lib/format.js';
import ajax from '../../lib/ajax.js';
import config from '../../config.js';

class MyOpus {

    constructor() {
        this._opt = null;
        this._opusData = [];
    }

    /*
        opt={
            id
        }
    */
    open(opt) {
        opt = opt || {};
        format.callback(opt);
        this._opt = opt;
        this._opusData = [];
        getApp().data.user.wxlogin({
            login: true,
            success: (res) => {
                wx.navigateTo({
                    url: '/app/user/myopus/view/page?id='+opt.id,
                })
            }
        })

    }

    setId(id){
        this._opt.id = id;
    }

    loadUserInfo(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/user/getUserPublicInfo',
            data: { id: this._opt.id},
            success:(res)=>{
                res = res.data;
                if(res.code == 200){
                    opt._success(res);
                }else{
                    opt._fail(res);
                }
            },
            fail:(res)=>{
                opt._fail({code:400,errmsg:'网络异常'})
            }
        })
    }

    getOpusList(opt) {
        opt = opt || {};
        format.callback(opt);
        opt.page = opt.page || 1;
        ajax.post({
            url: config.pxpro + 'mini/user/getUserOpusList',
            data: {
                id: this._opt.id,
                page: opt.page
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    for (let i in res.list) {
                        let c = false;
                        for (let j in this._opusData) {
                            if (this._opusData[j].id == res.list[i].id) {
                                c = true;
                                break;
                            }
                        }
                        if (!c) {
                            this._opusData.push(res.list[i]);
                        }
                    }
                    opt._success(this._opusData);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({ code: 400, errmsg: '网络异常' })
            }
        });
    }

}

export default new MyOpus();