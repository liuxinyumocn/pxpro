// ssu/user/wallet/view/page.js
import wallet from '../wallet.js';
import recharge from '../recharge/recharge.js';
import browser from '../../../../domoe/browser/browser.js';

Page({

    /**
     * 页面的初始数据
     */
    data: {
        balance:0,
        balanceStr:"0.00",
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        this.reflashBalance();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        wx.showNavigationBarLoading();
        wallet.getBalance({
            force:1,
            success:(res)=>{
                this.reflashBalance();
            },
            fail:(res)=>{
                console.log(res);
                wx.showToast({
                    title: res.errmsg,
                    icon:"none"
                })
            },
            complete:(res)=>{
                wx.hideNavigationBarLoading();
                wx.stopPullDownRefresh();
            }
        })
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    reflashBalance:function(){  //获取余额
        let walletinfo = wallet.getInfo();
        let fee = walletinfo;
        let sfee = fee + "";
        //元位
        let y = "0";
        if (fee > 99)
            y = parseInt(fee / 100) + "";
        //从个位起每3位增加1个逗号
        let length = y.length;
        let buff = "";
        for(let i=length;i>0;i--){
            if((length-i)%3==0 && i!=length){
                buff = ","+buff;
            }
            buff = y.substr(i-1,1)+buff;
        }
        y=buff;
        //角位
        let j = "0";
        if (fee > 9) {
            j = sfee.substr(-2, 1);
        }
        //分位
        let f = "0";
        if (fee > 0) {
            f = sfee.substr(-1, 1);
        }
        let balance =  y + "." + j + f;
        this.setData({
            balanceStr:balance
        });

        if(!wallet.tipSetKey){
            wx.showModal({
                title: '提示',
                content: '您还没有设置支付密码，将无法使用余额付款，是否立即设置？',
                success:(res)=>{
                    if(res.confirm){
                        this.setPaykey();
                    }
                    wallet.tipSetKey = true;
                }
            })
        }
    },
    recharge:function(){
        recharge.open({
            success:(res)=>{
                
            }
        });
    },
    withdraw:function(){
        wx.navigateTo({
            url: '/app/user/wallet/withdraw/view/page?balance='+this.data.balanceStr,
        })
    },
    setPaykey:function(){
        wx.showLoading({
            title: '请稍后',
        })
        wallet.applySetKey({
            success:(res)=>{
                wx.hideLoading();
                browser.open(res.url);
            },
            fail:(res)=>{
                wx.showToast({
                    title: res.errmsg,
                    icon:"none"
                })
            }
        });
    },
    info:function(){
        wallet.dealHistory({
            success:(res)=>{
                console.log(res);
                browser.open(res.url);
            },
            fail: (res) => {
                wx.showToast({
                    title: res.errmsg,
                    icon: "none"
                })
            }
        });
    }
})