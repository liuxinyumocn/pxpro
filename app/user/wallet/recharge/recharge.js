/*
    充值插件
*/
import config from '../../../config.js';
import format from '../../../lib/format.js';
import ajax from '../../../lib/ajax.js';

class Recharge {
    constructor() {
    }
    
    open(){
        wx.navigateTo({
            url: '/app/user/wallet/recharge/view/page',
        })
    }

    applyRecharge(opt){
        opt = opt || {};
        format.callback(opt);
        let cash = parseInt(opt.fee * 100);
        ajax.get({
            url: config.pxpro + "mini/wallet/applyRecharge",
            data:{
                fee:cash
            },
            success:(res)=>{
                opt._success(res.data);
                
            },fail:(res)=>{
                opt._fail({
                    code:400,
                    errmsg:"网络异常"
                });
            }
        });
    }
}

export default new Recharge();