// ssu/user/wallet/recharge/view/page.js
import config from '../../../../config.js';
import recharge from '../recharge.js';
import browser from '../../../../../domoe/browser/browser.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        fee:""
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    wechatpay:function(){
        setTimeout(function(){
            //申请充值请求
            let fee = this.data.fee;
            if (fee == "" || fee <= 0 || fee > 10000) {
                wx.showToast({
                    title: '不合法的充值金额',
                    icon: "none"
                })
                return;
            }
            wx.showLoading({
                title: '请稍后',
            })
            recharge.applyRecharge({
                fee: fee,
                success: (res) => {
                    wx.hideLoading();
                    if (res.code == 200) {
                        browser.open(config.pay + "pay/Cashier/deal/query/" + res.dealid);
                        this.setData({
                            fee: ""
                        });
                    } else {
                        wx.showToast({
                            title: res.errmsg,
                            icon: "none"
                        })
                    }
                },
                fail: (res) => {
                    wx.showToast({
                        title: res.errmsg,
                        icon: "none"
                    })
                }
            });
        }.bind(this),100);
    },
    input: function (res) {
        let fee = res.detail.value;
        if(fee == "")
            fee = 0;
        let cash = parseInt(fee * 100)/100;
        if(cash > 10000)
            cash = 10000;
        if(cash < 0)
            cash = 0;
        this.setData({
            fee :cash
        });
    }
})