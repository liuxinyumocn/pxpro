/*
    钱包插件
*/
import format from '../../lib/format.js';
import ajax from '../../lib/ajax.js';
import config from '../../config.js';

class Wallet{
    constructor(){
        this.init();
    }

    /*
        用户初始化
        res = {
            login:true/false,
            balance:123,
            userID:1
        }
    */
    init(res){
        this.tipSetKey = false;
        this._login = false;
        this._balance = 0;
        this._userID = -1;
        this._isset = 0;    //是否设置支付密码
        // if(res.login){
        //     this._login = true;
        //     this._balance = res.balance;
        //     this._userID = res.userID;
        // }
    }
    /*
        获取钱包信息
    */
    getInfo(){
        return this._balance;
    }

    /*
        打开钱包
    */
    open(opt){
        opt = opt || {};
        format.callback(opt);
        this._open2(opt);
    }

    _open2(opt){
        //this.open(opt);
        //获取余额
        this.getBalance({
            force : 0,
            success:(res)=>{
                wx.navigateTo({
                    url: '/app/user/wallet/view/page',
                })
            },
            fail:(res)=>{
                opt._fail(res);
            }
        });
    }

    applySetKey(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + "mini/wallet/getSetkey",
            success:(res)=>{
                console.log(res);
                if(res.data.code == 200){
                    let url = config.pay + "pay/setPayKey?token=" + res.data.token;
                    opt._success({
                        code:200,
                        url:url
                    });
                }else{
                    opt._fail(res.data);
                }
            },
            fail:(res)=>{
                opt._fail({
                    code:400,
                    errmsg:"网络异常"
                });
            }
        });
    }

    getBalance(opt){
        opt = opt || {};
        format.callback(opt);
        ajax.get({
            url: config.pxpro + "mini/wallet/balance",
            data: {
                force: opt.force,
                random: ajax.random()
            },
            success: (res) => {
                if(res.data.code == 200){
                    this._balance = res.data.balance;
                    this._isset =  res.data.isset;
                    if(this._isset == 1)
                        this.tipSetKey = true;
                    opt._success(res.data);
                }else{
                    opt._fail(res);
                }
            },
            fail:(res)=>{
                opt._fail({
                    code : 400,
                    errmsg : "网络异常"
                });
            }
        });
    }

    dealHistory(opt){
        opt = opt || {};
        format.callback(opt);
        ajax.get({
            url: config.pxpro + "mini/wallet/pid",
            success:(res)=>{
                if(res.data.code == 200){
                    opt._success({
                        code:200,
                        url: config.pay + "pay/user/history/userid/" + res.data.pid
                    });
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({
                    code: 400,
                    errmsg: "网络异常"
                });
            }
        });
    }
}

export default new Wallet();