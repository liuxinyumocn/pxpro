// ssu/user/wallet/withdraw/view/page.js
import withdraw from '../withdraw.js';
import doc from '../../../../lib/doc.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        balance:"",
        withdraw:0,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        this.setData({
            balance: options.balance
        });
    },
    doc:function(){
        doc.open('u3');
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    input:function(res){
        let value = res.detail.value;
        let fee = parseInt(value * 100);
        if(fee < 100)
            fee = 100;
        if(fee > 1000000)
            fee = 1000000;
            fee /= 100;
        this.setData({
            withdraw:fee
        });
    },
    wechatpay:function(){
        setTimeout(function(){
            if(this.data.withdraw < 1 || this.data.withdraw > 10000){
                wx.showToast({
                    title: '单笔提现最低1元 最高1万元',
                    icon: 'none'
                })
                return ;
            }

            wx.showModal({
                title: '提示',
                content: '为保障财产安全，' + this.data.withdraw+'元将被提现至最初用于实名认证微信账号中，是否继续？',
                success:(res)=>{
                    if(res.confirm){
                        wx.showLoading({
                            title: '请稍后',
                        })
                        withdraw.apply({
                            fee:parseInt(this.data.withdraw * 100),
                            success:(res)=>{
                                wx.showToast({
                                    title: '提现成功！',
                                    icon:'none'
                                })
                            },
                            fail:(res)=>{
                                wx.showToast({
                                    title: res.errmsg,
                                    icon:'none'
                                })
                            }
                        });
                    }
                }
            })
        }.bind(this),300);
    }
})