/*
    提现插件
*/
import config from '../../../config.js';
import format from '../../../lib/format.js';
import ajax from '../../../lib/ajax.js';

class Recharge {
    constructor() {
    }

    /*
        opt={
            fee
        }
    */
    apply(opt){
        opt = opt || {};
        format.callback(opt);
        ajax.get({
            url : config.pxpro + "mini/wallet/applyWithdraw",
            data:{
                fee:opt.fee
            },
            success:(res)=>{
                if(res.data.code == 200){
                    opt._success(res.data.code);
                }else{
                    opt._fail(res.data);
                }
            },
            fail:(res)=>{
                opt._fail({
                    code:400,
                    errmsg:"网络异常"
                });
            }
        })
    }
}

export default new Recharge();