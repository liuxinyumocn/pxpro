/*
    举报系统
*/
import config from '../config.js';
import format from '../lib/format.js';
import ajax from '../lib/ajax.js';

class Report {

    constructor() {

    }

    /*
        上传举报信息
        fromc:'album',
        fromid
    */
    upload(opt){
        opt = opt || {};
        format.callback(opt);

        wx.showModal({
            title: '提示',
            content: '维护良好网络环境我们坚定不移！如果该用户发布内容涉嫌违反国家法律、骚扰用户、发布与创意无关的私人广告，可进行举报，被核实违规的用户根据严重程度处以封禁手机号、封禁所登录时使用的微信设备等处罚。是否立即提交举报内容？',
            success: (res) => {
                let arr = [
                    '色情低俗、违法犯罪',
                    '与创意无关的纯粹广告',
                    '有害未成年成长的内容',
                    '政治敏感',
                    '侮辱谩骂',
                    '侵权'
                ];
                if (res.confirm) {
                    wx.showActionSheet({
                        itemList: arr,
                        success: (res2) => {
                            getApp().data.user.wxlogin({
                                login: true,
                                success: (res) => {
                                    wx.showLoading({
                                        title: '正在提交',
                                    })
                                    ajax.post({
                                        url: config.pxpro + 'mini/report',
                                        data: {
                                            content: arr[res2.tapIndex],
                                            fromc: opt.fromc,
                                            fromid: opt.fromid
                                        },
                                        success: (res) => {
                                            wx.hideLoading();
                                            res = res.data;
                                            if (res.code == 200) {
                                                opt._success(res);
                                            } else {
                                                opt._fail(res);
                                            }
                                        },
                                        fail: (res) => {
                                            wx.hideLoading();
                                            opt._fail({ code: 400, errmsg: '网络异常' })
                                        }
                                    });
                                },
                                fail: (res) => {
                                    opt._fail({
                                        code: 400,
                                        errmsg: '您需要登录后才可以举报'
                                    });
                                }
                            })
                        }
                    })
                }
            }
        })
    }
}

export default new Report();