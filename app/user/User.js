import format from '../lib/format.js';
import ajax from '../lib/ajax.js';
import config from '../config.js';
import login from 'login/login.js';
import myOpus from 'myopus/myopus.js';
import vip from 'vip/vip.js';
import message from '../message/message.js';
/*
    用户类
*/

export default class User{

    constructor(){

        this._wxlogin = false;
        this._openID = '';
        this._userinfo = {
            account : null,
            data:{
                nickname:'加载中',
            },
            main:{
                follow: 0,
                liked: 0,
                opus: 0,
                fans: 0
            }
        }
        ajax.clear();   //清除所有登录记录
        this.wxlogin();
    }

    setInviteUser(){
        wx.getStorage({
            key: 'inviteUser',
            success: function(res) {},
        })
    }

    getUserPublicData(){
        return this._userinfo.data;
    }

    getOpenID(){
        return this._openID;
    }

    getID(){
        return this._userinfo.account;
    }

    islog(){
        if(this._userinfo.account == null)
            return false;
        return true;
    }

    wxlogin(opt) {  //微信的静默登录
        opt = opt || {};
        format.callback(opt);
        opt.login = opt.login || false;

        if(this._wxlogin){
            if(this._userinfo.account !== null){
                opt._success({
                    code: 200
                });
                return;
            }
            //微信登录了 但是应用没有登录
            if (opt.login) { //需要强制登录
                this.login(opt);
                return;
            } else {    //不需要强制登录
                opt._success({
                    code: 201
                });
                return;
            }
        }

        wx.login({
            success:(res)=>{
                let code = res.code;
                ajax.post({
                    url: config.pxpro + 'mini/user/wxlogin',
                    data:{
                        code:code
                    },
                    success:(res)=>{
                        if (res.data.code == 200) {
                            this._wxlogin = true;
                            this._openID = res.data.openid;
                            this._userinfo.account = res.data.account;
                            if(res.data.account != null){
                                if(res.data.i)
                                    config.system = 'Android';
                                message.getDataList();
                                message.loadHeaderData();
                                this.loadUserInfo({
                                    success:(res)=>{
                                        this.loadUserMain({
                                            success:(res)=>{
                                                opt._success({code:200});
                                            },
                                            fail: (res) => {
                                                opt._fail(res);
                                            }
                                        });
                                    },
                                    fail: (res) => {
                                        opt._fail(res);
                                    }
                                });
                                return;
                            }
                            if(opt.login){
                                this.login(opt);
                            }else
                            opt._success({
                                code:201
                            });
                        }else{
                            wx.showModal({
                                title: '提示',
                                content: res.data.errmsg,
                                showCancel:false
                            })
                            opt._fail(res.data);
                        }
                    },
                    fail:(res)=>{
                        opt._fail({code:400,errmsg:'网络异常'});
                    }
                });
            },
            fail:(res)=>{
                opt._fail({
                    code:400,
                    errmsg:'系统异常'
                });
            }
        })

    }

    /*
        加载用户主要数据
    */
    loadUserMain(opt){
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/user/getUserMainInfo',
            data: {
                id: this._userinfo.account
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    this._userinfo.main = res.data;
                    //加载用户会员数据
                    vip.getInfo({
                        success: (res) => {
                            opt._success();
                        },
                        fail:(res)=>{
                            opt._fail(res);
                        }
                    });
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({
                    code: 400,
                    errmsg: '网络异常'
                });
            }
        });
    }

    loadUserInfo(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/user/getUserInfo',
            data:{
                id: this._userinfo.account 
            },
            success:(res)=>{
                res = res.data;
                if(res.code == 200){
                    this._userinfo.data = res.data;
                    //加载通知数据
                    message.getDataList();
                    message.loadHeaderData();
                    opt._success();
                }else{
                    opt._fail(res);
                }
            },
            fail:(res)=>{
                opt._fail({
                    code: 400,
                    errmsg: '网络异常'
                });
            }
        });
    }

    /*
        用户登录
    */
    login(opt) {
        login.open(opt);
    }


    /*
        打开我的作品
    */
    openMyOpus(opt) {
        opt = opt || {};
        format.callback(opt);
        if(this._userinfo.account == null){
            opt._fail({
                code:400,
                errmsg:'您还没登录呢'
            });
        }
        myOpus.open({
            id:this._userinfo.account
        });
    }

    /*
        关注/取消关注某个人
    */
    follow(opt) {
        opt = opt || {};
        format.callback(opt);
        ajax.post({
            url: config.pxpro + 'mini/follow',
            data: {
                userid: opt.id,
                follow:opt.follow
            },
            success: (res) => {
                res = res.data;
                if (res.code == 200) {
                    opt._success(res);
                } else {
                    opt._fail(res);
                }
            },
            fail: (res) => {
                opt._fail({
                    code: 400,
                    errmsg: '网络异常'
                });
            }
        });
    }

}