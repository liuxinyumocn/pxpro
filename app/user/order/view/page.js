// app/user/order/view/page.js
import order from '../order.js';
import format from '../../../lib/format.js';
import tool from '../../../lib/tool.js';
import config from '../../../config.js';
import browser from '../../../../domoe/browser/browser.js';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        opusPageIndex: 0,
        loadOpusing: false,
        opusList: [],
        system:'a',
        contact:false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        wx.hideShareMenu({})
        this.nextPageOpus();
        this.setData({
            system:config.system
        });
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        this.data.opusPageIndex = 0;
        order._opusData = [];
        this.nextPageOpus({
            complete: (res) => {
                wx.stopPullDownRefresh();
            }
        });
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.nextPageOpus();
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    nextPageOpus: function (opt) {
        opt = opt || {};
        format.callback(opt);
        if (this.data.loadOpusing)
            return;
        wx.showNavigationBarLoading();
        this.data.loadOpusing = true;
        order.getOpusList({
            page: this.data.opusPageIndex + 1,
            success: (res) => {
                wx.hideNavigationBarLoading();
                this.data.opusPageIndex++;
                this.loadOpus(res);
                opt._success(res);
            },
            fail: (res) => {
                wx.hideNavigationBarLoading();
                wx.showToast({
                    title: res.errmsg,
                    icon: 'none'
                })
                opt._fail(res);
            },
            complete: (res) => {
                this.data.loadOpusing = false;
            }
        });
    },
    loadOpus: function (res) {
        let arr = [];
        for (let i in res) {
            let item = res[i];
            let cell = {
                title: item.title,
                id: item.id,
                date: tool.getDate(item.timestamp),
                orderid: item.orderid,
                status: item.status,
                fee: tool.getFee(item.fee)
            }
            arr.push(cell);
        }
        this.setData({
            opusList: arr
        });
    },
    read:function(res){
        let orderid = res.currentTarget.dataset.orderid;
        let status = res.currentTarget.dataset.status;
        if(status == 0){ //待付款拉取最新状态
            wx.showLoading({
                title: '请稍后',
            })
            order.read({
                orderid:orderid,
                success: (res) => {
                    wx.hideLoading();
                    if(res.status == 0){
                        //去付款
                        browser.forpay(orderid,function(){
                            this.onPullDownRefresh();
                        }.bind(this));
                    }else{
                        this.other(orderid,res.status);
                    }
                },
                fail:(res)=>{
                    wx.hideLoading();
                    wx.showToast({
                        title: res.errmsg,
                        icon: 'none'
                    })
                }
            })
            return ;
        }
        this.other(orderid,status);
    },
    other:function(orderid,status){
        let str = '如对当前交易有异议，请使用订单号联系客服人员，是否复制订单号并联系客服呢？';
        let pb = '如有疑问请使用订单号联系客服人员，是否复制订单号并联系客服呢？';
        if (status == 1) {
            str = '当前交易已完成，'+pb;
        } else if (status == 2) {
            str = '当前交易已关闭，' + pb;
        } else if (status == 3) {
            str = '当前交易已退款，' + pb;
        }
        wx.showModal({
            title: '提示',
            content: str,
            success: (res) => {
                if (res.confirm) {
                    wx.setClipboardData({
                        data: orderid,
                        success: (res) => {
                            //打开客服窗口
                            this.setData({
                                contact:true
                            })
                        }
                    })
                }
            }
        })
    }
})