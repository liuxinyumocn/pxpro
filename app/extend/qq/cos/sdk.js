import config from '../../../config.js';
import format from '../../../lib/format.js';
let CosAuth = require('./cos-auth');

let cosSdk = function (opt) {
    opt = opt || {};
    format.callback(opt);

    var prefix = 'https://' + config.bucket + '.cos.' + config.region + '.myqcloud.com/';

    // 对更多字符编码的 url encode 格式
    var camSafeUrlEncode = function (str) {
        return encodeURIComponent(str)
            .replace(/!/g, '%21')
            .replace(/'/g, '%27')
            .replace(/\(/g, '%28')
            .replace(/\)/g, '%29')
            .replace(/\*/g, '%2A');
    };

    // 获取临时密钥
    var stsCache;
    var getCredentials = function (callback) {
        if (stsCache && Date.now() / 1000 + 30 < stsCache.expiredTime) {
            callback();
            return;
        }

        var data = opt.temKey;
        var credentials = data.credentials;
        if (credentials) {
            stsCache = data
        } else {
            opt._fail({
                code:400,
                errmsg:'系统繁忙'
            });
        }
        callback(stsCache && stsCache.credentials);
    };

    // 计算签名
    var getAuthorization = function (options, callback) {
        getCredentials(function (credentials) {
            callback({
                XCosSecurityToken: credentials.sessionToken,
                Authorization: CosAuth({
                    SecretId: credentials.tmpSecretId,
                    SecretKey: credentials.tmpSecretKey,
                    Method: options.Method,
                    Pathname: options.Pathname,
                })
            });
        });
    };

    // 上传文件
    var uploadFile = function (filePath,opt) {
        let filename = opt.fileName || filePath.substr(filePath.lastIndexOf('/') + 1);
        var Key = config.path + filename; // 这里指定上传的文件名
        getAuthorization({ Method: 'POST', Pathname: '/' }, function (AuthData) {
            var requestTask = wx.uploadFile({
                url: prefix,
                name: 'file',
                filePath: filePath,
                formData: {
                    'key': Key,
                    'success_action_status': 200,
                    'Signature': AuthData.Authorization,
                    'x-cos-security-token': AuthData.XCosSecurityToken,
                    'Content-Type': '',
                },
                success: function (res) {
                    var url = prefix + camSafeUrlEncode(Key).replace(/%2F/g, '/');
                    if (res.statusCode === 200) {
                        opt._success({
                            code:200,
                            content:url
                        });
                    } else {
                        opt._fail({
                            code:400,
                            content: JSON.stringify(res),
                            errmsg:'上传失败'
                        });
                    }
                    console.log(res.statusCode);
                    console.log(url);
                },
                fail: function (res) {
                    opt._fail({
                        code:400,
                        errmsg:'网络异常'
                    });
                }
            });
            requestTask.onProgressUpdate(function (res) {
                console.log('正在进度:', res);
            });
        });
    };

    uploadFile(opt.temPath,opt);

}


export default cosSdk;